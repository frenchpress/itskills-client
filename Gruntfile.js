"use strict";

module.exports = function(grunt) {

	grunt.initConfig({

		clean: {
			tmp : {
				src: [ 'tmp' ]
			},
			destfile : {
				src: [ 'build' ]
			}
		},

		jshint: {
			files: ['Gruntfile.js', 'src/**/*.js', 'tests/**/*.js'],
			options: {
		        additionalSuffixes: ['.js'],
		        globalstrict: true,
		        loopfunc: true,
		        globals : {
		        	require : true,
		        	module : true,
		        	React : true,
		        	console : true,
		        	document : true,
		        	Modernizr : true,
		        	localStorage : true,
		        	FormData : true,
		        	FileReader : true,
		        	_ : true,
		        	Raphael : true,
		        	setTimeout : true,
		        	moment : true,
		        	window : true
		        }
		    },
		},

		less: {
			style: {
				files: {
					"build/css/style.css": "src/less/app.less"
				}
			}
		},

		browserify: {
			dist: {
				files: {
					'tmp/js/app-babel.js': ['src/app/*.js', 'src/app/components/*.js'],
				},
				options: {
					transform: [
						[require('grunt-react').browserify, {global:true}]
					]
				},
			}
		},

		babel: {
			options: {
				sourceMap: false
			},
			dist: {
				files: {
					'build/js/app.js': 'tmp/js/app-babel.js'
				}
			}
		},

		copy: {
			main: {
				files: [{
					src: ['bower_components/modernizr/modernizr.js'], 
					dest: 'build/vendors/js/modernizr.js'
				},{
					src: ['bower_components/lodash/lodash.min.js'], 
					dest: 'build/vendors/js/lodash.min.js'
				}, {
					src: ['bower_components/react/react.js'], 
					dest: 'build/vendors/js/react.js'
				}, {
					src: ['bower_components/page.js/page.js'], 
					dest: 'build/vendors/js/page.js'
				}, {
					src: ['bower_components/axios/dist/axios.min.js'], 
					dest: 'build/vendors/js/axios.min.js'
				}, {
					src: ['bower_components/raphael/raphael-min.js'], 
					dest: 'build/vendors/js/raphael-min.js'
				}, {
					src: ['bower_components/moment/min/moment.min.js'], 
					dest: 'build/vendors/js/moment-min.js'
				}, {
					src: ['bower_components/axios/dist/axios.min.map'], 
					dest: 'build/vendors/js/axios.min.map'
				}, {
					src: ['bower_components/pure/grids-responsive-min.css'], 
					dest: 'build/vendors/css/purecss-base.css'
				}, {
					src: ['bower_components/pure-offset/index.css'], 
					dest: 'build/vendors/css/purecss-offset.css'
				}, {
					src: ['bower_components/pure-responsive-offset/index.css'], 
					dest: 'build/vendors/css/pure-responsive-offset.css'
				}, {
					src: ['bower_components/font-awesome/fonts/fontawesome-webfont.woff2'], 
					dest: 'build/fonts/fontawesome-webfont.woff2'
				}, {
					src: ['src/fonts/Lato/*.ttf'],
					dest: 'build/fonts/Lato',
					expand: true,
					flatten : true
				}, {
					src: ['src/fonts/Open_Sans/*.ttf'], 
					dest: 'build/fonts/Open_Sans/',
					expand: true,
					flatten : true
				}, {
					src: ['src/pictures/brands/*'], 
					dest: 'build/pictures/brands/',
					expand: true,
					flatten : true
				}, {
					src: ['src/pictures/misc/*'], 
					dest: 'build/pictures/misc/',
					expand: true,
					flatten : true
				}],
			},
		},

		includeSource: {
			options: {
				basePath : 'build',
				templates: {
					html: {
						js: '<script src="{filePath}"></script>',
						css: '<link rel="stylesheet" type="text/css" href="{filePath}" />',
					}
				}
			},
			myTarget: {
				files: {
					'build/index.html': 'src/index.tpl.html'
				}
			}
		},

		watch: {
			options: {
		      livereload: true,
		    },
			css: {
				files: 'src/less/*.less',
				tasks: ['less']
			},
			scripts: {
			    files: ['src/app/*.js', 'src/app/**/*.js'],
			    tasks: ['default']
			 }
		},

		svgstore: {
            options: {
                prefix : 'svg-',
                svg : {
                    xmlns : 'http://www.w3.org/2000/svg',
                    'xmlns:xlink' : "http://www.w3.org/1999/xlink"
                }
            },
            default : {
                files: {
                    'build/pictures/svg-defs.svg': ['src/pictures/svg/*.svg'],
                }
            }
        },

        include_file: {
        default_options: {
            cwd: '',
            src: ['build/index.html'],
            dest: ''
        }
    }

	});

grunt.loadNpmTasks('grunt-contrib-copy');
grunt.loadNpmTasks('grunt-contrib-clean');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-browserify');
grunt.loadNpmTasks('grunt-include-source');
grunt.loadNpmTasks('grunt-jsxhint-babel');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-less');
grunt.loadNpmTasks('grunt-babel');
grunt.loadNpmTasks('grunt-svgstore');
grunt.loadNpmTasks('grunt-include-file');


grunt.registerTask('default', ['clean:destfile', 'jshint', 'less', 'browserify', 'babel', 'copy:main', 'svgstore', 'includeSource', 'include_file', 'clean:tmp']);

};