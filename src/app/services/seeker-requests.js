"use strict";

let superagent = require('../../../node_modules/superagent/lib/client.js'),
	properties = require('./properties.js'),
	base = properties.baseUrl + "seeker/",
	storage = require('./storage.js');

function addCredentials(data){
	let emp = storage.get('seeker');
	data.email = emp.email;
	data.password = emp.password;
}

function addFormCredentials(formData){
	let emp = storage.get('seeker');
	formData.append('email', emp.email);
	formData.append('password', emp.password);
}

module.exports = {
	login(email, password, callback){
		superagent
			.get(properties.baseUrl + "seeker")
			.query({
				email : email,
				password : password
			})
			.end((err, res) => {
				if(callback){
					callback(res.body);
				}
			});
	},
	isMailAvailable(mail, callback){
		superagent
			.get(base + 'mail/' + mail + '/available')
			.end((err, res) => {
				if(callback){
					callback(res.body.available);
				}
			});
	}, 
	create(mail, password, name, callback){
		superagent
			.post(base)
			.send({
				email : mail,
				password : password,
				name : name
			}).end((err,res) => {
				if(callback){
					let ans = '';
					if(res.status === 400){
						ans = res.body.reason;
					} else if (res.status === 500){
						ans = 'Sorry, it looks like there is communication trouble with our servers.';
					}
					callback(res.status === 201, ans);
				}
			});
	}, 
	update(email, data, callback){
		var seek = storage.get('seeker');
		if(seek && email !== seek.email){
			data.newEmail = email;
		}
		addCredentials(data);
		superagent
			.put(base + seek.email)
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false, 'Sorry, the server seems to be unreachable.');
					}
				}
			});
	}, 
	addCv(email, cv, callback){
		var formData = new FormData();
		formData.append('cv', cv);
		addFormCredentials(formData);
		superagent
			.post(base + email + "/cv")
			.send(formData)
			.end((err, res) => {
				if(callback){
					callback(res.status === 200, res.body && res.body.message ? res.body.message : res.body);
				}
			});
	},
	getJobs(mail, callback){
		superagent
			.get(base + mail + '/jobs')
			.end((err, res) => {
				if(callback){
					callback(res.body);
				}
			});
	},
	getEmployerProfile(id, callback){
		superagent
			.get(base + 'employer/' + id)
			.end((err, res) => {
				if(callback){
					callback(res.body);
				}
			});
	},
	favor(jobId, callback){
		let data = {};
		addCredentials(data);
		superagent
			.post(base + 'jobs/' + jobId + '/favor')
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false);
					}
				}
			});
	},
	discard(jobId, callback){
		let data = {};
		addCredentials(data);
		superagent
			.post(base + 'jobs/' + jobId + '/discard')
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false);
					}
				}
			});
	},
	unfavor(jobId, callback){
		let data = {};
		addCredentials(data);
		superagent
			.post(base + 'jobs/' + jobId + '/unfavor')
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false);
					}
				}
			});
	},
}