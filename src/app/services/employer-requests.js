"use strict";

let superagent = require('../../../node_modules/superagent/lib/client.js'),
	properties = require('./properties.js'),
	base = properties.baseUrl + "employer/",
	storage = require('./storage.js');

function addCredentials(data){
	let emp = storage.get('employer');
	data.email = emp.email;
	data.password = emp.password;
}

function addFormCredentials(formData){
	let emp = storage.get('employer');
	formData.append('email', emp.email);
	formData.append('password', emp.password);
}

module.exports = {
	login(email, password, callback){
		superagent
			.get(base)
			.query({
				email : email,
				password : password
			})
			.end((err, res) => {
				if(callback){
					callback(res.body);
				}
			});
	},
	getSeekerProfile(id, callback){
		superagent
			.get(base + 'seeker/' + id)
			.end((err, res) => {
				if(callback){
					callback(res.body);
				}
			});
	},
	isMailAvailable(mail, callback){
		superagent
			.get(base + 'mail/' + mail + '/available')
			.end((err, res) => {
				if(callback){
					callback(res.body.available);
				}
			});
	},
	create(mail, password, callback){
		superagent
			.post(base)
			.send({
				email : mail,
				password : password
			}).end((err,res) => {
				if(callback){
					let ans = '';
					if(res.status === 400){
						ans = res.body.reason;
					} else if (res.status === 500){
						ans = 'Sorry, it looks like there is communication trouble with our servers.';
					}
					callback(res.status === 201, ans);
				}
			});
	},
	update(email, data, callback){
		var emp = storage.get('employer');
		if(emp && email !== emp.email){
			data.newEmail = email;
		}
		addCredentials(data);
		superagent
			.put(base + emp.email)
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false, 'Sorry, the server seems to be unreachable.');
					}
				}
			});
	},
	createJob(job, callback){
		let body = {job : job}
		addCredentials(body);
		superagent
			.post(base + 'jobs')
			.send(body)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 201){
						callback(true, res.body);
					} else {
						callback(false, 'Sorry, the server seems to be unreachable.');
					}
				}
			});
	},
	sortJobs(array, callback){
		let body = {array : array}
		addCredentials(body);
		superagent
			.put(base + 'jobs/sort')
			.send(body)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false, 'Sorry, the server seems to be unreachable.');
					}
				}
			});
	},
	addPicture(email, picture, callback){
		let formData = new FormData();
		formData.append('avatar', picture);
		addFormCredentials(formData);
		superagent
			.post(base + email + "/picture")
			.send(formData)
			.end((err, res) => {
				if(callback){
					callback(res.status === 200, res.body ? res.body.message : undefined);
				}
			});
	},
	getAllJobs(mail, callback){
		let data = {};
		addCredentials(data);
		superagent
			.get(base + 'jobs')
			.query(data)
			.end((err, res) => {
				if(callback){
					callback(!err, res.body);
				}
			});
	},
	getAllMatches(mail, callback){
		let data = {};
		addCredentials(data);
		superagent
			.get(base + 'matches')
			.query(data)
			.end((err, res) => {
				if(callback){
					callback(!err, res.body);
				}
			});
	},
	getJobSeekers(id, callback){
		let data = {};
		addCredentials(data);
		superagent
			.get(base + 'jobs/' + id + '/seekers')
			.query(data)
			.end((err, res) => {
				if(callback){
					callback(!err, res.body);
				}
			});
	},
	favor(jobId, seekerId, callback){
		let data = {seekerId : seekerId};
		addCredentials(data);
		superagent
			.post(base + 'jobs/' + jobId + '/favor')
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false);
					}
				}
			});
	},
	discard(jobId, seekerId, callback){
		let data = {seekerId : seekerId};
		addCredentials(data);
		superagent
			.post(base + 'jobs/' + jobId + '/discard')
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false);
					}
				}
			});
	},
	unfavor(jobId, seekerId, callback){
		let data = {seekerId : seekerId};
		addCredentials(data);
		superagent
			.post(base + 'jobs/' + jobId + '/unfavor')
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true);
					} else {
						callback(false);
					}
				}
			});
	},
}