"use strict";

let superagent = require('../../../node_modules/superagent/lib/client.js'),
	storage = require('./storage.js');

//Flat
let baseUrl = 'http://localhost:3000/',
stepperProps = (id, steps, step) => {
	return {
		canvasId : id,
		width : 200,
		height : 200,
		strokeWidth : {
			bcg : 10,
			inner : 10
		},
		colors : {
			bcg : 'white',
			inner : '#293540',
			label : '#293540',
			pourcent : '#293540',
			unselectedLabel : 'grey'
		},
		activeStep : step,
		steps : steps,
		transitionTime : 0.5,
		fontSize : {
			label : 15,
			percent : 25,
			unselected : 10
		},
		fontFamily : {
			label : 'light',
			percent : 'light'
		},
		offsets : {
			label : 20,
			percent : 25,
			unselected : 15
		},
		unselectedOpacity : 0.8
	}
},
ratingProps = (id, rating) => {
	return {
		canvasId : id,
		width : 60,
		height : 60,
		strokeWidth : {
			bcg : 0,
			inner : 2
		},
		rating : rating,
		colors : {
			strokeBcg : '',
			bcg : '#293540',
			inner : 'white',
			rating : 'white',
		},
		fontSize : {
			rating : 15
		},
		fontFamily : {
			rating : 'light'
		},
		offsets : {
			rating : 0,
			inner : 4
		},
		withPercent : true
	}
},
ratingProps2 = (id, rating) => { 
	return {
		canvasId : id,
		width : 200,
		height : 200,
		strokeWidth : {
			bcg : 0,
			inner : 8
		},
		rating : rating,
		colors : {
			strokeBcg : '',
			bcg : '#79BDE0',
			inner : 'white',
			rating : 'white',
		},
		fontSize : {
			rating : 30
		},
		fontFamily : {
			rating : 'light'
		},
		offsets : {
			rating : 0,
			inner : 10
		},
		withPercent : true
	}
};

//Fetched
function getProps(cb){
	var props = storage.get('properties');
	if(props && cb){
		cb(props);
	} else if(cb){
		superagent
			.get(baseUrl + 'properties')
			.end((err, res) => {
				storage.set('properties', res.body);
				cb(res.body);
			});
	}
}

function getIndustries(cb){
	getProps(function(props){
		cb(props.industries);
	});
}

function getIndusty(id, cb){
	getProps(function(props){
		props.industries.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getLocations(cb){
	getProps(function(props){
		cb(props.locations);
	});
}

function getLocation(id, cb){
	getProps(function(props){
		props.locations.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getCompanySizes(cb){
	getProps(function(props){
		cb(props.companySizes);
	});
}

function getCompanySize(id, cb){
	getProps(function(props){
		props.companySizes.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getDegrees(cb){
	getProps(function(props){
		cb(props.degrees);
	});
}

function getDegree(id, cb){
	getProps(function(props){
		props.degrees.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getSocials(cb){
	getProps(function(props){
		cb(props.social);
	});
}

function getSocial(id, cb){
	getProps(function(props){
		props.social.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getStatuses(cb){
	getProps(function(props){
		cb(props.status);
	});
}

function getStatus(id, cb){
	getProps(function(props){
		props.status.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getXps(cb){
	getProps(function(props){
		cb(props.xp);
	});
}

function getXp(id, cb){
	getProps(function(props){
		props.xp.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getXpsSelected(xps, cb){
	getProps(function(props){
		let tab = [];
		props.xp.map((xp) => {
			xps.forEach(x => {
				if(xp.id === x){
					tab.push(xp);
				}
			});
		});
		cb(tab);
	});
}

function getJobTypes(cb){
	getProps(function(props){
		cb(props.jobType);
	});
}

function getJobType(id, cb){
	getProps(function(props){
		props.jobType.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getSalaries(cb){
	getProps(function(props){
		cb(props.salary);
	});
}

function getSalary(id, cb){
	getProps(function(props){
		props.salary.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}

function getPlans(cb){
	getProps(function(props){
		cb(props.plans);
	});
}

function getPlan(id, cb){
	getProps(function(props){
		props.plans.forEach(function(x){
			if(x.id === id){
				cb(x);
			}
		});
	});
}


function getJobTypesSelected(types, cb){
	getProps(function(props){
		let tab = [];
		if(types){
			props.jobType.map((type) => {
				types.forEach(x => {
					if(type.id === x){
						tab.push(type);
					}
				});
			});
		}
		cb(tab);
	});
}

//Export
module.exports = {
	baseUrl : baseUrl,
	routes : {
		employer : {
			register1 : '/itskills/employer/register/1',
			register2 : '/itskills/employer/register/2',
			register3 : '/itskills/employer/register/3',
			register4 : '/itskills/employer/register/4',
			home : '/itskills/employer/home',
			jobs : '/itskills/employer/jobs/:id',
			matches : '/itskills/employer/matches',
			seekprofile : '/itskills/employer/seeker/:id/profile',
			profile : '/itskills/employer/profile',
			settings : '/itskills/employer/settings'
		},
		seeker : {
			register1 : '/itskills/seeker/register/1',
			register2 : '/itskills/seeker/register/2',
			register3 : '/itskills/seeker/register/3',
			register4 : '/itskills/seeker/register/4',
			home : '/itskills/seeker/home',
			jobs : '/itskills/seeker/jobs/:id',
			empprofile : '/itskills/seeker/employer/:id/profile',
			profile : '/itskills/seeker/profile',
			settings : '/itskills/seeker/settings'
		},
		login : '/itskills/login',
		core : '/itskills/index',
		emp : '/itskills/employer'
	},
	stepperProps : stepperProps,
	ratingProps : ratingProps,
	ratingProps2 : ratingProps2,

	getIndustries : getIndustries,
	getIndusty : getIndusty,
	getLocations : getLocations,
	getLocation : getLocation,
	getCompanySizes : getCompanySizes,
	getCompanySize : getCompanySize,
	getDegrees : getDegrees,
	getDegree : getDegree,
	getSocials : getSocials,
	getSocial : getSocial,
	getStatuses : getStatuses,
	getStatus : getStatus,
	getXps : getXps,
	getXp : getXp,
	getJobTypes : getJobTypes,
	getJobType : getJobType,
	getJobTypesSelected : getJobTypesSelected,
	getXpsSelected : getXpsSelected,
	getSalaries : getSalaries,
	getSalary : getSalary,
	getPlans : getPlans,
	getPlan : getPlan
};