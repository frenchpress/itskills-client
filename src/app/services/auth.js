let properties = require('./properties'),
	storage = require('./storage'),
	employerRequests = require('./employer-requests.js'),
	seekerRequests = require('./seeker-requests.js');

module.exports = {
	employer : {
		login(email, pwd, callback){
			employerRequests.login(email, pwd, x => {
				x.password = pwd;
				console.log('set employer');
				console.log(x);
				storage.set('employer', x);
				if(callback){
					callback();
				}
			});
		}
	},
	seeker : {
		login(email, pwd, callback){
			seekerRequests.login(email, pwd, x => {
				x.password = pwd;
				console.log('set seeker');
				console.log(x);
				storage.set('seeker', x);
				if(callback){
					callback();
				}
			});
		}
	}
}