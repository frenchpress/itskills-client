"use strict";

let superagent = require('../../../node_modules/superagent/lib/client.js'),
	properties = require('./properties.js'),
	base = properties.baseUrl + "employer/jobs/",
	storage = require('./storage.js');

	module.exports = {
	getJobDetail(id, callback){
		superagent
			.get(base + id)
			.end((err, res) => {
				if(callback){
					callback(res.body);
				}
			});
	}, 
	update(id, data, callback){
		let emp = storage.get('employer');
		superagent
			.put(base + id + '?email=' + emp.email + '&password=' + emp.password)
			.send(data)
			.end((err, res) => {
				if(callback){
					if(!err && res.status === 200){
						callback(true, res.body);
					} else {
						callback(false, 'Sorry, the server seems to be unreachable.');
					}
				}
			});
	}
}