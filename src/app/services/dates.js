"use strict";

module.exports = {
	localToUtc(date){
		
	},
	UtcToLocal(date){
		return moment(date, "YYYY-MM-DDTHH:mm:ss Z");
	},
    display : {
        written(date){
            return moment(date).format("D MMMM YYYY");
        }   
    },
    dateOfExpiry(timestamp){
    	return new Date(timestamp + 30 * 86400000);
    },
    daysLeft(timestamp){
    	let now = new Date().getTime(),
    		diff = now - timestamp;
    	return 30 - Math.floor(diff / 86400000);
    }
}