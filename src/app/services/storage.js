"use strict";

function createCookie(name, value) {
    let expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    let nameEQ = name + "=",
    	ca = document.cookie.split(';');
    for(var i=0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' '){
        	c = c.substring(1,c.length);
        } 
        if (c.indexOf(nameEQ) === 0) {
        	return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

module.exports = {
	get(key){
		if(Modernizr.localstorage){
			return JSON.parse(localStorage.getItem(key));
		} else {
			return JSON.parse(readCookie(key));
		}
	},
	set(key, value){
		if(Modernizr.localstorage){
			localStorage.setItem(key, JSON.stringify(value));
		} else {
			createCookie(key, JSON.stringify(value));
		}
	}
}