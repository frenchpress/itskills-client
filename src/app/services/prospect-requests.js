"use strict";

let superagent = require('../../../node_modules/superagent/lib/client.js'),
	properties = require('./properties.js'),
	base = properties.baseUrl + "prospect/",
	storage = require('./storage.js');

module.exports = {
	createSeekerProspect(mail, callback){
		superagent
			.post(base + 'seeker')
			.send({
				email : mail
			}).end((err, res) => {
				if(callback){
					callback(res.status === 201);
				}
			});
	},
	createEmployerProspect(mail, callback){
		superagent
			.post(base + 'employer')
			.send({
				email : mail
			}).end((err, res) => {
				if(callback){
					callback(res.status === 201);
				}
			});
	}
}