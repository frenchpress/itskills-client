"use strict";

let superagent = require('../../../node_modules/superagent/lib/client.js'),
	properties = require('./properties.js'),
	base = properties.baseUrl + "tags/",
	storage = require('./storage.js');

module.exports = {
	getTags(mail, callback){
		superagent
			.get(base)
			.end((err, res) => {
				if(callback){
					callback(res.body);
				}
			});
	},
}