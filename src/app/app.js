"use strict";

let comps = {
	employer : require('./components/employer/pages.js'),
	seeker : require('./components/seeker/pages.js'),
	global : require('./components/global.js'),
	core : require('./components/unsign/core.js'),
	emp : require('./components/unsign/emp.js')
},
yarr = require('../../node_modules/yarr.js/yarr.js'),
properties = require('./services/properties.js');

//Core

yarr(properties.routes.core, function(){
	React.render(
		<comps.core.Core />,
		document.getElementById('content')
	)
});

yarr(properties.routes.emp, function(){
	React.render(
		<comps.emp.Emp />,
		document.getElementById('content')
	)
});

//Login

yarr(properties.routes.login, function(){
	React.render(
		<comps.global.Login />,
		document.getElementById('content')
	)
});

yarr(properties.routes.login, function(){
	React.render(
		<comps.global.Login />,
		document.getElementById('content')
	)
});

//Employer

yarr(properties.routes.employer.register1, function(){
	React.render(
		<comps.employer.Register1 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.register2, function(){
	React.render(
		<comps.employer.Register2 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.register3, function(){
	React.render(
		<comps.employer.Register3 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.register4, function(){
	React.render(
		<comps.employer.Register4 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.home, function(){
	React.render(
		<comps.employer.Home />,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.matches, function(){
	React.render(
		<comps.employer.Matches />,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.profile, function(){
	React.render(
		<comps.employer.Profile />,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.seekprofile, function(ctx){
	React.render(
		<comps.employer.Seekprofile seekId={ctx.params.id}/>,
		document.getElementById('content')
	)
});

yarr(properties.routes.employer.settings, function(){
	React.render(
		<comps.employer.Settings />,
		document.getElementById('content')
	)
});

//Seeker

yarr(properties.routes.seeker.register1, function(){
	React.render(
		<comps.seeker.Register1 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.seeker.register2, function(){
	React.render(
		<comps.seeker.Register2 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.seeker.register3, function(){
	React.render(
		<comps.seeker.Register3 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.seeker.register4, function(){
	React.render(
		<comps.seeker.Register4 />,
		document.getElementById('content')
	)
});

yarr(properties.routes.seeker.home, function(){
	React.render(
		<comps.seeker.Home />,
		document.getElementById('content')
	)
});

yarr(properties.routes.seeker.empprofile, function(ctx){
	React.render(
		<comps.seeker.Empprofile empId={ctx.params.id}/>,
		document.getElementById('content')
	)
});

yarr(properties.routes.seeker.profile, function(){
	React.render(
		<comps.seeker.Profile/>,
		document.getElementById('content')
	)
});

//Jobs 

yarr(properties.routes.employer.jobs, function(ctx){
	React.render(
		<comps.employer.Jobs jobId={ctx.params.id}/>,
		document.getElementById('content')
	)
});

yarr(properties.routes.seeker.jobs, function(ctx){
	React.render(
		<comps.seeker.Jobs jobId={ctx.params.id}/>,
		document.getElementById('content')
	)
});

// yarr(properties.routes.login);
// yarr(properties.routes.core);
// yarr(properties.routes.emp);

// yarr(properties.routes.employer.register1);
// yarr(properties.routes.employer.register2);
// yarr(properties.routes.employer.register3);
// yarr(properties.routes.employer.register4);

// yarr(properties.routes.employer.home);
// yarr(properties.routes.employer.matches);
// yarr(properties.routes.employer.profile);
// yarr(properties.routes.employer.jobs);
// yarr('/itskills/employer/seeker/561a073ce3ca7754cf9d4518/profile');
// yarr('/itskills/employer/jobs/561ed07757dadc6f06b5f911');

// yarr(properties.routes.seeker.register1);
// yarr(properties.routes.seeker.register2);
// yarr(properties.routes.seeker.register3);
// yarr(properties.routes.seeker.register4);

yarr(properties.routes.seeker.home);
// yarr(properties.routes.seeker.profile);
// yarr('/itskills/seeker/employer/5616ecb121a7476a3ec74f49/profile');
// yarr('/itskills/seeker/jobs/5622c70157dadc6f06b5f96c');

