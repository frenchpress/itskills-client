"use strict";

let FileDragAndDrop = require('../../../node_modules/react-file-drag-and-drop/build/FileDragAndDrop.js');

let MailInput = React.createClass({
  componentDidMount(){
    if(this.props.defaultValue){
      this.refs.input.getDOMNode().value = this.props.defaultValue;
    }
  },
  checkMail(event) {
    let mail = event.target.value,
        re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if(mail.length > 0 && re.test(mail)){
      this.props.mailUpdate(true, mail);
    } else {
      this.props.mailUpdate(false, mail);
    }
  },
  render() {
    return (	
      <input ref="input" onBlur={this.checkMail} type="mail" className={this.props.classProp + " with-shadow-underline"} autoFocus={this.props.isFocus} placeholder={this.props.placeholder}/>
    );
  }
});

let GenericInput = React.createClass({
  componentDidMount(){
    if(this.props.defaultValue){
      this.refs.input.getDOMNode().value = this.props.defaultValue;
    }
  },
  componentWillReceiveProps(nextProp){
    if(nextProp.defaultValue && !this.refs.input.getDOMNode().value){
      this.refs.input.getDOMNode().value = nextProp.defaultValue;
    }
  },
  blur(event) {
    if(this.props.compulsory){
      this.props.exists(!!event.target.value.length, event.target.value);
    }
  },
  keyDown(event){
    if(event.which === 13 && this.props.enterPressed){
      this.props.enterPressed(event.target.value);
    } else if(event.which === 32 && this.props.spaceBarPressed){
      this.props.spaceBarPressed(event.target.value);
    }
  },
  keyUp(event){
    if(this.props.keyUp){
      this.props.keyUp(event.target.value);
    }
  },
  render() {
    return (  
      <input ref="input" style={this.props.givenStyle} onKeyUp={this.keyUp} onBlur={this.blur} type="text" className={this.props.classProp + " with-shadow-underline"} onKeyDown={this.keyDown} autoFocus={this.props.isFocus} placeholder={this.props.placeholder}/>
    );
  }
});

let Textarea = React.createClass({
  componentDidMount(){
    if(this.props.defaultValue){
      this.refs.input.getDOMNode().value = this.props.defaultValue;
    }
  },
  componentWillReceiveProps(nextProp){
     if(nextProp.defaultValue){
      this.refs.input.getDOMNode().value = nextProp.defaultValue;
    }
  },
  blur(event) {
    if(this.props.compulsory && this.props.exists){
      this.props.exists(!!event.target.value.length, event.target.value);
    }
  },
  enterPressed(event){
    if(event.which === 13 && this.props.enterPressed){
      this.props.enterPressed(event.target.value);
    }
  },
  keyUp(event){
    if(this.props.setValue){
      this.props.setValue(event.target.value);
    }
  },
  render() {
    return (  
      <textarea ref="input" onKeyUp={this.keyUp} onBlur={this.blur} type="text" className={this.props.classProp + " with-shadow-underline"} onKeyDown={this.enterPressed} autoFocus={this.props.isFocus} rows={this.props.rows} placeholder={this.props.placeholder}/>
    );
  }
});

let PwdInput = React.createClass({
  pwdStrength(event){

    this.props.pwdUpdate(event.target.value.length > 6 && event.target.value.length <= 30, event.target.value);

    let strength = 0;
    if(/[a-z]/.test(event.target.value)){
      strength += 15;
    }
    if(/[A-Z]/.test(event.target.value)){
      strength += 15;
    }
    if(/\d/.test(event.target.value)){
      strength += 15;
    }
    if(event.target.value.length > 15){
      strength += 55;
    } else if(event.target.value.length > 10){
      strength += 40;
    } else if(event.target.value.length > 5){
      strength += 20;
    }
    if(this.props.pwdStrength){
      this.props.pwdStrength(strength);  
    }
  },
  enterPressed(event){
    if(event.which === 13){
      this.props.enterPressed();
    }
  },
  render() {
    return (
      <input onKeyUp={this.pwdStrength} onKeyDown={this.enterPressed} type="password" className={this.props.classProp + " with-shadow-underline"} autoFocus={this.props.isFocus} placeholder={this.props.placeholder}/>
    );
  }
});

let ValidateButton = React.createClass({
  render() {
    let changeColorIcon = this.props.submitableUpdate ? 'background-color2' : 'background-color1',
        changeIcon = this.props.submitableUpdate ? 'svg-check' : 'svg-pause';
    return (
    	<IconButton className={this.props.classProp} onClick={this.props.onClick} icon={changeIcon} generalBackground='background-color1' colorIcon={changeColorIcon + ' fill-white button-transition'} labels={this.props.labels} />
    );
  }
});

let ErrorBox = React.createClass({
  render() {
    let givenStyle = {
      visibility : this.props.visible ? 'visible' : 'hidden'
    };
    return (
      <div className='font-13px color5 display-error' style={givenStyle}>{this.props.labels}</div>
    );
  }
});

let IconButton = React.createClass({
  render() {
    let divStyle = {
      height : '40px'
    }
    return (
      <div onClick={this.props.onClick} className={'pointer inline-block margin-top-20px' + ' ' + this.props.generalBackground + " " + this.props.className} style={divStyle}>
        <Icon icon={this.props.icon}  classProp={'padding-10px icon-mini ' + this.props.colorIcon} /> 
        <div className="inline-block padding-right-20px padding-left-20px color4 font-18px validate-label thin-text">
          {this.props.labels}
        </div>
      </div>
    );
  }
});

let MiniIconButton = React.createClass({
  render() {
    let divStyle = {
      height : '25px'
    }
    let textColorClass = this.props.textColorlass ? this.props.textColorlass : 'color4';
    return (
      <div onClick={this.props.onClick} className={'pointer inline-block margin-top-5px' + ' ' + this.props.generalBackground + " " + this.props.className} style={divStyle}>
        <Icon icon={this.props.icon}  classProp={'padding-5px icon-tiny ' + this.props.colorIcon} /> 
        <div className={"inline-block padding-right-10px padding-left-5px font-11px validate-label-mini " + textColorClass}>
          {this.props.labels}
        </div>
      </div>
    );
  }
});

let ProgressBar = React.createClass({
  render() {
    let innerStyle = {
      width : this.props.percent+ '%',
      backgroundColor : this.props.color
    };
    return (
      <div className='progress-bar-wrapper'>
        <div className='progress-bar-inner' style={innerStyle}></div>
      </div>
    );
  }
});

let Label = React.createClass({
  render() {
    return (
   		<h4>{this.props.labels}</h4>
    );
  }
});

let Icon = React.createClass({
  render() {
    let useTag = '<use xlink:href="#' + this.props.icon + '"/>';
    return <svg style={this.props.givenStyle} onClick={this.props.click} className={this.props.classProp + ' inline-block'} dangerouslySetInnerHTML={{__html: useTag }} />;
  }
});

let CustomSelect = React.createClass({
  getInitialState() {
    let data = [];
    if(this.props.data){
        data = this.props.data.map(item => {
          return {id : item.id, item : item.name, valid : true}
        });
    }
    return {
      originalData : data,
      data : data,
      open : {
        display : 'block'
      }
    };
  },
  componentWillReceiveProps(nextProps){
    if(nextProps.data){
        let data = nextProps.data.map(item => {
          return {id : item.id, item : item.name, valid : true}
        });
        this.setState({data : data, originalData : data});
    }
  },
  change(event){
    let data = this.state.originalData.map(item => {
      return {id : item.id, item : item.item, valid : item.item.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1 || item.item === "Other"}
    });
    this.setState({data : data, open : {
      display : 'block'
    }});
  },
  focus(){
    this.setState({open : {
      display : 'block'
    }});
  },
  keyUp(event){
    if(event.which === 13 && event.target.value.length){
      this.enter();
    }
  },
  enter(){
    let found = false;
    this.state.data.forEach( block => {
      if(block.valid && !found){
        React.findDOMNode(this.refs.specialSelect).value = block.item;
        this.setState({open : {
          display : 'none'
        }});
        found = true;
        this.props.setValue(block.id);
      }
    });
    React.findDOMNode(this.refs.specialSelect).blur();
  },
  nodeClick(block){
    React.findDOMNode(this.refs.specialSelect).value = block.item;
    this.props.setValue(block.id);
    this.setState({open : {
      display : 'none'
    }});
  },
  render() {
    let nodes = this.state.data.map((line, index) => {
        let selectClass = line.valid ? 'valid-select-option' : 'invalid-select-option',
            ndClick = this.nodeClick.bind(this, line);
        return <div key={index} onClick={ndClick} className={'select-option ' + selectClass}>{line.item}</div>;
    });
    return (
      <div>
        <div className='width-10pc inline-block'>
          <Icon icon={this.props.icon} classProp={this.props.iconClassProp}/>
        </div>
        <input id="specialSelect" ref="specialSelect" value={this.state.selected} onKeyUp={this.keyUp} onFocus={this.focus} onChange={this.change} type="text" autoFocus={this.props.isFocus} className={"width-80pc padding-left-5px margin-bottom-10px v-align-top " + this.props.classProp}/>
        <div style={this.state.open}>
          {nodes}
        </div>
      </div>
    );
  }
});

let TinyList = React.createClass({
  getInitialState() {
    let data = [];
    if(this.props.data){
      data = this.props.data.map(x => {
        return {id : x.id, item : x.name, selected : false}
      });
    }
    return {
      selected : null,
      data : data,
      arrowStyle : {
        "visibility" : 'hidden'
      }
    };
  },
  componentDidMount(){
    if(this.props.defaultId && this.props.data){
      this.props.data.forEach(x => {
        if(x.id === +this.props.defaultId){
          this.select({id : x.id, item : x.name, selected : false});
        }
      });
    }
  },
  componentWillReceiveProps(nextProp){
    let selected = -1;
    if(nextProp.data){
      let data = nextProp.data.map(x => {
          if(nextProp.defaultId && +nextProp.defaultId === x.id){
            selected = x;
          }
          return {
            id : x.id, 
            item : x.name, 
            selected : nextProp.defaultId && +nextProp.defaultId === x.id ? true : false
          }
      });
      this.setState({data : data, selected : selected}, () => {
        if(selected >= 0){
          this.select(selected);
        }
      });
    }
  },
  select(block){
    this.setState({selected : block, arrowStyle : {
      "paddingTop" : 19 * block.id - 19 + 'px'
    }});
    this.props.setValue(block);
  },
  render() {
    let array = this.state.data.map((block, index) => {
      let innSelect = this.select.bind(this, block),
          innerClass = (this.state.selected && block.id === this.state.selected.id) ? 'tiny-list-selected' : '';
      return <div key={index} className={"tiny-list " + innerClass} onClick={innSelect}>{block.item}</div>
    });
    return (
      <div>
        <div className='width-10pc inline-block v-align-top'>
          <Icon icon={this.props.icon} classProp={this.props.iconClassProp}/>
        </div>
        <div className='margin-left-10px inline-block margin-right-5px v-align-top tiny-list-arrow' style={this.state.arrowStyle}>
          <Icon icon='svg-arrow-r' classProp='icon-tiny fill-color2 tiny-list-arrow'/>
        </div>
        <div className='inline-block v-align-top padding-botton-1px'>
          {array}
        </div>
      </div>
    );
  }
});

let DragAndDropImage = React.createClass({
  getInitialState() {
    return {};
  },
  componentDidMount(){
    React.findDOMNode(this.refs.fileInput).addEventListener('change', evt => {
      this.handleDrop(evt.target);
    }, false);
  },
  handleDrop: function (dataTransfer) {
    let files = dataTransfer.files;
    if(this.props.handleFiles){
      this.props.handleFiles(files);
      let size = files[0].size > 1000000 ? Math.round(files[0].size / 100000)/10 + " MB" : Math.round(files[0].size / 100)/10 + " KB";
      this.setState({file : files[0], fileName : files[0].name, fileSize : size});
    }
  },
  browse(){
    React.findDOMNode(this.refs.fileInput).click();
  },
  render: function () {
    let styles = {
      default : {display : this.props.image || this.props.errMsg ? 'none' : 'block'},
      image : {display : this.props.image && !this.props.errMsg ? 'block' : 'none'},
      err : {display : this.props.errMsg ? 'block' : 'none'}
    },
    detailsStyle = {
      display : this.state.fileName && this.state.fileSize ? "inline-block" : "none"
    };
    return (
        <div>
          <div>
            <div className={this.props.classProp + " overflow-hidden inline-block v-align-top"} onClick={this.browse}>
              <FileDragAndDrop onDrop={this.handleDrop}>
                <Icon icon={this.props.defaultIcon} classProp="icon-normal bloc-centered margin-top-15px fill-color1" givenStyle={styles.default}/>
                <div className="font-10px text-centered margin-top-5px" style={styles.default}>Browse or Drag</div>
                <img src={this.props.image} style={styles.image} className=""/>
                <div style={styles.err} className="padding-5px font-12px color5">{this.props.errMsg}</div>
              </FileDragAndDrop>
            </div>
            <div className="v-align-top margin-left-10px" style={detailsStyle}>
              <div><span className="font-12px">File :</span> <span className="font-10px">{this.state.fileName}</span></div>
              <div><span className="font-12px">Size :</span> <span className="font-10px">{this.state.fileSize}</span></div>
            </div>
          </div>
          <div className="font-12px text-centered width-80px margin-top-5px margin-bottom-5px">or</div>
          <div onClick={this.browse} className="pointer inline-block padding-right-20px padding-left-20px padding-top-5px padding-bottom-5px white-color background-color1 font-13px thin-text">
            Browse
          </div>
          <input type="file" ref="fileInput" name="files[]" className="hide"/>
        </div>
    );
  }
});

let DragAndDrop = React.createClass({
  getInitialState() {
    return {};
  },
  componentDidMount(){
     React.findDOMNode(this.refs.fileInput).addEventListener('change', evt => {
      this.handleDrop(evt.target);
    }, false);
  },
  handleDrop: function (dataTransfer) {
    let files = dataTransfer.files;
    if(files[0]){
      let size = files[0].size > 1000000 ? Math.round(files[0].size / 100000)/10 + " MB" : Math.round(files[0].size / 100)/10 + " KB";
      this.setState({file : files[0], fileName : files[0].name, fileSize : size});
      if(this.props.handleFiles){
        this.props.handleFiles(files);
      }
    }
  },
  browse(){
     React.findDOMNode(this.refs.fileInput).click();
  },
  render: function () {
    let styles = {
      default : {display : this.state.file || this.props.errorMsg ? 'none' : 'block'},
      info : {display : this.state.file && !this.props.errorMsg ? 'block' : 'none'},
      error : {display : this.props.errorMsg ? 'block' : 'none'}
    };
    return (
        <div>
          <div className={this.props.classProp} onClick={this.browse}>
            <FileDragAndDrop onDrop={this.handleDrop}>
              <Icon icon={this.props.defaultIcon} classProp="icon-normal bloc-centered margin-top-15px fill-color1" givenStyle={styles.default}/>
              <div className="font-10px text-centered margin-top-5px" style={styles.default}>Browse or Drag</div>
              <div src={this.props.image} style={styles.info} className="overflow-hidden padding-5px">
                <div className="font-10px">File : {this.state.fileName}</div>
                <div className="font-10px">Size : {this.state.fileSize}</div>
              </div>
              <div style={styles.error} className="padding-5px font-12px color5">
                {this.props.errorMsg}
              </div>
            </FileDragAndDrop>
          </div>
          <div className="font-12px text-centered width-120px margin-top-5px margin-bottom-5px">or</div>
          <div onClick={this.browse} className="pointer inline-block padding-right-40px padding-left-40px padding-top-5px padding-bottom-5px white-color background-color1 font-13px thin-text">
            Browse
          </div>
          <input type="file" ref="fileInput" name="files[]" className="hide"/>
        </div>
    );
  }
});

let SocialMediasDisplay = React.createClass({
  getInitialState() {
    return {
      social : this.props.social ? this.props.social : [],
      selected : 0
    };
  },
  select(social){
    this.setState({selected : social.id});
  },
  componentWillReceiveProps(nextProps){
    if(nextProps.social){
      this.setState({social : nextProps.social});
    }
  },
  componentDidUpdate(){
    let node = React.findDOMNode(this.refs.current);
    if(node){
      node.focus();
    }
  },
  render() {
    let url = '', name = '';
    let socialIcons = this.state.social.map((x, index) => {
        if(x.id === this.state.selected){
          url = x.url;
          name = x.name;
        }
        let additionalClasses = x.id === this.state.selected ? 'fill-color2 ' : '';
        if(x.id !== this.state.selected && x.url){
          additionalClasses += "fill-color1";
        } else if(x.id !== this.state.selected) {
          additionalClasses += "hide";
        }
        let innSelect = this.select.bind(this, x);

      return <Icon key={index} click={innSelect} icon={'svg-social-' + x.name} classProp={additionalClasses + " transition-all-ease-3 icon-mini margin-left-5px margin-right-5px hover-opacity-100 pointer"} />
    });

    return (
      <div className="padding-left-10px">
        <div>
          {socialIcons}
        </div>
        <div className="padding-left-5px padding-top-5px">
          <a href="{url}" target="_blank">{name}</a>
        </div>
      </div>
    );
  }
});

let SocialMedias = React.createClass({
  getInitialState() {
    let tmp = [];
    if(this.props.social){
      tmp = this.props.social.map(x => {
        return {
          id : x.id,
          name : x.name,
          selected : x.selected,
          url : x.url
        }
      });
    }
    return {social : tmp};
  },
  componentWillReceiveProps(nextProps){
    if(nextProps.social){
      let tmp = nextProps.social.map(x => {
        return {
          id : x.id,
          name : x.name,
          selected : x.selected,
          url : x.url
        }
      });
      this.setState({social : tmp});
    }
  },
  select(social){
    let tmp = this.state.social.map(x => {
      return {
        id : x.id,
        name : x.name,
        selected : social.name === x.name,
        url : x.url
      }
    });
    this.setState({social : tmp}, () => {
      let node = React.findDOMNode(this.refs.current);
      if(node && social.url){
        node.value = social.url;
      }
    });
  },
  inputChange(social, url){
    let tmp = this.state.social.map(x => {
      return {
        id : x.id,
        name : x.name,
        selected : x.selected,
        url : x.name === social.name ? url : x.url
      }
    });
    this.setState({social : tmp});
    if(this.props.updateSocial){
      this.props.updateSocial(tmp);
    }
  },
  componentDidUpdate(){
    let node = React.findDOMNode(this.refs.current);
    if(node){
      node.focus();
    }
  },
  render() {
    let socialIcons = this.state.social.map((x, index) => {
        let additionalClasses = x.selected ? 'fill-color1 ' : '';
        if(!x.selected && x.url){
          additionalClasses += "fill-color2";
        } else if(!x.selected){
          additionalClasses += "opacity-30";
        }
        let innSelect = this.select.bind(this, x);

      return <Icon key={index} click={innSelect} icon={'svg-social-' + x.name} classProp={additionalClasses + " transition-all-ease-3 icon-mini margin-left-5px margin-right-5px hover-opacity-100 pointer"} />
    });

    let socialInputs = this.state.social.map((x, index) => {
      let inChange = this.inputChange.bind(this, x),
          style = {
            display : x.selected ? "block" : "none"
          },
          focus = x.selected ? "focus" : undefined,
          ref = x.selected ? 'current' : '';
      return <GenericInput classProp='margin-left-5px' ref={ref} key={index} keyUp={inChange} placeholder={x.name} givenStyle={style} enterPressed={this.props.enterPressed} isFocus={focus}/>
    });
    return (
      <div>
        <div>
          {socialIcons}
        </div>
        <div>
          {socialInputs}
        </div>
      </div>
    );
  }
});

let Option = React.createClass({
  render() {
    return (
      <div className="radio inline-block">
        <label><input type="radio" value={this.props.value} name={this.props.optionName} />{this.props.label}</label>
      </div>
    );
  }
});

let TagDisplay = React.createClass({
  render() {
    return (
      <div className="inline-block tiny-tag background-color2" >
        <div className="inline-block"> 
          <span className="font-12px">{this.props.tag}</span>
        </div>
      </div>
    );
  }
});

let TagsBoxShow = React.createClass({
  getInitialState() {
    return {
      tags : this.props.tags ? this.props.tags : []
    };
  },
  componentWillReceiveProps(nextProp){
    this.setState({tags : nextProp.tags});
  },
  render() {
    let selectedTags = '';
    if(this.state.tags){
      selectedTags = this.state.tags.map((tag, index) => {
        return <TagDisplay key={index} tag={tag} selected='true' />
      });
    }
    let showSelectedTags = {'display' : 'block'};
    return (
        <div style={showSelectedTags}>
          <Icon icon='svg-tag' classProp='icon-mini fill-color1 v-align-sub width-20pc v-align-top margin-top-10px'/>
          <div className='margin-left-10px inline-block width-80pc v-align-top'>{selectedTags}</div>
        </div>
    );
  }
});

let Tag = React.createClass({
  getInitialState() {
    return {
      bcgClass : this.props.selected === 'true' ? "background-color2" : "background-color1"
    };
  },
  render() {
    var countStyle = {display : !this.props.tag.count ? 'none' : 'inline'};
    return (
      <div className={"inline-block tiny-tag " + this.state.bcgClass}>
        <div className="inline-block pointer" onClick={this.props.click}> 
          <span className="font-13px">{this.props.tag.label}</span>
          <span className="opacity-70 font-12px margin-left-2px" style={countStyle}>({this.props.tag.count})</span>
        </div>
        <div className="inline-block pointer tiny-tag-cross" onClick={this.props.remove}>x</div>
      </div>
    );
  }
});

let TagsBox = React.createClass({
  getInitialState() {
    return {tags : [], selectedTags : [], banned : []};
  },
  componentWillReceiveProps(nextProps){
    if(nextProps.allTags){
      this.onKeyUp('', true, nextProps.allTags);
      if(nextProps.preSelected && this.state.selectedTags.length === 0){
        this.preSelected(nextProps.preSelected, nextProps.allTags);
      }
    }
  },
  onKeyUp(value, force, forcedTags){
    let allTags = forcedTags ? forcedTags : this.props.allTags; 
    if(allTags && value.trim() !== this.state.value || force){
      let current = [];
      allTags.forEach( x => {
        if(x.label.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||  value.toLowerCase().indexOf(x.label.toLowerCase()) !== -1){
          let banned = false;
          this.state.banned.forEach(y => {
            banned = banned || x.label === y.label;
          });
          if(!banned){
            current.push(x);
          }
        }
      });
      current.sort((x, y) => x.count < y.count ? 1 : -1);
      if(current.length > 8){
        current = _.take(current, 8);
      }
      this.setState({tags : current, value : value});
    }
  },
  selectTag(tag){
    if(this.state.selectedTags.length < this.props.limit){
      let found = false;
      this.state.selectedTags.forEach(x => {
        if(x.label === tag.label){
          found = true;
        }
      });
      if(!found){
        let newSelected = this.state.selectedTags;
        newSelected.push(tag);
        this.setState({selectedTags : newSelected});
        this.props.updateTags(newSelected);
      }
    } else {
      this.setState({errorText : this.props.oolMessage});
    }
    var test = React.findDOMNode(this.refs.tagInputs).value = '';
  },
  deleteTag(tag){
    let banned = this.state.banned;
    banned.push(tag);
    this.setState({banned : banned}, () => {
      this.onKeyUp(this.state.value, true);
    });
  },
  deleteSelectedTag(tag){
    let newSelected = this.state.selectedTags;
    for (var i = 0; i < newSelected.length; i++) {
      if(newSelected[i].label === tag.label){
        newSelected.splice(i, 1);
        break;
      }
    }
    this.setState({selectedTags : newSelected, errorText : undefined});
    this.props.updateTags(newSelected);
  },
  forceTag(value){
    var tag = {label : value.trim().toLowerCase().replace(/\s/g, '-'), count : 0};
    this.props.allTags.forEach(x => {
      if(value === x.label){
        tag = x;
      }
    });
    if(tag.label.length){
      this.selectTag(tag);
    }
  },
  addButton(){
    this.forceTag(this.state.value);
  },
  preSelected(preSelect, tags) {
    let found = false;
      preSelect.map((tag, index) => {
        tags.forEach(x => {
          if(tag === x.label){
            this.state.selectedTags.push(x);
            found = true;
          }
        });
        if(!found)
          this.state.selectedTags.push({"label" : tag, "count": "0"});
        found = false;
      });
  },
  render() {
      let availableTags = this.state.tags.map((tag, index) => {
      let innSelect = this.selectTag.bind(this, tag),
          innDelete = this.deleteTag.bind(this, tag);
      return <Tag key={index} click={innSelect} tag={tag} selected='false' remove={innDelete}/>
    });
    let selectedTags = this.state.selectedTags.map((tag, index) => {
      let innDelete = this.deleteSelectedTag.bind(this, tag);
      return <Tag key={index} tag={tag} selected='true' remove={innDelete}/>
    });
    let showAvailableTags = {'display' : this.state.tags.length ? 'block' : 'none'},
        showSelectedTags = {'display' : this.state.selectedTags.length ? 'block' : 'none'},
        counterColor = this.state.selectedTags.length === +this.props.limit ? 'background-color2' : 'background-color1';
    return (
      <div>
        <div>
          <div className="inline-block width-70pc">
            <GenericInput ref="tagInputs" style="" classProp="full-width" keyUp={this.onKeyUp} enterPressed={this.forceTag} autofocus="focus" placeholder="e.g. scala, docker, front-end..." isFocus={this.props.isFocus}/>
            <MiniIconButton icon="svg-add2" labels="Add" className="background-color1 inline-block margin-top-5px" colorIcon="fill-white" onClick={this.addButton}/>
          </div>
          <div className={"inline-block rounded-radius counter-padding float-right transition-all-ease-3 " + counterColor}>
            <div className="inline-block white-color font-10px v-align-middle thin-text padding-bottom-10px">{this.state.selectedTags.length}</div>
            <div className="inline-block white-color font-20px v-align-middle thin-text">/</div>
            <div className="inline-block white-color font-10px v-align-middle thin-text padding-top-10px">{this.props.limit}</div>
          </div>
        </div>
        <div style={showAvailableTags}>
          <div className="font-9px margin-top-5px color1">Popular</div>
          <div>{availableTags}</div>
        </div>
        <div style={showSelectedTags}>
          <div className="font-9px color1">Selected</div>
          <div>{selectedTags}</div>
        </div>
        <div className="color5 font-13px">{this.state.errorText}</div>
      </div>
    );
  }
});

let MultiChoiceList = React.createClass({
  getInitialState() {
    let data = [];
    if(this.props.data){
      data = this.props.data.map(x => {
        return {id : x.id, item : x.name, selected : false}
      });
    }
    return {
      selected : [],
      data : data
    };
  },
  componentDidMount(){
    this.setDefaults();
  },
  componentWillReceiveProps(nextProp){
    if(nextProp.data){
      let data = nextProp.data.map(x => {
        let selected = !!(nextProp.defaultIds && nextProp.defaultIds.indexOf(x.id) !== -1);
        return {id : x.id, item : x.name, selected : selected}
      });
      this.setState({data : data});
    }
  },
  setDefaults(){
    if(this.props.defaultIds){
      this.props.data.forEach(x => {
        if(this.props.defaultIds.indexOf(x.id) !== -1){
          this.select({id : x.id, item : x.name, selected : true});
        }
      });
    }
  },
  select(block){
    let newData = this.state.data;
    newData.forEach(x => {
      if(x.id === block.id){
        x.selected = !x.selected;
      }
    })
    this.setState({data : newData});
    if(this.props.setValue) this.props.setValue(_.filter(newData, 'selected', true));
  },
  classSelected(block){
    let newData = this.state.data, innerClass = '';
    newData.forEach(x =>{
      if(x.id === block.id){
        innerClass = x.selected === true ? 'tiny-list-selected' : '';
      }
    })
    return innerClass;
  },
  iconClassSelected(block){
    let newData = this.state.data,
        innerIconClass = {"visibility" : 'hidden'};
    newData.forEach(x =>{
      if(x.id === block.id){
        innerIconClass = {"visibility" : x.selected === true ? 'visible' : 'hidden'};
      }
    })
    return innerIconClass;
  },
  render() {
    let array = this.state.data.map((block, index) => {
      let innSelect =  this.select.bind(this, block),
          innerClass = this.classSelected(block),
          innerIconClass = this.iconClassSelected(block);
      return <div key={index}>
              <div className='v-align-top padding-left-5px width-20px inline-block' style={innerIconClass}>
                <Icon icon='svg-check2' classProp='margin-top-5px icon-micro fill-color2' /> 
              </div> 
              <div className={"v-align-top inline-block tiny-list " + innerClass} onClick={innSelect}>{block.item}</div>
            </div>
    });
    return (
      <div>
        <div className='width-10pc inline-block v-align-top'>
          <Icon icon={this.props.icon} classProp={this.props.iconClassProp}/>
        </div>
        <div className='inline-block v-align-top padding-botton-1px'>
          {array}
        </div>
      </div>
    );
  }
});

let EditButton = React.createClass({
  render() {
    return (
      <div className='inline-block' onClick={this.props.onClick}>
        <Icon icon='svg-edit' classProp={'icon-mini margin-left-5px ' + this.props.colorIcon} />
      </div>
    );
  }
});

let Toggle = React.createClass({
  getInitialState() {
    return {};
  },
  toggle(){
    if(this.props.toggle){
      this.props.toggle();
    }
  },
  mouseOver(){
    if(!this.state.over){
      this.setState({over : true, opacity : 'opacity-0'}, () => {
        setTimeout(() => { 
          this.setState({opacity : 'opacity-100 transition-all-ease-5'});
        }, 10);
      });
    }
  },
  mouseOut(){
    if(this.state.over){
      this.setState({over : false});
    }
  },
  render() {
    if(this.props.choices){
      let styles = [{
        display : (this.props.choices[0].selected && !this.state.over) || (!this.props.choices[0].selected && this.state.over) ? "block" : "none"
      }, {
        display : (this.props.choices[1].selected && !this.state.over) || (!this.props.choices[1].selected && this.state.over) ? "block" : "none"
      }],
      classes = {
        icon : this.state.over ? this.props.overIconColor : this.props.iconColor,
        label : this.state.over ? this.props.overLabelColor : this.props.labelColor
      };
      return (
        <div onClick={this.toggle} className={"toggle pointer " + this.state.opacity} onMouseEnter={this.mouseOver} onMouseLeave={this.mouseOut}>
          <div style={styles[0]}>
            <div className={"inline-block text-centered v-align-top " + this.props.labelClasses + " " + classes.label} >{this.props.choices[0].label}</div>
            <div className={"inline-block " + this.props.iconClasses}>
              <Icon icon={this.props.choices[0].icon} classProp={this.props.iconClasses + " " + classes.icon}/>
            </div>
          </div>
          <div style={styles[1]}>
            <div className={"inline-block text-centered v-align-top " + this.props.labelClasses + " " + classes.label}>{this.props.choices[1].label}</div>
            <div className={"inline-block " + this.props.iconClasses}>
              <Icon icon={this.props.choices[1].icon} classProp={this.props.iconClasses + " " + classes.icon}/>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div>
        </div>
      );
    }
  }
});

let Bottombar = React.createClass({
  render() {
    return (
      <div className="background-contact white-color common-bottom-bar">
        <div className="container pure-g padding-top-40px padding-bottom-40px">
          <div className="pure-u-md-1-2 pure-u-sm-1-2">
            <div className="letter-spacing-1px margin-bottom-40px margin-left-5px">
              Contact & feedback
            </div>
            <div className="margin-left-5px">
              <a href="mailto:frank@narwhal.co.nz">
                <div className="transition-all-ease-3 pointer opacity-70 hover-opacity-100">
                  <div className="inline-block v-align-top thin-text font-18px margin-right-5px">@</div>
                  <div className="padding-top-3px inline-block v-align-top thin-text font-14px">Frank Bassard</div>
                </div>
              </a>
              <a href="mailto:mohamed@narwhal.co.nz">
                <div className="transition-all-ease-3 pointer opacity-70 hover-opacity-100 margin-top-10px">
                  <div className="inline-block v-align-top thin-text font-18px margin-right-5px">@</div>
                  <div className="padding-top-3px inline-block v-align-top thin-text font-14px">Mohamed Mokhtari</div>
                </div>
              </a>
            </div>
          </div>
          <div className="pure-u-md-1-2 pure-u-sm-1-2">
            <div className="letter-spacing-1px margin-bottom-40px">
              Social
            </div>
            <div>
              <div className="center-hover-show">
                <a href="">
                  <Icon icon='svg-social-facebook' classProp='icon-mini fill-white'/>
                  <div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Facebook</div>
                </a>
              </div>
              <div className="margin-top-10px center-hover-show">
                <a href="">
                  <Icon icon='svg-social-twitter' classProp='icon-mini fill-white'/>
                  <div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Twitter</div>
                </a>
              </div>
              <div className="margin-top-10px center-hover-show">
                <a href="">
                  <Icon icon='svg-social-google-plus' classProp='icon-mini fill-white'/>
                  <div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Google +</div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

let Paginator = React.createClass({
  before(){
    if(this.props.before){
      this.props.before();
    }
  },
  after(){
    if(this.props.after){
      this.props.after();
    }
  },
  goTo(value){
    if(this.props.goTo){
      this.props.goTo(value);
    }
  },
  render() {
    let allPages = [],
        showAll = this.props.page && this.props.pages && this.props.pages > 1 ? "" : "hide" ;
    if(this.props.pages && this.props.pages > 0){
      for (var i = 0; i < this.props.pages; i++) {
        allPages[i] = i + 1;
      }
    }
    while(allPages.length > 7){
        if(allPages[allPages.length - 1] - this.props.page > this.props.page - allPages[0]){
          allPages.pop();
        } else {
          allPages.shift();
        }
    }

    let width = "width-" + (40 + 20*allPages.length) + "px";
    let innPages = allPages.map((page, index) => {
      let innGoto = this.goTo.bind(this, page),
          border = this.props.page && page == this.props.page ? 'thin-border-top-color1' : 'thin-border-top';

      return (
        <div key={index} className={"inline-block pointer padding-left-5px padding-right-5px " + border} onClick={innGoto}>
          {page}
        </div>
      )
    });

    return (
      <div className={"bloc-centered " + width +  " " + showAll}>
        <div className="inline-block">
          <Icon icon="svg-arrow-l-2" click={this.before} classProp="fill-color1 icon-micro margin-right-5px pointer"/>
        </div>
        {innPages}
        <div className="inline-block">
          <Icon icon="svg-arrow-r-2" click={this.after} classProp="fill-color1 icon-micro margin-left-5px pointer"/>
        </div>
      </div>
    );
  }
});

module.exports = {
	MailInput : MailInput,
	PwdInput : PwdInput,
  GenericInput : GenericInput,
	ValidateButton : ValidateButton,
	Label : Label,
	Icon : Icon,
  IconButton : IconButton,
  MiniIconButton : MiniIconButton,
  ErrorBox : ErrorBox,
  ProgressBar : ProgressBar,
  CustomSelect : CustomSelect,
  TinyList : TinyList,
  Textarea : Textarea,
  DragAndDropImage : DragAndDropImage,
  SocialMediasDisplay : SocialMediasDisplay,
  SocialMedias : SocialMedias,
  Option : Option,
  TagsBoxShow : TagsBoxShow, 
  Tag : Tag,
  TagsBox : TagsBox,
  DragAndDrop : DragAndDrop,
  MultiChoiceList : MultiChoiceList,
  EditButton : EditButton,
  Toggle : Toggle,
  Bottombar : Bottombar,
  Paginator : Paginator
};