"use strict";

let empprofile = require('../profiles/employer.js'),
	requests = require('../../services/employer-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	employerCommon = require('./common.js');

let Empprofile = React.createClass({
	getInitialState() {
	   	return {
	   		employer : storage.get('employer')
	   	};
	},
	componentDidMount(){
		let emp = storage.get('employer');
		requests.login(emp.email, emp.password, emp => {
			emp.social.forEach(soc => {
				properties.getSocial(soc.id, bundle => {
					soc.name = bundle.name;
				});
			});
			this.setState({employer : emp});
		});
	},
	render() {
		return (
			<div>
				<employerCommon.Topbar forceRoute="employer/profile"/>
				<empprofile.Profile employer={this.state.employer} edit="true"/>
			</div>
		);
	}
});

module.exports = {
	Empprofile : Empprofile
};