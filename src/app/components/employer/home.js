"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	employerCommon = require('./common.js'),
	requests = require('../../services/employer-requests.js'),
	JobRequests = require('../../services/jobs-requests.js'),
	properties = require('../../services/properties.js'),
	dates = require('../../services/dates.js'),
	storage = require('../../services/storage.js'),
	Rating = require('../../../../node_modules/Rating/js/rating.js');

let HomePage = React.createClass({
	getInitialState() {
		return {
			employer : storage.get('employer')
		};
 	},
 	componentDidMount(){
 		requests.getAllJobs(this.state.employer.email, (success, jobs) => {
 			if(success && jobs){
 				this.setState({jobs : jobs});
 			}
 		});
 	},
 	changeJob(job){
 		requests.getJobSeekers(job._id, (err, seekers) => {
 			seekers.forEach(sk => {
 				sk.favored = job.favored.indexOf(sk._id) !== -1;
 			});
 			this.setState({selectedJob : job, seekers : seekers});
 		});
 	},
 	favorSeeker(seeker){
 		requests.favor(this.state.selectedJob._id, seeker._id, x => {
 			let job = this.state.selectedJob;
 			job.favored.push(seeker._id);
 			this.changeJob(job);
 		});
 	},
 	unfavorSeeker(seeker){
 		requests.unfavor(this.state.selectedJob._id, seeker._id, x => {
 			let job = this.state.selectedJob,
 				index = job.favored.indexOf(seeker._id);
 			if(index !== -1){
 				job.favored.splice(index, 1);
 			}
 			this.changeJob(job);
 		});
 	},
 	discardSeeker(seeker){
 		requests.discard(this.state.selectedJob._id, seeker._id, x => {
 			this.changeJob(this.state.selectedJob);
 		});
 	},
 	toggleVisible(job){
 		job.visible = !job.visible;
 		JobRequests.update(job._id, job, x => {
 			if(x){
 				this.componentDidMount();
 			}
 		});
 	},
	render() {
		return (
			<div className="main-container">
				<employerCommon.Topbar forceRoute='employer/home'/>
				<div id="container" className='pure-g padding-top-10px'>
					<div className="pure-u-md-1-3">
						<JobList jobs={this.state.jobs} changeJob={this.changeJob} toggleVisible={this.toggleVisible}/>
					</div>
					<div className="pure-u-md-2-3">
						<JobSeekeers job={this.state.selectedJob} seekers={this.state.seekers} favorSeeker={this.favorSeeker} discardSeeker={this.discardSeeker} unfavorSeeker={this.unfavorSeeker}/>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let JobSeekeers = React.createClass({
	getInitialState() {
		return {
			seekers : [],
			pages : 1,
			page : 1
		};
 	},
 	componentWillReceiveProps(nextProp){
 		if(nextProp.seekers){
 			this.setState({seekers : nextProp.seekers, pages : Math.ceil(nextProp.seekers.length/5)});	
 		}
 	},
 	componentDidUpdate(){
 		this.state.seekers.map((seeker, index) => {
 			let ref = React.findDOMNode(this.refs["rating-" + index]);
 			while (ref.firstChild) {
				ref.removeChild(ref.firstChild);
			}
 			new Rating(properties.ratingProps("rating-" + index, Math.round(seeker.rating)));
 		});
 	},
 	favor(seeker){
 		if(this.props.favorSeeker){
 			this.props.favorSeeker(seeker);
 		}
 	},
 	discard(seeker){
 		if(this.props.discardSeeker){
 			this.props.discardSeeker(seeker);
 		}
 	},
 	unfavor(seeker){
 		if(this.props.unfavorSeeker){
 			this.props.unfavorSeeker(seeker);
 		}
 	},
 	details(seeker){
 		yarr(properties.routes.employer.seekprofile.replace(':id', seeker._id));
 	},
 	pageBefore(){
 		let page = this.state.page;
 		this.setState({page : page - 1 > 0 ? page - 1 : 1});
 	},
 	pageAfter(){
 		let page = this.state.page;
 		this.setState({page : page + 1 <= this.state.pages ? page + 1 : this.state.pages});
 	},
 	pageGoTo(page){
 		this.setState({page : page});
 	},
	render() {
		let seekers = this.state.seekers.map((seeker, index) => {
			let readableXp = '', readableDegree = '', 
				favorStyle = {display : seeker.favored ? 'none' : 'inline-block'},
				unFavorStyle = {display : seeker.favored ? 'inline-block' : 'none'},
				innFavor = this.favor.bind(this, seeker),
				innDiscard = this.discard.bind(this, seeker),
				innUnfavor = this.unfavor.bind(this, seeker),
				innDetails = this.details.bind(this, seeker),
				onPage = index < 5*this.state.page && index >= (this.state.page-1)*5 ? '' : 'hide';
			properties.getXp(seeker.xp, x => {
				readableXp = x.name;
			});
			properties.getDegree(seeker.education, x => {
				readableDegree = x.name;
			});
			return (
				<div key={index} className={"pure-g padding-10px thin-border-bottom " + onPage}>
					<div>
						<div className="pure-u-md-2-3">
							<div className="font-18px margin-bottom-5px">{seeker.name}</div>
							<common.TagsBoxShow tags={seeker.skills} />
							<div className="margin-top-5px">
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-degree" />
								<span className="font-12px line-height-20px v-align-bottom">{readableDegree}</span>
							</div>
							<div className="margin-top-10px">
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-experience" />
								<span className="font-12px line-height-20px v-align-bottom">{readableXp}</span>
							</div>
							
						</div>
						<div className="pure-u-md-1-3">
							<div id={"rating-" + index} ref={"rating-" + index} className="float-right margin-top-10px"></div>
						</div>
					</div>
					<div>
						<div className="height-35px">
							<common.MiniIconButton icon="svg-details" labels="Details" className="background-color1 inline-block" colorIcon="fill-white" onClick={innDetails}/>
							<common.MiniIconButton icon="svg-close" labels="Discard" className="background-color1 inline-block float-right" colorIcon="fill-white" onClick={innDiscard}/>
							<div style={favorStyle} className="float-right">
								<common.MiniIconButton icon="svg-check" labels="Interested" className="background-color1 inline-block margin-right-5px" colorIcon="fill-white" onClick={innFavor}/>
							</div>
							<div style={unFavorStyle} className="float-right">
								<common.MiniIconButton icon="svg-close" labels="Uninterested" className="background-color5 inline-block margin-right-5px" colorIcon="fill-white" onClick={innUnfavor}/>
							</div>
						</div>
					</div>
				</div>
			)
		});
		let inactiveFallback = {
			display : this.props.job && !this.props.job.active? 'block' : 'none'
		}, invisibleFallback = {
			display : inactiveFallback.display === 'none' && this.props.job && this.props.job.visible === false ? 'block' : 'none'
		}, emptyFallback = {
			display : this.state.seekers.length || inactiveFallback.display === 'block' || invisibleFallback.display === 'block' ? 'none' : 'block'
		}, seekersBlock = {
			display : inactiveFallback.display === 'none' && emptyFallback.display === 'none' && invisibleFallback.display === 'none' ? 'block' : 'none'
		};
		return (
			<div>
				<h3 className="no-margin-bottom no-margin-left no-margin-right padding-10px thin-border-bottom">Seekers</h3>
				<div style={seekersBlock}>
					{seekers}
					<div className="margin-top-10px">
						<common.Paginator page={this.state.page} pages={this.state.pages} before={this.pageBefore} after={this.pageAfter} goTo={this.pageGoTo}/>
					</div>
				</div>
				<div className="text-centered margin-top-20px font-14px" style={emptyFallback}>Sorry, we didn't find any job seeker fitting for this job</div>
				<div className="text-centered margin-top-20px font-14px" style={inactiveFallback}>This job is currently inactive, activate it to see candidates.</div>
				<div className="text-centered margin-top-20px font-14px" style={invisibleFallback}>This job is currently invisible, change visibility to see candidates.</div>
			</div>
		);
	}
});

let JobList = React.createClass({
	getInitialState() {
		return {
			"selected" : -1, 
			jobs : [], 
			showNewJob : false,
			page : 1,
			pages : 1
		};
 	},
 	componentWillReceiveProps(nextProp){
 		nextProp.jobs.sort((x, y) => x.rank > y.rank);
 		nextProp.jobs.forEach((x, index) => {
 			x.rank = index + 1;
 			this.craftJob(x);
 		});
 		this.setState({jobs : nextProp.jobs, pages : Math.ceil(nextProp.jobs.length/4)}, () => {
 			if(this.state.selected === -1 && this.state.jobs.length){
 				this.selectJob(0, this.state.jobs[0]);
 			}
 		});
 	},
 	toggleVisible(job){
 		if(this.props.toggleVisible){
 			this.props.toggleVisible(job);
 		}
 	},
 	craftJob(job){
		job.choices = [{
			icon : 'svg-visible',
			label : 'Visible',
			selected : job.visible
		}, {
			icon : 'svg-invisible',
			label : 'Invisible',
			selected : !job.visible
		}];
		job.iconColor = job.visible ? "fill-color1" : "fill-color5";
		job.overIconColor = job.visible ? "fill-color5" : "fill-color1";
		job.labelColor = job.visible ? "color1" : "color5";
		job.overLabelColor = job.visible ? "color5" : "color1";
 	},
 	selectJob(index, job){
 		this.setState({"selected" : index});
 		this.props.changeJob(job);
 	},
 	toggleJobBox(){
 		React.findDOMNode(this.refs.jobTitle).value = '';
 		this.setState({"showNewJob" : !this.state.showNewJob}, () => {
 			if(this.state.showNewJob){
 				React.findDOMNode(this.refs.jobTitle).focus();
 			}
 		});
 	},
 	addJob(){
 		let title = React.findDOMNode(this.refs.jobTitle).value,
 			emp = storage.get('employer'),
 			rank = 1;
 		this.state.jobs.forEach(x => {
 			rank = x.rank >= rank ? x.rank + 1 : rank;
 		});
 		requests.createJob({
 			title : title,
 			rank : rank
 		}, (x, job) => {
 			if(x){
 				yarr(properties.routes.employer.jobs.replace(':id', job._id));
 			}
 		});
 		this.toggleJobBox();
 	},
 	jobUp(job){
 		let tmpJobs = this.state.jobs;
 		if(job._id !== tmpJobs[tmpJobs.length -1]._id){
 			let base = job.rank,
				jobs = this.state.jobs.map(x => {
				if(x._id === job._id){
					x.rank = base + 1;
				} else if(x.rank === base + 1){
					x.rank = base; 
				}
				return x;
			});
			jobs.sort((x, y) => x.rank > y.rank);
			this.setState({jobs : jobs}, () => {
				let array = this.state.jobs.map(x => {
					return {
						_id : x._id,
						rank : x.rank
					}
				});
				requests.sortJobs(array);
			});
 		}
 	},
 	jobDown(job){
 		let tmpJobs = this.state.jobs;
 		if(job._id !== tmpJobs[0]._id){
 			let base = job.rank,
				jobs = this.state.jobs.map(x => {
				if(x._id === job._id){
					x.rank = base - 1;
				} else if(x.rank === base - 1){
					x.rank = base + 1; 
				}
				return x;
			});
			jobs.sort((x, y) => x.rank > y.rank);
			this.setState({jobs : jobs}, () => {
				let array = this.state.jobs.map(x => {
					return {
						_id : x._id,
						rank : x.rank
					}
				});
				requests.sortJobs(array);
			});
 		}
 	},
 	edit(job, event){
 		event.stopPropagation();
 		yarr(properties.routes.employer.jobs.replace(':id', job._id));
 	},
 	pageBefore(){
 		let page = this.state.page;
 		this.setState({page : page - 1 > 0 ? page - 1 : 1});
 	},
 	pageAfter(){
 		let page = this.state.page;
 		this.setState({page : page + 1 <= this.state.pages ? page + 1 : this.state.pages});
 	},
 	pageGoTo(page){
 		this.setState({page : page});
 	},
	render() {
		let jobs = this.state.jobs.map((job, index) => {
			let unformatted = job.started ? dates.daysLeft(job.started) : undefined,
				innToggle = this.toggleVisible.bind(this, job),
				innSelect = this.selectJob.bind(this, index, job),
				innJobUp = this.jobUp.bind(this, job),
				innJobDown = this.jobDown.bind(this, job),
				innEdit = this.edit.bind(this, job),
				bcgClass = this.state.selected === index ? "background-white " : "",

				displayIncomplete = job.complete ? 'hide' : '',
				displayInvactive = job.complete && !job.active ? '' : 'hide',
				displayVisibility = job.complete && job.active ? '' : 'hide',

				showDate = unformatted ? '' : 'hide',
				showDateFallback = unformatted ? 'hide' : '',

				onPage = index < 4*this.state.page && index >= (this.state.page-1)*4 ? '' : 'hide';
		return (
			<div key={index} className={"padding-10px pure-g thin-border-bottom thin-border-right " + bcgClass + " " + onPage} onClick={innSelect}>
				<div className="pure-u-md-1-2">
					<div className="font-18px letter-spacing-1px">{job.title}</div>

					<div className={"font-11px thin-text opacity-80 " + showDate}>{unformatted} days left</div>
					<div className={"font-11px thin-text opacity-80 " + showDateFallback}>Not started yet</div>

					<div className="margin-top-15px color1 margin-left-5px thin-border-left">
						<div className="inline-block font-11px text-right margin-left-5px">
							<div>{job.views}</div>
							<div>{job.matches}</div>
							<div>{job.invitesIn}</div>
							<div>{job.invitesOut}</div>
						</div>
						<div className="inline-block font-11px margin-left-5px">
							<div>Views</div>
							<div>Matches</div>
							<div>Invites received</div>
							<div>Invites sent</div>
						</div>
					</div>
					<div className="margin-top-5px">
						<common.MiniIconButton onClick={innEdit} icon="svg-edit" labels="Edit" className="background-color1" colorIcon="fill-white"/>
					</div>
				</div>
				<div className="pure-u-md-1-2 margin-top-5px">
					<div className="float-right margin-right-10px">

						<div className={displayVisibility}>
							<common.Toggle choices={job.choices} toggle={innToggle} iconColor={job.iconColor} overIconColor={job.overIconColor} iconClasses='icon-mini ' labelColor={job.labelColor} overLabelColor={job.overLabelColor} labelClasses='font-12px margin-top-1px margin-right-5px'/>
						</div>

						<div className={"pointer margin-top-5px " + displayInvactive} onClick={innEdit}>
							<common.Icon icon="svg-close" classProp="icon-mini fill-color5 pointer hover-fill-color5"/>
							<div className="inline-block color5 v-align-top font-14px margin-left-5px">Inactive</div>
						</div>

						<div className={"pointer margin-top-5px " + displayIncomplete} onClick={innEdit}>
							<common.Icon icon="svg-warning" classProp="icon-mini fill-color5 pointer hover-fill-color5"/>
							<div className="inline-block color5 v-align-top font-14px margin-left-5px">Incomplete</div>
						</div>

						<div className="margin-top-20px height-20px" onClick={innJobDown}>
							<common.Icon icon="svg-mini-up" classProp="icon-mini fill-color1 float-right pointer hover-fill-color2"/>
						</div>
						<div className="margin-top-5px height-20px">
							<common.Icon icon="svg-move" classProp="icon-mini fill-color1 float-right move hover-fill-color2" />
						</div>
						<div className="margin-top-5px height-20px" onClick={innJobUp}>
							<common.Icon icon="svg-mini-down" classProp="icon-mini fill-color1 float-right pointer hover-fill-color2"/>
						</div>
					</div>
				</div>
			</div>
			)
		});
		let styles = {
			newJobButton : {
				display : this.state.showNewJob ? "none" : "inline-block"
			},
			newJobInput : {
				display : this.state.showNewJob ? "block" : "none"
			},
			noJob : {
				display : !this.state.showNewJob && (!this.state.jobs || !this.state.jobs.length) ? "block" : "none"
			}
		}
		return (
			<div>
				<div className="thin-border-bottom">
					<h3 className="inline-block no-margin-bottom no-margin-left no-margin-right padding-10px ">Your jobs</h3>
					<div className="inline-block thin-text font-14px pointer white-color background-color1 radius-5px padding-left-5px padding-right-10px padding-top-2px padding-bottom-3px margin-left-5px" style={styles.newJobButton} onClick={this.toggleJobBox}>
						+ Create
					</div>
				</div>
				<div className="padding-10px" style={styles.newJobInput}>
					<input ref="jobTitle" type="text" className="padding-left-10px margin-bottom-5px full-width background-white" placeholder="Job title"></input>
					<common.MiniIconButton icon="svg-add2" labels="Create" className="background-color1" colorIcon="fill-white" onClick={this.addJob}/>
					<common.MiniIconButton icon="svg-close" labels="Cancel" className="background-color5 margin-left-10px" colorIcon="fill-white" onClick={this.toggleJobBox}/>
				</div>
				<div className="margin-left-10px margin-top-10px padding-10px width-158px background-color1 white-color radius-5px" style={styles.noJob}>
					<div className="arrow-up"></div>
					<div className="font-14px margin-bottom-2px margin-neg-top-10px">No job yet !</div>
					<div className="font-12px thin-text opacity-90">
						Create your <span className="underlined pointer" onClick={this.toggleJobBox}>first job</span> and start getting matches
					</div>
				</div>
				{jobs}
				<div className="margin-top-10px">
					<common.Paginator page={this.state.page} pages={this.state.pages} before={this.pageBefore} after={this.pageAfter} goTo={this.pageGoTo}/>
				</div>
			</div>
		);
	}
});

module.exports = {
	HomePage : HomePage
};