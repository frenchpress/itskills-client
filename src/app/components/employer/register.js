"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	employerCommon = require('./common.js'),
	requests = require('../../services/employer-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	Stepper = require('../../../../node_modules/SimpleSteps/js/stepper.js'),
	auth = require('../../services/auth.js'),
	steps = ['Basic info', 'Details', 'Media', 'Completed'];

let Reg1Form = React.createClass({
	getInitialState() {
   	 return {
   	 		stateMail : false,
   	 		statePwd : false,
   	 		submitable : false,
   	 		mailErrVisible : false,
   	 		pwdErrVisibility : false,
   	 		nameErrVisibility : false,
   	 		formErrVisibility : false,
   	 		progressBarVisibility : {
   	 			visibility : 'hidden'
   	 		},
   	 		strength : 0,
   	 		color : '',
   	 		mailErrorLabel : "Invalid email address",
		};
 	},
	mailUpdate(valid, mail){
		if(valid){
			requests.isMailAvailable(mail, (ans) => {
				if(ans){
					this.setState({stateMail : true, submitable : this.state.statePwd, mailErrVisible: false, inputMail: mail}); 
				} else {
					this.setState({stateMail : false, submitable : false, mailErrVisible: true, mailErrorLabel : "Email already in use"});
				}
			});
		} else{
			this.setState({stateMail : false, submitable : false, mailErrVisible: true, mailErrorLabel : "Invalid email address"});
		}
	},
	pwdUpdate(valid, password){
		if(valid){
			this.setState({statePwd : true, submitable : this.state.stateMail, pwdErrVisibility: false, progressBarVisibility : {visibility : 'hidden'}, inputPwd: password});
		} else{
			this.setState({statePwd : false, submitable : false, pwdErrVisibility: true, progressBarVisibility : {visibility : 'hidden'}});
		}
	},
	pwdStrength(strength){
		let colors = ['#FF530D', '#293540', '#79BDE0'], use = 0;
		if(strength > 66){
			use = 2;
		} else if(strength > 33){
			use = 1;
		}
		this.setState({progressBarVisibility : {visibility : 'visible'}, strength : strength, color : colors[use]});
	},
	testEnter(event){
		if(event.which === 13){
			this.register();
		}
	},
	register(){
		if(this.state.submitable){
			requests.create(this.state.inputMail, this.state.inputPwd, (ans, error) => {
				if(ans){
					storage.set('employer', {email : this.state.inputMail, password : this.state.inputPwd, name : this.state.inputName});
					yarr(properties.routes.employer.register2);
				} else {
					this.setState({formError: error, formErrVisibility : true});
				}
			});
		}
	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className = 'pure-u-md-1-2 offset-md-1-4 pure-u-sm-3-4 offset-sm-1-8 margin-top-40px'>
						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">Sign up</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">Take 5 minutes to create you account.</div>
						</div>

						<form className='padding-bottom-30px' onKeyUp={this.testEnter}>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels='E-mail *'  />
								<div>
									<div className='margin-left-5pc width-10pc inline-block padding-top-3px'>
										<common.Icon icon='svg-mail' classProp='icon-mini fill-color1'/>
									</div>
									<common.MailInput mailUpdate={this.mailUpdate} classProp='width-80pc margin-bottom-10px v-align-top' isFocus='focus' placeholder="e.g. your@mail.co.nz"/>
									<common.ErrorBox labels={this.state.mailErrorLabel} visible={this.state.mailErrVisible}/>
								</div>
							</div>

							<div className='padding-bottom-10px'>	
								<common.Label labels='Password *' />
								<div>
									<div className='margin-left-5pc width-10pc inline-block padding-top-3px'>
										<common.Icon icon='svg-password' classProp='icon-mini fill-color1'/>
									</div>
									<common.PwdInput pwdUpdate={this.pwdUpdate} pwdStrength={this.pwdStrength} enterPressed={this.register} classProp='width-80pc margin-bottom-10px v-align-top' placeholder="password : 6 characters min"/>
									<common.ErrorBox labels='Your password must have at between 6 and 30 characters' visible={this.state.pwdErrVisibility}/>
									<div className = 'pure-g' style={this.state.progressBarVisibility}>
										<div className = 'pure-u-md-1-6 text-right padding-right-10px font-12px'>
											{this.state.strength}%
										</div>
										<div className = 'pure-u-md-2-3'>
											<common.ProgressBar color={this.state.color} percent={this.state.strength}/>
										</div>
									</div>

								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate={this.state.submitable} labels={'Next'} classProp="margin-bottom-20px"/>
								<common.ErrorBox labels={this.state.formError} visible={this.state.formErrVisibility}/>
							</div>
						</form>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Reg2Form = React.createClass({
	getInitialState() {
		return {
			employer : storage.get('employer'),
			errorCompanyNameVisibility : false,
			errorAddressVisibility : false,
			errorPhoneVisibility : false,
			submitable : false
		};
 	},
 	componentDidMount(){
 		let stepper = new Stepper(properties.stepperProps("stepper", steps, 0));
 		stepper.changeStep(1);
 	},
 	companyNameUpdate(bool, value, cb){
 		let submitPreReqs = this.state.address && this.state.phone;
 		this.setState({errorCompanyNameVisibility : !bool, companyName : value, submitable : submitPreReqs && bool}, cb);
 	},
 	addressUpdate(bool, value, cb){
 		let submitPreReqs = this.state.companyName && this.state.phone;
 		this.setState({errorAddressVisibility : !bool, address : value, submitable : submitPreReqs && bool}, cb);
 	},
 	phoneUpdate(bool, value, cb){
 		let submitPreReqs = this.state.companyName && this.state.address;
 		this.setState({errorPhoneVisibility : !bool, phone : value, submitable : submitPreReqs && bool}, cb);
 	},
 	websiteUpdate(bool, value, cb){
 		this.setState({website : value}, cb);
 	},
 	phoneEnter(value){
 		this.phoneUpdate(!!value.length, value, function(){
 			this.register();
 		});
 	},
 	webEnter(value){
 		this.websiteUpdate(!!value.length, value, function(){
 			this.register();
 		});
 	},
 	testEnter(event){
		if(event.which === 13){
			this.register();
		}
	},
 	register(){
 		if(this.state.submitable){
 			let data = {
	 			companyName : this.state.companyName,
				address : this.state.address,
				phone : this.state.phone
	 		};
	 		if(this.state.website){
	 			data.website = this.state.website;
	 		}
 			requests.update(this.state.employer.email, data, (ans, error) =>{
 				let emp = storage.get('employer');
 				emp.companyName = this.state.companyName;
 				emp.address = this.state.address;
 				emp.phone = this.state.phone;
 				storage.set('employer', emp);
 				yarr(properties.routes.employer.register3);
 			});
 		}
 	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div id="stepper" className='margin-top-100px inner-svg-centered'></div>
					</div>
					<div className = 'pure-u-md-1-2 pure-u-sm-3-4 margin-top-40px'>
						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">Create your profile</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">Welcome among the pack. We will make the most of every piece of information you provite to target awesome profiles for you.</div>
						</div>
						<form className='padding-left-30px padding-right-30px padding-bottom-30px' onKeyUp={this.testEnter}>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Company name *'}  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-username' classProp='icon-mini fill-color1'/>
									</div>
									<common.GenericInput compulsory='true' exists={this.companyNameUpdate} classProp='width-80pc margin-bottom-10px v-align-top' isFocus='focus' placeholder="your brand"/>
									<common.ErrorBox labels='Company name is required' visible={this.state.errorCompanyNameVisibility}/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Address *'}  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-address' classProp='icon-mini fill-color1'/>
									</div>
									<common.GenericInput compulsory='true' exists={this.addressUpdate} classProp='width-80pc margin-bottom-10px v-align-top' placeholder="HQ/offices address"/>
									<common.ErrorBox labels='Address is required' visible={this.state.errorAddressVisibility}/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Phone number *'}  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-number' classProp='icon-mini fill-color1'/>
									</div>
									<common.GenericInput compulsory='true' exists={this.phoneUpdate} enterPressed={this.phoneEnter} classProp='width-80pc margin-bottom-10px v-align-top' placeholder="+64x/0x..."/>
									<common.ErrorBox labels='Phone number is required' visible={this.state.errorPhoneVisibility}/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Company\'s website'}  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-web' classProp='icon-mini fill-color1'/>
									</div>
									<common.GenericInput compulsory='true' exists={this.websiteUpdate} enterPressed={this.webEnter} classProp='width-80pc margin-bottom-10px v-align-top' placeholder="public facing website"/>
								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate={this.state.submitable} labels={'Next'} classProp="margin-bottom-20px"/>
							</div>	
						</form>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Reg3Form = React.createClass({
	getInitialState() {
		return {
			employer : storage.get('employer'),
			defaultId : 2
		};
 	},
 	componentWillMount(){
 		properties.getIndustries(x => {
 			this.setState({industries : x});
 		});
 		properties.getCompanySizes(x => {
 			this.setState({companySizes : x});
 		});
 	},
 	componentDidMount(){
 		let stepper = new Stepper(properties.stepperProps("stepper", steps, 1));
 		stepper.changeStep(2);
 	},
 	setIndustry(industry){
 		let submitable = this.state.description && this.state.size;
 		this.setState({industry : industry, submitable : submitable});
 	},
 	setSize(size){
 		let submitable = this.state.description && this.state.industry;
 		this.setState({size : size, defaultId: size.id, submitable : submitable});
 	},
 	setDescription(description){
 		let submitable = this.state.industry && this.state.size;
 		this.setState({description : description, submitable : submitable});
 	},
 	register(){
 		if(this.state.submitable){
 			requests.update(this.state.employer.email, {
	 			size : this.state.size.id,
				industry : this.state.industry,
				description : this.state.description
	 		}, (ans, error) => {
 				yarr(properties.routes.employer.register4);
 			});
 		}
 	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div id="stepper" className='margin-top-100px inner-svg-centered'></div>
					</div>
					<div className = 'pure-u-md-1-2 pure-u-sm-3-4 margin-top-40px'>
						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">Tell us a little more about {this.state.employer.companyName}</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">Help us targeting the best prospects for {this.state.employer.companyName}</div>
						</div>
						<div className='padding-left-30px padding-right-30px padding-bottom-30px'>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Industry *'} />
								<div className='margin-left-5pc width-90pc'>
									<common.CustomSelect setValue={this.setIndustry} data={this.state.industries} icon='svg-industry' iconClassProp="icon-mini fill-color1" classProp='width-85pc margin-bottom-10px v-align-top' isFocus="focus"/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Size *'} />
								<div className='margin-left-5pc width-90pc'>
									<common.TinyList defaultId={this.state.defaultId} setValue={this.setSize} data={this.state.companySizes} icon='svg-size1' iconClassProp="icon-mini fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' isFocus="focus"/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Description *'} />
								<div className='margin-left-5pc width-90pc'>
									<div className='width-10pc inline-block v-align-top'>
										<common.Icon icon='svg-details' classProp='icon-mini fill-color1'/>
									</div>
									<div className='width-90pc inline-block v-align-top'>
										<common.Textarea setValue={this.setDescription} compulsory='true' enterPressed={this.webEnter} classProp='width-90pc margin-bottom-10px v-align-top' rows='5' placeholder="Why is your company the best one ?"/>
									</div>
								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate={this.state.submitable} labels={'Next'} classProp="margin-bottom-20px"/>
							</div>
						</div>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Reg4Form = React.createClass({
	getInitialState() {
		return {
			employer : storage.get('employer')
		};
 	},
 	componentWillMount(){
 		properties.getSocials(x => {
 			this.setState({social : x});
 		});
 	},
 	componentDidMount(){
 		let stepper = new Stepper(properties.stepperProps("stepper", steps, 2));
 		stepper.changeStep(3);
 	},
 	handleUpload(files){
 		if(files[0]){
 			requests.addPicture(this.state.employer.email, files[0], (x, y)=> {
 				if(!x){
 					this.setState({fileErrMsg : y});
 				} else if(this.state.fileErrMsg){
 					this.setState({fileErrMsg : undefined});
 				}
 			});
 			if(files[0].size <= 1000000){
 				let reader = new FileReader();

				reader.onload = (theFile => {
					return e => {
						this.setState({image : e.target.result});
					};
				})(files[0]);

				reader.readAsDataURL(files[0]);
 			}
 		}
 	},
 	updateSocial(social){
 		this.setState({social : social});
 	},
 	testEnter(event){
		if(event.which === 13){
			this.register();
		}
	},
 	register(){
 		let emp = storage.get('employer');
 		if(this.state.social){
 			requests.update(this.state.employer.email, {
	 			social : this.state.social
	 		}, (ans, error) => {
	 			if(ans){
	 				auth.employer.login(emp.email, emp.password, () => {
	 					yarr(properties.routes.employer.home);
	 				});
	 			}
			});
 		} else {
 			auth.employer.login(emp.email, emp.password, () => {
				yarr(properties.routes.employer.home);
			});
 		}
 	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div id="stepper" className='margin-top-100px inner-svg-centered'></div>
					</div>
					<div className = 'pure-u-md-1-2 pure-u-sm-3-4 margin-top-40px'>
						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">One last step</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">Are you wired ?</div>
						</div>

						<div className='padding-left-30px padding-right-30px padding-bottom-30px' onKeyUp={this.testEnter}>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels='Company logo' />
								<div className='width-100pc'>
									<common.DragAndDropImage errMsg={this.state.fileErrMsg} classProp="image-picker" defaultIcon="svg-picture1" handleFiles={this.handleUpload} image={this.state.image}/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels='Social medias' />
								<div className='inline-block v-align-top'>
									<common.SocialMedias social={this.state.social} updateSocial={this.updateSocial} enterPressed={this.register}/>		
								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate="true" labels='Finish' classProp="margin-bottom-20px"/>
							</div>
						</div>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Topbar = React.createClass({
 	goToProspect(){
 		yarr(properties.routes.employer.home);
 	},
 	goToSeeker(){
 		yarr(properties.routes.core);
 	},
	render() {
		return (
			<div className = 'pure-g background-white height-60px'>
				<div id="container" className = 'tobar'>
					<div className='float-left margin-top-5px margin-right-5px margin-left-5px'>
						<common.Icon icon='svg-logo' classProp='icon-medium fill-color1'/>
					</div>
					<div className="letter-spacing-1px pointer float-left color1 padding-left-15px padding-right-15px navUnSelectable" onClick={this.goToSeeker}>Narwhal</div>
					<div className="letter-spacing-1px pointer thin-text float-left color1 padding-left-15px padding-right-15px navSelected">Register (employer)</div>
					
				</div>
			</div>
		);
	}
});

module.exports = {
	Reg1Form : Reg1Form,
	Reg2Form : Reg2Form,
	Reg3Form : Reg3Form,
	Reg4Form : Reg4Form
};