"use strict";

let skprofile = require('../profiles/seeker.js'),
	requests = require('../../services/employer-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	empCommon = require('./common.js');

let Seekprofile = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : {}
	   	};
	},
	componentDidMount(){
		requests.getSeekerProfile(this.props.seekId, seeker => {
			seeker.social.forEach(soc => {
				properties.getSocial(soc.id, bundle => {
					soc.name = bundle.name;
				});
			});
			this.setState({seeker : seeker});
		});
	},
	render() {
		return (
			<div>
				<empCommon.Topbar />
				<skprofile.Profile seeker={this.state.seeker} edit="false"/>
			</div>
		);
	}
});

module.exports = {
	Seekprofile : Seekprofile
};