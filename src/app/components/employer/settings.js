"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	employerCommon = require('./common.js'),
	requests = require('../../services/employer-requests.js'),
	JobRequests = require('../../services/jobs-requests.js'),
	properties = require('../../services/properties.js'),
	dates = require('../../services/dates.js'),
	storage = require('../../services/storage.js');

let Settings = React.createClass({
	getInitialState() {
		return {
			employer : storage.get('employer')
		};
 	},
 	componentDidMount(){
 		properties.getPlans(x => {
 			this.setState({plans : x});
 		});
 		let emp = storage.get('employer');
 		requests.login(emp.email, emp.password, x => {
 			this.setState({employer : x});
 		});
 	},
 	changePlan(plan){
 		let emp = this.state.employer;
 		emp.plan = plan.id;
 		emp.planDate = new Date();
 		requests.update(emp.email, emp, x => {
 			if(x){
				requests.login(emp.email, emp.password, x => {
					this.setState({employer : x});
				});
 			}
 		});
 	},
	render() {
		let selectedPlan = {},
			joinDate = this.state.employer && this.state.employer.planDate ? moment(this.state.employer.planDate).format('MMMM Do YYYY') : '(unknown date)';
		if(this.state.plans && this.state.employer){
			this.state.plans.forEach(x => {
				if(x.id == this.state.employer.plan){
					selectedPlan = x;
				}
			});
		}
		let plan1 = this.state.plans ? this.state.plans[0] : undefined,
			plan2 = this.state.plans ? this.state.plans[1] : undefined,
			plan3 = this.state.plans ? this.state.plans[2] : undefined,
			plan4 = this.state.plans ? this.state.plans[3] : undefined,
			resetFormatted = this.state.employer ? moment(this.state.employer.nextReset).format('MMMM Do YYYY') : '';
		return (
			<div className="main-container">
				<employerCommon.Topbar forceRoute='employer/settings'/>
				<div id="container" className='pure-g padding-top-10px'>
					<h3 className="container margin-left-10px margin-right-10px padding-10px background-color3">Settings</h3>
					<h4 className="margin-left-5px">Your current plan</h4>
					<div className="padding-10px font-13px">
						You joined the <b>{selectedPlan.name}</b> plan on <b>{joinDate}</b>. 
					</div>
					<div className="padding-10px font-13px">
						You have <b>{this.state.employer.monthlyLeft}</b> available jobs this month. The next reset will occur on {resetFormatted}
					</div>

					<h4 className="margin-left-5px">Change plan</h4>
					<div className="pure-g">
						<div className="pure-u-md-1-4">
							<Plan plan={plan1} selectedPlan={selectedPlan} changePlan={this.changePlan}/>
						</div>
						<div className="pure-u-md-1-4">
							<Plan plan={plan2} selectedPlan={selectedPlan} changePlan={this.changePlan}/>
						</div>
						<div className="pure-u-md-1-4">
							<Plan plan={plan3} selectedPlan={selectedPlan} changePlan={this.changePlan}/>
						</div>
						<div className="pure-u-md-1-4">
							<Plan plan={plan4} selectedPlan={selectedPlan} changePlan={this.changePlan}/>
						</div>
					</div>
					
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Plan = React.createClass({
	getInitialState() {
		return {};
 	},
 	selectPlan(){
 		if(this.props.changePlan && this.props.plan){
 			this.props.changePlan(this.props.plan);
 		}
 	},
	render() {
		let plan = this.props.plan ? this.props.plan : {}, 
			selected = this.props.selectedPlan ? this.props.selectedPlan : {}, 
			isSelected = this.props.plan && this.props.selectedPlan && this.props.selectedPlan.id == this.props.plan.id,
			buttonLabel = this.props.plan && this.props.selectedPlan && this.props.plan.id > this.props.selectedPlan.id ? "Upgrade" : "Downgrade",
			iconClass = this.props.plan && this.props.selectedPlan && this.props.plan.id > this.props.selectedPlan.id ? "svg-arrow-u" : "svg-arrow-d",
			buttonClass = this.props.plan && this.props.selectedPlan && this.props.plan.id == this.props.selectedPlan.id ? "hide" : "",
			selectedClass = this.props.plan && this.props.selectedPlan && this.props.plan.id == this.props.selectedPlan.id ? "" : "hide";
		return (
			<div className="width-200px padding-5px bloc-centered">
				<div className="text-centered background-color3 padding-5px">
					<div className="margin-top-2px margin-bottom-2px">{plan.name}</div>
					<div className="font-14px margin-top-2px margin-bottom-2px"><span className="opacity-70"> $</span>{plan.pricing}<span className="opacity-70"> /month</span></div>
				</div>
				<div className="margin-top-15px margin-bottom-15px">
					<div className="width-200px font-15px text-centered">{plan.monthly}</div>
					<div className="width-200px font-13px text-centered opacity-70">Job activation /month</div> 
				</div>
				<div className="margin-top-15px margin-bottom-15px">
					<div className="width-200px font-15px text-centered">Unlimited</div>
					<div className="width-200px font-13px text-centered opacity-70">Likes</div> 
				</div>
				<div className="margin-top-15px margin-bottom-15px">
					<div className="width-200px font-15px text-centered">Unlimited</div>
					<div className="width-200px font-13px text-centered opacity-70">Liked</div> 
				</div>
				<div className="margin-top-15px margin-bottom-15px">
					<div className={"width-110px bloc-centered " + buttonClass}>
						<common.MiniIconButton icon={iconClass} labels={buttonLabel} className="background-color1 inline-block" colorIcon="fill-white" onClick={this.selectPlan}/>
					</div>
					<div className={"margin-top-20px background-color1 white-color padding-top-5px padding-bottom-5px padding-left-15px padding-right-15px bloc-centered text-centered font-11px width-110px opacity-60 " + selectedClass}>
						Selected
					</div>
				</div>
			</div>
		);
	}
});

module.exports = {
	Settings : Settings
};