let jobs = require('../jobs/jobs.js'),
	common = require('../common.js'),
	storage = require('../../services/storage.js'),
	requests = require('../../services/employer-requests.js'),
	empCommon = require('./common.js');


let Jobs = React.createClass({
	getInitialState() {
	   	return {
	   		employer : {}
	   	};
	},
	componentDidMount(){
		let emp = storage.get('employer');
 		requests.login(emp.email, emp.password, x => {
 			this.setState({employer : x});
 		});
	},
	render() {
		return (
			<div className="main-container">
				<empCommon.Topbar forceRoute="job"/>
				<jobs.Jobs jobId={this.props.jobId} employer={this.state.employer} edit="true"/>
				<common.Bottombar />
			</div>
		);
	}
});

module.exports = {
	Jobs : Jobs
}