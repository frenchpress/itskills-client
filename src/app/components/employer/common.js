"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js');

let Topbar = React.createClass({
	getInitialState() {
		let emp = storage.get('employer'); 
		return {
			employer : emp ? emp : {}
		};
 	},
 	settings(){
 		yarr(properties.routes.employer.settings);
 	},
 	handshake(){
 		console.log('Handshake');
 	},
 	goToProspect(){
 		yarr(properties.routes.employer.home);
 	},
 	goToMatches(){
		yarr(properties.routes.employer.matches);
 	},
 	isWithinCurrentRoute(label){
 		if(this.props.forceRoute){
 			return this.props.forceRoute === label;
 		} else {
 			return window.location ? window.location.pathname.indexOf(label) !== -1 : false;	
 		}
 	},
 	goProfil(){
 		yarr(properties.routes.employer.profile);
 	},
	render() {
		let avatar = (function(){
			if(this.state.employer.avatar){

				let res = this.state.employer.avatar.split("/"),
					url = res[res.length - 1],
					profileSel = this.isWithinCurrentRoute('employer/profile') ? 'avatarSelected' : 'avatarUnSelected';

					return (
						<div className={"float-right padding-top-15px padding-bottom-7px " + profileSel}>
							<img onClick={this.goProfil} src={properties.baseUrl +'employers-avatars/' + url} alt="Company logo" className='width-25px height-25px radius-5px pointer'></img>
						</div>
					)
			} else {
				let profileSelIcon = this.isWithinCurrentRoute('employer/profile') ? 'iconSelected' : '';
				return (
					<div onClick={this.goProfil} className={profileSelIcon + ' pointer float-right margin-top-20px margin-right-5px padding-top-3px padding-left-5px padding-right-5px radius-5px background-greycolor hover-color-color1 transition-3'}>
						<common.Icon icon='svg-anonymous' classProp='icon-tiny fill-white'/>
					</div>
				)
			}
		}.call(this));

		let prospectSel = this.isWithinCurrentRoute('employer/home') ? 'navSelected' : 'navUnSelected',
			matchesSel = this.isWithinCurrentRoute('employer/matches') ? 'navSelected' : 'navUnSelected',
			profileSelNav = this.isWithinCurrentRoute('employer/profile') ? 'navSelected' : 'navUnSelected',
			settingsSel = this.isWithinCurrentRoute('employer/settings') ? 'iconSelected' : '';
		
		return (
			<div className = 'pure-g background-white height-60px'>
				<div id="container" className = 'tobar'>
					<div className='pointer float-left margin-top-5px margin-right-5px margin-left-5px'>
						<common.Icon icon='svg-logo' classProp='icon-medium fill-color1' click={this.goToProspect} />
					</div>
					
					<div className={"letter-spacing-1px pointer thin-text float-left color1 padding-left-10px padding-right-10px " + prospectSel} onClick={this.goToProspect}>Discovery</div>

					<div className={"letter-spacing-1px pointer thin-text float-left color1 padding-left-10px padding-right-10px " + matchesSel} onClick={this.goToMatches}>Matching</div>
					
					<div className={"letter-spacing-1px pointer float-right color1 padding-left-10px padding-right-10px " + profileSelNav} onClick={this.goProfil}>{this.state.employer.companyName}</div>
					
					{avatar}
					
					<div className={'pointer float-right margin-top-20px margin-right-5px padding-top-3px padding-left-5px padding-right-5px radius-5px background-greycolor hover-color-color1 transition-3 ' + settingsSel} onClick={this.settings}>
						<common.Icon icon='svg-settings1' classProp='icon-tiny fill-white'/>
					</div>

					<div className='pointer float-right margin-top-20px margin-right-5px padding-top-3px padding-left-5px padding-right-5px radius-5px background-greycolor hover-color-color1 transition-3' onClick={this.handshake}>
						<common.Icon icon='svg-handshake' classProp='icon-tiny fill-white' />
					</div>
					
				</div>
			</div>
		);
	}
});

module.exports = {
	Topbar : Topbar
};