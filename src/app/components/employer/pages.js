"use strict";

let register = require('./register.js'),
	home = require('./home.js'),
	matches = require('./matches.js'),
	profile = require('./profile.js'),
	seekprofile = require('./seekprofile.js'),
	jobs = require('./jobs.js'),
	settings = require('./settings.js');

let Register1 = React.createClass({
	render() {
		return (
			<register.Reg1Form />
		);
	}
});

let Register2 = React.createClass({
	render() {
		return (
			<register.Reg2Form />
		);
	}
});

let Register3 = React.createClass({
	render() {
		return (
			<register.Reg3Form />
		);
	}
});

let Register4 = React.createClass({
	render() {
		return (
			<register.Reg4Form />
		);
	}
});

let Home = React.createClass({
	render() {
		return (
			<home.HomePage />
		);
	}
});

let Matches = React.createClass({
	render() {
		return (
			<matches.MatchesPage />
		);
	}
});


let Profile = React.createClass({
	render() {
		return (
			<profile.Empprofile />
		);
	}
});

let Seekprofile = React.createClass({
	render() {
		return (
			<seekprofile.Seekprofile seekId={this.props.seekId}/>
		);
	}
});

let Jobs = React.createClass({
	render() {
		return (
			<jobs.Jobs jobId={this.props.jobId}/>
		);
	}
});

let Settings = React.createClass({
	render() {
		return (
			<settings.Settings/>
		);
	}
});


module.exports = {
	Register1 : Register1,
	Register2 : Register2,
	Register3 : Register3,
	Register4 : Register4,
	Home : Home,
	Matches : Matches,
	Profile : Profile,
	Seekprofile : Seekprofile,
	Jobs : Jobs,
	Settings : Settings
};