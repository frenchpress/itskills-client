"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	employerCommon = require('./common.js'),
	requests = require('../../services/employer-requests.js'),
	JobRequests = require('../../services/jobs-requests.js'),
	properties = require('../../services/properties.js'),
	dates = require('../../services/dates.js'),
	storage = require('../../services/storage.js'),
	Rating = require('../../../../node_modules/Rating/js/rating.js');

let MatchesPage = React.createClass({
	getInitialState() {
		return {
			employer : storage.get('employer'),
		};
 	},
 	componentDidMount(){
 		requests.getAllMatches(this.state.employer.email, (success, matches) => {
 			if(success && matches){
 				if(matches.seekers){
 					matches.seekers.forEach(x => {
 						x.show = false;
 					});
 				}
 				var i = 1,
 					arr = _.times(matches.jobs.length, () => {
 						return i++;
 					});

 				this.setState({
 					jobs : matches.jobs,
 					seekers : matches.seekers,
 					jobsFilters : arr
 				}, this.impactFilters);
 			}
 		});
 	},
 	impactFilters(){
 		let jobs = this.state.jobs,
 			seekers = this.state.seekers;
 		if(jobs && this.state.matchesFilters && this.state.jobsFilters){
 			seekers.forEach(x => {
 				x.show = false;
 				x.likedBy = [];
 				x.likes = [];
 				x.matches = [];
 			});
 			this.state.jobsFilters.forEach(jobId => {
 				jobs.forEach(job => {
 					if(job.id === jobId){
 						seekers.forEach(seeker => {
 							//If we show matches
 							if(this.state.matchesFilters.indexOf(1) !== -1){
 								if(seeker.favored.indexOf(job._id) !== -1 && job.favored.indexOf(seeker._id) !== -1){
				 					seeker.show = true;
				 					seeker.matches.push({_id : job.id, title : job.title});
				 				}
 							}
 							//If we show interested seekers
 							if(this.state.matchesFilters.indexOf(2) !== -1){
 								if(seeker.favored.indexOf(job._id) !== -1 && job.discarded.indexOf(seeker._id) === -1){
				 					seeker.show = true;
				 					seeker.likes.push({_id : job.id, title : job.title});
				 				}	
 							}
 							//If we show interessting seekers
 							if(this.state.matchesFilters.indexOf(3) !== -1){
 								if(job.favored.indexOf(seeker._id) !== -1){
				 					seeker.show = true;
				 					seeker.likedBy.push({_id : job.id, title : job.title});
				 				}	
 							}
			 			});
 					}
 				});
 			});
 			this.setState({
				jobs : jobs,
				seekers : seekers
			});
 		}
 	},
 	matchesSort(matches){
 		this.setState({matchesFilters : matches}, this.impactFilters);
 	},
 	jobsSort(jobs){
 		this.setState({jobsFilters : jobs}, this.impactFilters);
 	},
 	favorSeeker(seeker){
 		let jobs = this.state.jobs;
 		seeker.likes.forEach(innJob => {
 			jobs.forEach(job => {
 				if(job.id === innJob._id){
 					requests.favor(job._id, seeker._id, x => {
			 			job.favored.push(seeker._id);
			 			this.setState({jobs : jobs}, this.impactFilters);
			 		});
 				}
 			});
 		});
 	},
 	unfavorSeeker(seeker){
 		let jobs = this.state.jobs;
 		seeker.likedBy.forEach(innJob => {
 			jobs.forEach(job => {
 				if(job.id === innJob._id){
 					requests.unfavor(job._id, seeker._id, x => {
			 			let index = job.favored.indexOf(seeker._id);
			 			if(index !== -1){
			 				job.favored.splice(index, 1);
			 				this.setState({jobs : jobs}, this.impactFilters);
			 			}
			 		});
 				}
 			});
 		});
 	},
 	discardSeeker(seeker){
 		this.unfavorSeeker(seeker);
 		let jobs = this.state.jobs;
 		seeker.likes.forEach(innJob => {
 			jobs.forEach((job) => {
				if(job.id === innJob._id){
					requests.discard(job._id, seeker._id, x => {
			 			let seekers = this.state.seekers;
			 			seekers.forEach((seek, index) => {
			 				if(seek._id === seeker._id){
			 					seekers.splice(index, 1);
			 					this.setState({seekers : seekers}, this.impactFilters);
			 				}
			 			});
			 		});			
 				}
 			});
 		});
 		
 	},
	render(){
		return (
			<div className="main-container">
				<employerCommon.Topbar forceRoute="employer/matches"/>
				<div id="container" className='pure-g padding-top-10px'>
					<div className="pure-u-md-1-3">
						<Filters jobs={this.state.jobs} matchesSort={this.matchesSort} jobsSort={this.jobsSort}/>
					</div>
					<div className="pure-u-md-2-3">
						<Seekers seekers={this.state.seekers} favorSeeker={this.favorSeeker} discardSeeker={this.discardSeeker} unfavorSeeker={this.unfavorSeeker}/>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Seekers = React.createClass({
	getInitialState() {
	   	return {
	   		seekers : this.props.seekers ? this.props.seekers : [],
	   		page  : 1,
	   		pages : this.props.seekers ? Math.ceil(this.props.seekers.length/5) : 1,
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seekers){
			let count = 0;
			nextProp.seekers.forEach(x => {
				if(x.show){
					count++;
				}
			});
			this.setState({seekers : nextProp.seekers, pages : Math.ceil(count/5)});
		}
	},
	favor(seeker){
 		if(this.props.favorSeeker){
 			this.props.favorSeeker(seeker);
 		}
 	},
 	discard(seeker){
 		if(this.props.discardSeeker){
 			this.props.discardSeeker(seeker);
 		}
 	},
 	unfavor(seeker){
 		if(this.props.unfavorSeeker){
 			this.props.unfavorSeeker(seeker);
 		}
 	},
 	pageBefore(){
 		let page = this.state.page;
 		this.setState({page : page - 1 > 0 ? page - 1 : 1});
 	},
 	pageAfter(){
 		let page = this.state.page;
 		this.setState({page : page + 1 <= this.state.pages ? page + 1 : this.state.pages});
 	},
 	pageGoTo(page){
 		this.setState({page : page});
 	},
	render() {
		let count = 0;
		let seekers = this.state.seekers.map((seeker, index) => {
			let readableXp = '', readableDegree = '', 
				show = seeker.show ? '' : 'hide',
				favorStyle = {display : seeker.likedBy && seeker.likedBy.length ? 'none' : 'inline-block'},
				unFavorStyle = {display : seeker.likedBy && seeker.likedBy.length ? 'inline-block' : 'none'},
				innFavor = this.favor.bind(this, seeker),
				innDiscard = this.discard.bind(this, seeker),
				innUnfavor = this.unfavor.bind(this, seeker),
				showPrivateDate = seeker.matches && seeker.matches.length ? '' : 'hide',
				onPage = count < 5*this.state.page && count >= (this.state.page-1)*5 ? '' : 'hide';
			properties.getXp(seeker.xp, x => {
				readableXp = x.name;
			});
			properties.getDegree(seeker.education, x => {
				readableDegree = x.name;
			});
			if(seeker.show){
				count++;
			}
			return (
				<div key={index} className={"pure-g padding-10px thin-border-bottom thin-border-left " + onPage + " " + show}>
					<div>
						<div className="pure-u-md-2-3">
							<h4 className="no-margin no-padding-top padding-bottom-10px">{seeker.name}</h4>
							<common.TagsBoxShow tags={seeker.skills} />

							<div className={"margin-top-10px " + showPrivateDate}>
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-mail" />
								<span className="font-12px line-height-20px v-align-bottom">{seeker.email}</span>
							</div>
							<div className={"margin-top-10px " + showPrivateDate}>
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-number" />
								<span className="font-12px line-height-20px v-align-bottom">{seeker.phone}</span>
							</div>
							
							<div className="thin-text margin-top-5px">
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-degree" />
								<span className="font-12px line-height-20px v-align-bottom">{readableDegree}</span>
							</div>
							<div className="thin-text margin-top-10px">
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-experience" />
								<span className="font-12px line-height-20px v-align-bottom">{readableXp}</span>
							</div>
						</div>
						<div className="pure-u-md-1-3">
							<MatchBlock matches={seeker.matches} likes={seeker.likes} likedBy={seeker.likedBy} className='float-right margin-right-10px'/>
						</div>
					</div>
					<div>
						<div className="height-35px">
							<common.MiniIconButton icon="svg-details" labels="Details" className="background-color1 inline-block" colorIcon="fill-white" />
							<common.MiniIconButton icon="svg-close" labels="Discard" className="background-color1 inline-block float-right" colorIcon="fill-white" onClick={innDiscard}/>
							<div style={favorStyle} className="float-right">
								<common.MiniIconButton icon="svg-check" labels="Interested" className="background-color1 inline-block margin-right-5px" colorIcon="fill-white" onClick={innFavor}/>
							</div>
							<div style={unFavorStyle} className="float-right">
								<common.MiniIconButton icon="svg-close" labels="Uninterested" className="background-color5 inline-block margin-right-5px" colorIcon="fill-white" onClick={innUnfavor}/>
							</div>
						</div>
					</div>
				</div>
			)
		});
		let emptyFallback = {
			display : this.state.seekers.length ? 'none' : 'block'
		}, seekersBlock = {
			display : emptyFallback.display === 'none' ? 'block' : 'none'
		};
		return (
			<div>
				<h3 className="no-margin-bottom no-margin-left no-margin-right padding-10px thin-border-bottom">Seekers</h3>
				<div style={seekersBlock}>
					{seekers}
					<div className="margin-top-10px">
						<common.Paginator page={this.state.page} pages={this.state.pages} before={this.pageBefore} after={this.pageAfter} goTo={this.pageGoTo}/>
					</div>
				</div>
				<div className="text-centered margin-top-10px font-12px" style={emptyFallback}>Sorry, we didn't find any job seeker fitting your criteria</div>
			</div>
		);
	}
});

let MatchBlock = React.createClass({
	render() {
		let matches = this.props.matches ? _.pluck(this.props.matches, 'title').join(', ') : '',
			likes = this.props.likes ? _.pluck(this.props.likes, 'title').join(', ') : '',
			likedBy = this.props.likedBy ? _.pluck(this.props.likedBy, 'title').join(', ') : '';
		let showMatches = {display : this.props.matches && this.props.matches.length ? 'block' : 'none'},
			showLikes = {display : this.props.likes && this.props.likes.length && !(this.props.matches && this.props.matches.length) ? 'block' : 'none'},
			showLikedBy = {display : this.props.likedBy && this.props.likedBy.length && !(this.props.matches && this.props.matches.length) ? 'block' : 'none'};
		return (
			<div className={this.props.className + ' thin-text'}>
				<div style={showMatches}>
					<common.Icon icon='svg-handshake' classProp="icon-mini fill-color1 margin-right-5px"/>
					<span className="v-align-top">Matches : {matches}</span>
				</div>
				<div style={showLikes}>
					<common.Icon icon='svg-arrow-r-3' classProp="icon-mini fill-color1 margin-right-5px padding-top-1px rotate-180"/>
					<span className="v-align-top">Liked : {likes}</span>
				</div>
				<div style={showLikedBy}>
					<common.Icon icon='svg-arrow-r-3' classProp="icon-mini fill-color1 margin-right-5px padding-top-1px"/>
					<span className="v-align-top">Liked for : {likedBy}</span>
				</div>
			</div>
		);
	}
});

let Filters = React.createClass({
	getInitialState() {
		let jobs = this.craftJobs(this.props.jobs);
	   	return {
	   		selection : [
	   			{id: 1, name: 'Matches'},
	   			{id: 2, name: 'Seekers interested'},
	   			{id: 3, name: 'Seekers of interest'}
	   		],
	   		selectionDefaults : [1, 2, 3],
	   		jobs : jobs,
	   		set : false,
	   		defaultIds : _.pluck(jobs, 'id')
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.jobs){
			let jobs = this.craftJobs(nextProp.jobs);
			this.setState({
				jobs : jobs,
				defaultIds : this.state.set ? this.state.defaultIds : _.pluck(jobs, 'id'),
				set : true
			});
		}
	},
	craftJobs(jobs){
		if(jobs){
			jobs.forEach((job, index) => {
				job.id = index + 1;
				job.name = job.title;
			});
			return jobs;
		} else {
			return [];
		}
	},
	setMatches(matches){
		if(this.props.matchesSort && matches){
			let array = _.pluck(matches, 'id');
			this.setState({selectionDefaults : array});
			this.props.matchesSort(array);
		}
	},
	setJobs(jobs){
		if(this.props.jobsSort && jobs){
			let array = _.pluck(jobs, 'id');
			this.setState({defaultIds : array}, () => {
				this.props.jobsSort(array);	
			});
		}
	},
	selectAllJobs(){
		this.setJobs(this.state.jobs);
	},
	deSelectAllJobs(){
		this.setJobs([]);
	},
	render() {
		return (
			<div>
				<h3 className="no-margin-bottom no-margin-left no-margin-right padding-10px thin-border-bottom">Filters</h3>
				<h4 className="thin-text font-16px padding-left-10px">My matches</h4>
				<p className="font-11px opacity-80 padding-left-10px">Remember that you can contact and be contacted by every one of your matches.</p>
				<div className='padding-left-10px'>
					<common.MultiChoiceList data={this.state.selection} defaultIds={this.state.selectionDefaults} setValue={this.setMatches} icon="svg-handshake" iconClassProp='icon-mini fill-color1 opacity-80 margin-left-5px'/>
				</div>
				<h4 className="thin-text font-16px padding-left-10px">My jobs</h4>
				<p className="font-11px opacity-80 padding-left-10px">Manually select every job you are currently focusing on. Alternatively <span className="color2 pointer" onClick={this.selectAllJobs}>select</span> or <span className="color2 pointer" onClick={this.deSelectAllJobs}>deselect</span> them all.</p>
				<div className='padding-left-10px'>
					<common.MultiChoiceList data={this.state.jobs} defaultIds={this.state.defaultIds} setValue={this.setJobs} icon="svg-job" iconClassProp='icon-mini fill-color1 opacity-80 margin-left-5px'/>
				</div>
			</div>
		);
	}
});

module.exports = {
	MatchesPage : MatchesPage
};