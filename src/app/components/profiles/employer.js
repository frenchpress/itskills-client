"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	requests = require('../../services/employer-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	auth = require('../../services/auth.js'),
	FileDragAndDrop = require('../../../../node_modules/react-file-drag-and-drop/build/FileDragAndDrop.js');

let Profile = React.createClass({
	getInitialState() {
	   	return {
	   		employer : this.props.employer ? this.props.employer : {},
	   		basicEdit : false,
	   		canModify : this.props.edit === "true"
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.employer){
			this.setState({
				employer : nextProp.employer
			});
		}
	},
	update(employer){
		requests.update(employer.email, employer, success => {
			if(success){
				let emp = storage.get('employer'),
					innMail = employer.newEmail ? employer.newEmail : employer.email;
				auth.employer.login(innMail, emp.password, () => {
					employer.email = innMail;
					this.setState({employer : employer});	
				});
			}
		})
	},
	editBasicBlock(){
		if(this.state.canModify){
			this.setState({basicEdit : true});
		}
	},
	cancelEditBasicBlock(){
		this.setState({basicEdit : false});
	},
	updateAvatar(file){
		requests.addPicture(this.state.employer.email, file, (x, y)=> {
			if(x){
				let tmpEmp = storage.get('employer');
				auth.employer.login(this.state.employer.email, tmpEmp.password, () => {
					this.setState({employer : storage.get('employer')});
				});
			}
		});
	},
	render() {
		return (
			<div className="main-container">
				<h3 className="container margin-left-10px margin-right-10px padding-10px thin-border-bottom background-color3">Employer profile</h3>
				<div className="container padding-top-10px padding-left-30px padding-right-30px">
					<div className='pure-g'>
						<div className="pure-u-md-1-4">
							<Avatar avatar={this.state.employer.avatar ? this.state.employer.avatar : undefined} updateAvatar={this.updateAvatar} canModify={this.state.canModify}/>
						</div>
						<div className="pure-u-md-3-4">
							<div className="padding-right-10px">
								<BasicBlock employer={this.state.employer} canModify={this.state.canModify} update={this.update} show={!this.state.basicEdit} edit={this.editBasicBlock}/>
								<BasicBlockEdit employer={this.state.employer} update={this.update} show={this.state.basicEdit} cancelEdit={this.cancelEditBasicBlock}/>
							</div>
						</div>
					</div>
					<hr className="margin-left-10px margin-right-10px opacity-30"/>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<Industry employer={this.state.employer} canModify={this.state.canModify} update={this.update}/>
						</div>
						<div className="pure-u-md-1-2">
							<Size employer={this.state.employer} canModify={this.state.canModify} update={this.update}/>
						</div>
					</div>
					<hr className="margin-left-10px margin-right-10px opacity-30"/>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<Social employer={this.state.employer} canModify={this.state.canModify} update={this.update}/>
						</div>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Social = React.createClass({
	getInitialState() {
	   	return {
	   		employer : this.props.employer ? this.props.employer : {},
	   		edit : false
	   	};
	},
	componentWillMount(){
 		properties.getSocials(x => {
 			if(this.state.employer && this.state.employer.social){
 				this.state.employer.social.forEach(soc => {
 					if(x.id === soc.id){
 						x.url = soc.url;
 						x.selected = soc.selected;
 					}
 				});
 			}
 			this.setState({socials : x});
 		});
 	},
	componentWillReceiveProps(nextProp){
		let socArr = [];
		if(nextProp.employer){
			if(this.state.socials){
				this.state.socials.forEach(x => {
					let found = false;
					nextProp.employer.social.forEach(soc => {
						if(x.id === soc.id){
	 						x.url = soc.url;
	 						x.selected = soc.selected;
	 						soc.name = x.name;
							socArr.push(x);
							found = true;
	 					}
					});
					if(!found){
						socArr.push(x);
					}
				});
			}
			this.setState({employer : nextProp.employer, socials : socArr});	
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	updateSocial(social){
		this.setState({socials : social});
	},
	done(){
		let emp = this.state.employer;
		emp.social = this.state.socials;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-top-5px margin-left-10px">Social</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.SocialMediasDisplay social={this.state.employer.social}/>
					</div>
				</div>
				<div className={'padding-left-10px ' + modif}>
					<h3>Social</h3>
					<common.SocialMedias social={this.state.socials} updateSocial={this.updateSocial} enterPressed={this.done}/>
					<common.MiniIconButton onClick={this.done} icon="svg-check" labels="Done" className="background-color1 inline-block" colorIcon="fill-white" />
				</div>
			</div>
		);
	}
});

let BasicBlock = React.createClass({
	getInitialState() {
	   	return {
	   		employer : this.props.employer ? this.props.employer : {}
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.employer){
			this.setState({
				employer : nextProp.employer, 
				activityChoices: [{
					icon : 'svg-check',
					label : 'Active',
					selected : nextProp.employer.active
				}, {
					icon : 'svg-close',
					label : 'Inactive',
					selected : !nextProp.employer.active
				}]
			});
		}
	},
	toggleActivity(){
		if(this.props.update){
			let emp = this.state.employer;
			emp.active = !emp.active;
			this.props.update(emp);
		}
	},
	render() {
		let iconColor = this.state.employer.active ? "fill-color1" : "fill-color5",
			overIconColor = this.state.employer.active ? "fill-color5" : "fill-color1",
			labelColor = this.state.employer.active ? "color1" : "color5",
			overLabelColor = this.state.employer.active ? "color5" : "color1",
			modifyShow = {display : this.props.canModify ? "block" : "none"},
			transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			globalShow = {display : this.props.show ? "block" : "none"},
			editButton = this.props.canModify ? '' : "hide",
			privateData = this.props.canModify ? '' : "hide";
		return (
			<div style={globalShow} className={"center-hover-show padding-5px radius-5px transition-all-ease-3 " + transitionClasses}>
				<div className='pure-g'>
					<div className="pure-u-md-1-2">
						<h4 className="no-margin-top margin-bottom-10px inline-block">{this.state.employer.companyName}</h4>
						<div className={"width-80px " + editButton}>
							<common.Toggle choices={this.state.activityChoices} toggle={this.toggleActivity} iconColor={iconColor} overIconColor={overIconColor} iconClasses='icon-mini ' labelColor={labelColor} overLabelColor={overLabelColor} labelClasses='thin-text font-12px margin-top-1px margin-right-5px'/>
						</div>
					</div>
					<div className="pure-u-md-1-2">
						<div className="float-right margin-right-5px transition-all-ease-3 opacity-0 hover-opacity-100">
							<common.MiniIconButton onClick={this.props.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
						</div>
					</div>
				</div>
				<p className="thin-text font-14px">{this.state.employer.description}</p>
				<div className='pure-g'>
					<div className="pure-u-md-1-2">
						<div className="thin-text margin-top-15px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-10px" icon="svg-address" />
							<span className="font-12px line-height-20px v-align-bottom">{this.state.employer.address}</span>
						</div>
						<div className="thin-text margin-top-15px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-10px" icon="svg-web" />
							<a className="font-12px line-height-20px v-align-bottom" href="{this.state.employer.website}" target="_blank">{this.state.employer.website}</a>
						</div>
					</div>
					<div className="pure-u-md-1-2">
						<div className={"thin-text margin-top-15px " + privateData}>
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-10px" icon="svg-mail" />
							<span className="font-12px line-height-20px v-align-bottom">{this.state.employer.email}</span>
						</div>
						<div className={"thin-text margin-top-15px " + privateData}>
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-10px" icon="svg-number" />
							<span className="font-12px line-height-20px v-align-bottom">{this.state.employer.phone}</span>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

let BasicBlockEdit = React.createClass({
	getInitialState() {
	   	return {
	   		employer : this.props.employer ? this.props.employer : {},
	   		refEmployer : this.props.employer ? this.props.employer : {},
	   		validMail : true
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.employer){
			this.setState({
				employer : nextProp.employer,
				refEmployer : nextProp.employer
			});
		}
	},
	update(){
		if(this.props.update){
			this.props.update(this.state.employer);
		}
		if(this.props.cancelEdit){
			this.props.cancelEdit();
		}
	},
	cancel(){
		this.setState({employer : this.state.refEmployer});
		if(this.props.cancelEdit){
			this.props.cancelEdit();
		}
	},
	changeCompanyName(value){
		let emp = this.state.employer;
		emp.companyName = value;
		this.setState({employer : emp});
	},
	changeDescription(value){
		let emp = this.state.employer;
		emp.description = value;
		this.setState({employer : emp});
	},
	changeEmail(valid, value){
		if(valid){
			let emp = this.state.employer;
			emp.email = value;
			this.setState({employer : emp, validMail : true});
		} else {
			this.setState({validMail : false});
		}
	},
	changePhone(value){
		let emp = this.state.employer;
		emp.phone = value;
		this.setState({employer : emp});
	},
	changeWebsite(value){
		let emp = this.state.employer;
		emp.website = value;
		this.setState({employer : emp});
	},
	changeAddress(value){
		let emp = this.state.employer;
		emp.address = value;
		this.setState({employer : emp});
	},
	render() {
		let globalShow = {display : this.props.show ? "block" : "none"},
			mailProp = this.state.validMail ? '' : 'red-border-bottom';
		return (
			<div style={globalShow} className="padding-5px radius-5px background-white">
				<div className='pure-g'>
					<common.GenericInput keyUp={this.changeCompanyName} placeholder='Company Name' defaultValue={this.state.employer.companyName} classProp="block full-width font-18px bold"/>
					<common.Textarea setValue={this.changeDescription} placeholder='Company description' defaultValue={this.state.employer.description} classProp="block full-width margin-top-15px thin-text font-14px"/>
				</div>
				
				<div className='pure-g'>
					<div className="pure-u-md-1-2">
						<div className="thin-text margin-top-15px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-mail" />
							<common.MailInput mailUpdate={this.changeEmail} placeholder='Email' defaultValue={this.state.employer.email} classProp={"font-12px v-align-top " + mailProp}/>
						</div>
						<div className="thin-text margin-top-15px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-number" />
							<common.GenericInput keyUp={this.changePhone} placeholder='Phone number' defaultValue={this.state.employer.phone} classProp="font-12px v-align-top"/>
						</div>
					</div>
					<div className="pure-u-md-1-2">
						<div className="thin-text margin-top-15px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-address" />
							<common.GenericInput keyUp={this.changeAddress} placeholder='Address' defaultValue={this.state.employer.address} classProp="font-12px v-align-top"/>
						</div>
						<div className="thin-text margin-top-15px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-web" />
							<common.GenericInput keyUp={this.changeWebsite} placeholder='website' defaultValue={this.state.employer.website} classProp="font-12px v-align-top"/>
						</div>
					</div>
				</div>

				<div className='pure-g'>
					<common.MiniIconButton onClick={this.update} icon="svg-check" labels="Save" className="margin-top-20px background-color1 inline-block margin-right-10px" colorIcon="fill-white" />
					<common.MiniIconButton onClick={this.cancel} icon="svg-close" labels="Cancel" className="margin-top-20px background-color5 inline-block" colorIcon="fill-white" />
				</div>

			</div>
		);
	}
});

let Industry = React.createClass({
	getInitialState() {
	   	return {
	   		employer : this.props.employer ? this.props.employer : {},
	   		industry : {},
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getIndustries(x => {
 			this.setState({industries : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.employer){
			properties.getIndusty(nextProp.employer.industry, ind => {
				this.setState({
					employer : nextProp.employer,
					industry : ind
				});	
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setIndustry(industry){
		let emp = this.state.employer;
		emp.industry = industry;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-top-5px margin-left-10px">Industry</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-10px" icon="svg-industry" />
						<span className="font-12px line-height-20px v-align-bottom">{this.state.industry.name}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Industry</h3>
					<common.CustomSelect setValue={this.setIndustry} data={this.state.industries} icon='svg-industry' iconClassProp="icon-avg fill-color1" classProp='margin-bottom-10px v-align-top' isFocus="focus"/>
				</div>
			</div>
		);
	}
});

let Size = React.createClass({
	getInitialState() {
	   	return {
	   		employer : this.props.employer ? this.props.employer : {},
	   		size : {},
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getCompanySizes(x => {
 			this.setState({companySizes : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.employer){
			properties.getCompanySize(nextProp.employer.size, size => {
				this.setState({employer : nextProp.employer, size : size});	
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setSize(size){
		let emp = this.state.employer;
		emp.size = size.id;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-top-5px margin-left-10px">Company size</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-10px" icon="svg-size1" />
						<span className="font-12px line-height-20px v-align-bottom">{this.state.size.name}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Company size</h3>
					<common.TinyList defaultId={this.state.employer.size} setValue={this.setSize} data={this.state.companySizes} icon='svg-size1' iconClassProp="icon-avg fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' isFocus="focus"/>
				</div>
			</div>
		);
	}
});

let Avatar = React.createClass({
	componentWillReceiveProps(nextProp){
		if(nextProp.avatar){
			this.setState({avatar : nextProp.avatar});
		}
	},
	componentDidMount(){
		React.findDOMNode(this.refs.fileInput).addEventListener('change', evt => {
			this.handleDrop(evt.target);
		}, false);
	},
	handleDrop(dataTransfer){
		if(this.props.canModify){
			let file = dataTransfer.files[0];
			if(this.props.updateAvatar){
				this.props.updateAvatar(file);
			}	
		}
	},
	browse(){
		if(this.props.canModify){
			React.findDOMNode(this.refs.fileInput).click();
		}
	},
	render() {
		let showAvatar = (() => {
			let res = this.state && this.state.avatar ? this.state.avatar.split("/") : undefined,
			url = res ? res[res.length - 1] : undefined;
			if(url){
				return (
					<img src={properties.baseUrl +'employers-avatars/' + url} alt="Company logo" className='margin-left-10px width-150px height-150px radius-5px'></img>
				);
			} else {
				return <div className="margin-left-10px width-150px height-150px radius-5px background-white"></div>
			}
		})();
		let showModifs = this.props.canModify ? "" : "hide";
		return (
			<div className="relative">
				{showAvatar}
				<div className={showModifs}>
					<div onClick={this.browse} className="update-avatar-overlay width-100px height-100px margin-left-10px transition-all-ease-3 padding-left-50px padding-top-50px">
						<FileDragAndDrop onDrop={this.handleDrop}>
							<div className="background-color1 radius-5px padding-10px width-30px height-30px">
								<common.Icon classProp="icon-normal v-align-top fill-white" icon="svg-upload" />
							</div>
						</FileDragAndDrop>
					</div>
				</div>
				<input type="file" ref="fileInput" name="files[]" className="hide"/>
			</div>
		);
	}
});

module.exports = {
	Profile : Profile
};