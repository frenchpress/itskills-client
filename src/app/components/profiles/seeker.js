"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	requests = require('../../services/seeker-requests.js'),
	tagsRequests = require('../../services/tag-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	auth = require('../../services/auth.js'),
	FileDragAndDrop = require('../../../../node_modules/react-file-drag-and-drop/build/FileDragAndDrop.js');

let Profile = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		basicEdit : false,
	   		canModify : this.props.edit === "true"
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			this.setState({
				seeker : nextProp.seeker
			});
		}
	},
	update(seeker){
		requests.update(seeker.email, seeker, success => {
			if(success){
				let seek = storage.get('seeker'),
					innMail = seeker.newEmail ? seeker.newEmail : seeker.email;
				auth.seeker.login(innMail, seek.password, () => {
					seeker.email = innMail;
					this.setState({seeker : seeker});	
				});
			}
		})
	},
	editBasicBlock(){
		this.setState({basicEdit : true});
	},
	cancelEditBasicBlock(){
		this.setState({basicEdit : false});
	},
	render() {
		return (
			<div className="main-container">
				<div id="container">
					<div className='pure-g padding-top-10px'>
						<h3 className="padding-10px thin-border-bottom background-color3">Seeker's Profile</h3>
						<div className="padding-left-40px padding-right-40px">
							<BasicBlock seeker={this.state.seeker} canModify={this.state.canModify} update={this.update} show={!this.state.basicEdit} edit={this.editBasicBlock}/>
							<BasicBlockEdit seeker={this.state.seeker} update={this.update} show={this.state.basicEdit} cancelEdit={this.cancelEditBasicBlock}/>
						</div>
					</div>
					<div className="padding-left-30px padding-right-30px">
						<hr className="margin-left-10px margin-right-10px opacity-07"/>
						<div className='pure-g'>
							<div className="pure-u-md-1-2">
								<Education seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
							<div className="pure-u-md-1-2">
								<Location seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
						</div>
						<hr className="margin-left-10px margin-right-10px opacity-07"/>
						<div className='pure-g'>
							<div className="pure-u-md-1-2">
								<Industries seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
							<div className="pure-u-md-1-2">
								<JobTypes seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
						</div>
						<hr className="margin-left-10px margin-right-10px opacity-07"/>
						<div className='pure-g'>
							<div className="pure-u-md-1-2">
								<Social seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
							<div className="pure-u-md-1-2">
								<Salary seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
						</div>
						<hr className="margin-left-10px margin-right-10px opacity-07"/>
						<div className='pure-g'>
							<Tags seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
						</div>
						<hr className="margin-left-10px margin-right-10px opacity-07"/>
						<div className='pure-g'>
							<div className="pure-u-md-1-2">
								<Xp seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
							<div className="pure-u-md-1-2">
								<Status seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
						</div>
						<hr className="margin-left-10px margin-right-10px opacity-07"/>
						<div className='pure-g'>
							<div className="pure-u-md-1-2">
								<CompanySize seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
							<div className="pure-u-md-1-2">
								<Cv seeker={this.state.seeker} canModify={this.state.canModify} update={this.update}/>
							</div>
						</div>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Cv = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		edit : false,
	   		fileErrorMsg : ''
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			this.setState({
				seeker : nextProp.seeker
			});
		}
	},
	handleFiles(files){
 		if(files[0] && this.props.canModify){
 			requests.addCv(this.state.seeker.email, files[0], (x, y) => {
 				if(!x){
 					this.setState({fileErrorMsg : y});
 				} else {
 					if(this.props.update){
 						let sk = this.state.seeker;
 						sk.cv = y.cv;
 						this.props.update(sk);
 					}
 					this.setState({fileErrorMsg : '', edit : false});
 				}
 			});
 		}
 	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide",
			isCv = this.state.seeker.cv ? "" : "hide",
			isNoCv = this.state.seeker.cv ? "hide" : "",
			url = this.state.seeker.cv ? properties.baseUrl + this.state.seeker.cv.replace('uploads/', '') : '';
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Cv</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className={"thin-text padding-10px " + isCv}>
						<a href={url}>
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-download" />
							<span className="font-12px line-height-20px v-align-bottom">Download</span>
						</a>
					</div>
					<div className={"thin-text padding-10px " + isNoCv}>
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-none" />
						<span className="font-12px line-height-20px v-align-bottom">No CV uploaded yet !</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Cv</h3>
					<common.DragAndDrop errorMsg='' classProp="file-picker" defaultIcon="svg-degree" handleFiles={this.handleFiles} image={this.state.image}/>
				</div>
			</div>
		);
	}
});

let Status = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		status : {},
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getStatuses(x => {
 			this.setState({statuses : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			properties.getStatus(nextProp.seeker.status, ind => {
				this.setState({
					seeker : nextProp.seeker,
					status : ind
				});	
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setStatus(status){
		let emp = this.state.seeker;
		emp.status = status;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Status</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-status" />
						<span className="font-12px line-height-20px v-align-bottom">{this.state.status.name}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Status</h3>
					<common.CustomSelect setValue={this.setStatus} data={this.state.statuses} icon='svg-status' iconClassProp="icon-avg fill-color1" classProp='margin-bottom-10px v-align-top' isFocus="focus"/>
				</div>
			</div>
		);
	}
});

let Xp = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		xp : {},
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getXps(x => {
 			this.setState({xps : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			properties.getXp(nextProp.seeker.xp, ind => {
				this.setState({
					seeker : nextProp.seeker,
					xp : ind
				});	
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setXp(xp){
		let emp = this.state.seeker;
		emp.xp = xp;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Experience</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-degree" />
						<span className="font-12px line-height-20px v-align-bottom">{this.state.xp.name}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Experience</h3>
					<common.CustomSelect setValue={this.setXp} data={this.state.xps} icon='svg-degree' iconClassProp="icon-avg fill-color1" classProp='margin-bottom-10px v-align-top' isFocus="focus"/>
				</div>
			</div>
		);
	}
});

let Salary = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		salary : {},
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getSalaries(x => {
 			this.setState({salaries : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			properties.getSalary(nextProp.seeker.salary, ind => {
				this.setState({
					seeker : nextProp.seeker,
					salary : ind
				});	
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setSalary(salary){
		let emp = this.state.seeker;
		emp.salary = salary;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Salary</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-dollar" />
						<span className="font-12px line-height-20px v-align-bottom">{this.state.salary.name}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Salary</h3>
					<common.CustomSelect setValue={this.setSalary} data={this.state.salaries} icon='svg-dollar' iconClassProp="icon-avg fill-color1" classProp='margin-bottom-10px v-align-top' isFocus="focus"/>
				</div>
			</div>
		);
	}
});

let Tags = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		edit : false
	   	};
	},
	componentWillMount(){
 		tagsRequests.getTags(this.state.seeker.mail, x => {
 			this.setState({allTags : x});
 		});
 	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			this.setState({seeker : nextProp.seeker});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	updateTags(tags){
		let seeker = this.state.seeker;
		seeker.skills = _.pluck(tags, 'label');
		this.setState({seeker : seeker});
	},
	done(){
		if(this.props.update){
			this.props.update(this.state.seeker);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Skills</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.TagsBoxShow tags={this.state.seeker.skills} />
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Skills</h3>
					<common.TagsBox allTags={this.state.allTags} preSelected={this.state.seeker.skills} updateTags={this.updateTags} limit="8" oolMessage="You can only pick 8 skills, choose skills you really want to work with !"/>
					<common.MiniIconButton onClick={this.done} icon="svg-check" labels="Done" className="background-color1 inline-block" colorIcon="fill-white" />
				</div>
			</div>
		);
	}
});

let Social = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		edit : false
	   	};
	},
	componentWillMount(){
 		properties.getSocials(x => {
 			if(this.state.seeker && this.state.seeker.social){
 				this.state.seeker.social.forEach(soc => {
 					if(x.id === soc.id){
 						x.url = soc.url;
 						x.selected = soc.selected;
 					}
 				});
 			}
 			this.setState({socials : x});
 		});
 	},
	componentWillReceiveProps(nextProp){
		let socArr = [];
		if(nextProp.seeker){
			if(this.state.socials){
				this.state.socials.forEach(x => {
					let found = false;
					nextProp.seeker.social.forEach(soc => {
						if(x.id === soc.id){
	 						x.url = soc.url;
	 						x.selected = soc.selected;
							socArr.push(x);
							found = true;
	 					}
					});
					if(!found){
						socArr.push(x);
					}
				});
			}
			this.setState({seeker : nextProp.seeker, socials : socArr});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	updateSocial(social){
		this.setState({socials : social});
	},
	done(){
		let seeker = this.state.seeker;
		seeker.social = this.state.socials;
		if(this.props.update){
			this.props.update(seeker);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Social</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.SocialMediasDisplay social={this.state.seeker.social}/>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Social</h3>
					<common.SocialMedias social={this.state.socials} updateSocial={this.updateSocial} enterPressed={this.done}/>
					<common.MiniIconButton onClick={this.done} icon="svg-check" labels="Done" className="background-color1 inline-block" colorIcon="fill-white" />
				</div>
			</div>
		);
	}
});

let Location = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		location : {},
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getLocations(x => {
 			this.setState({locations : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			properties.getLocation(nextProp.seeker.location, ind => {
				this.setState({
					seeker : nextProp.seeker,
					location : ind
				});	
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setLocation(location){
		let emp = this.state.seeker;
		emp.location = location;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Location</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-pin" />
						<span className="font-12px line-height-20px v-align-bottom">{this.state.location.name}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Location</h3>
					<common.CustomSelect setValue={this.setLocation} data={this.state.locations} icon='svg-pin' iconClassProp="icon-avg fill-color1" classProp='margin-bottom-10px v-align-top' isFocus="focus"/>
				</div>
			</div>
		);
	}
});

let Education = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		education : {},
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getDegrees(x => {
 			this.setState({degrees : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			properties.getDegree(nextProp.seeker.education, ind => {
				this.setState({
					seeker : nextProp.seeker,
					education : ind
				});	
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setEducation(education){
		let emp = this.state.seeker;
		emp.education = education;
		if(this.props.update){
			this.props.update(emp);
			this.setState({edit : false});
		}
	},
	render() {
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Education</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-degree" />
						<span className="font-12px line-height-20px v-align-bottom">{this.state.education.name}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Education</h3>
					<common.CustomSelect setValue={this.setEducation} data={this.state.degrees} icon='svg-degree' iconClassProp="icon-avg fill-color1" classProp='margin-bottom-10px v-align-top' isFocus="focus"/>
				</div>
			</div>
		);
	}
});

let BasicBlock = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {active : true}
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			this.setState({
				seeker : nextProp.seeker, 
				activityChoices: [{
					icon : 'svg-check',
					label : 'Active',
					selected : nextProp.seeker.active
				}, {
					icon : 'svg-close',
					label : 'Inactive',
					selected : !nextProp.seeker.active
				}]
			});
		}
	},
	toggleActivity(){
		if(this.props.update){
			let seeker = this.state.seeker;
			seeker.active = !seeker.active;
			this.props.update(seeker);
		}
	},
	render() {
		let iconColor = this.state.seeker.active ? "fill-color1" : "fill-color5",
			overIconColor = this.state.seeker.active ? "fill-color5" : "fill-color1",
			labelColor = this.state.seeker.active ? "color1" : "color5",
			overLabelColor = this.state.seeker.active ? "color5" : "color1",
			modifyShow = {display : this.props.canModify ? "block" : "none"},
			transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			globalShow = {display : this.props.show ? "block" : "none"},
			editButton = this.props.canModify ? '' : "hide",
			privateDate = this.props.canModify ? '' : "hide";
		return (
			<div style={globalShow} className={"center-hover-show padding-5px radius-5px transition-all-ease-3 " + transitionClasses}>
				<div className='pure-g'>
					<div className="pure-u-md-1-2">
						<h4 className="margin-top-10px margin-bottom-10px inline-block">{this.state.seeker.name}</h4>
						<div className={"width-80px " + editButton}>
							<common.Toggle choices={this.state.activityChoices} toggle={this.toggleActivity} iconColor={iconColor} overIconColor={overIconColor} iconClasses='icon-mini ' labelColor={labelColor} overLabelColor={overLabelColor} labelClasses='thin-text font-12px margin-top-1px margin-right-5px'/>
						</div>
					</div>
					<div className="pure-u-md-1-2">
						<div className="float-right margin-right-5px transition-all-ease-3 opacity-0 hover-opacity-100">
							<common.MiniIconButton onClick={this.props.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
						</div>
					</div>
				</div>
				<p className="thin-text font-14px">{this.state.seeker.description}</p>
				<div className='pure-g'>
					<div className="pure-u-md-1-2">
						<div className="thin-text margin-top-10px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-username" />
							<span className="font-12px line-height-20px v-align-bottom">{this.state.seeker.name}</span>
						</div>
						<div className={"thin-text margin-top-10px " + privateDate}>
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-number" />
							<span className="font-12px line-height-20px v-align-bottom">{this.state.seeker.phone}</span>
						</div>
					</div>
					<div className="pure-u-md-1-2">
						<div className="thin-text margin-top-10px margin-left-15px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-job" />
							<span className="font-12px line-height-20px v-align-bottom">{this.state.seeker.jobTitle}</span>
						</div>
						<div className={"thin-text margin-top-10px margin-left-15px " + privateDate}>
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-mail" />
							<span className="font-12px line-height-20px v-align-bottom">{this.state.seeker.email}</span>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

let BasicBlockEdit = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		refSeeker : this.props.seeker ? this.props.seeker : {},
	   		validMail : true
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			this.setState({
				seeker : nextProp.seeker,
				refSeeker : nextProp.seeker
			});
		}
	},
	update(){
		if(this.props.update){
			this.props.update(this.state.seeker);
		}
		if(this.props.cancelEdit){
			this.props.cancelEdit();
		}
	},
	cancel(){
		this.setState({seeker : this.state.refSeeker});
		if(this.props.cancelEdit){
			this.props.cancelEdit();
		}
	},
	changeDescription(value){
		let seeker = this.state.seeker;
		seeker.description = value;
		this.setState({seeker : seeker});
	},
	changeEmail(valid, value){
		if(valid){
			let seeker = this.state.seeker;
			seeker.email = value;
			this.setState({seeker : seeker, validMail : true});
		} else {
			this.setState({validMail : false});
		}
	},
	changePhone(value){
		let seeker = this.state.seeker;
		seeker.phone = value;
		this.setState({seeker : seeker});
	},
	changeName(value){
		let seeker = this.state.seeker;
		seeker.name = value;
		this.setState({seeker : seeker});
	},
	changeJobTitle(value){
		let seeker = this.state.seeker;
		seeker.jobTitle = value;
		this.setState({seeker : seeker});
	},
	render() {
		let globalShow = {display : this.props.show ? "block" : "none"},
			mailProp = this.state.validMail ? '' : 'red-border-bottom';
		return (
			<div style={globalShow} className="padding-5px radius-5px background-white">
				<div className='pure-g'>
					<common.GenericInput keyUp={this.changeName} placeholder='Name' defaultValue={this.state.seeker.name} classProp="block full-width font-18px bold margin-top-5px"/>
					<common.Textarea setValue={this.changeDescription} placeholder='Profile description' defaultValue={this.state.seeker.description} classProp="block full-width margin-top-15px thin-text font-14px"/>
				</div>
				
				<div className='pure-g'>
					<div className="pure-u-md-1-2">
						<div className="thin-text margin-top-10px margin-left-10px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-username" />
							<common.GenericInput keyUp={this.changeName} placeholder='User name' defaultValue={this.state.seeker.name} classProp="font-12px v-align-top"/>
						</div>
						<div className="thin-text margin-top-10px margin-left-10px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-number" />
							<common.GenericInput keyUp={this.changePhone} placeholder='Phone number' defaultValue={this.state.seeker.phone} classProp="font-12px v-align-top"/>
						</div>
					</div>
					<div className="pure-u-md-1-2">
						<div className="thin-text margin-top-10px margin-left-10px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-job" />
							<common.GenericInput keyUp={this.changeJobTitle} placeholder='Job title' defaultValue={this.state.seeker.jobTitle} classProp="font-12px v-align-top"/>
						</div>
						<div className="thin-text margin-top-10px margin-left-10px">
							<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px margin-top-5px" icon="svg-mail" />
							<common.MailInput mailUpdate={this.changeEmail} placeholder='Email' defaultValue={this.state.seeker.email} classProp={"font-12px v-align-top " + mailProp}/>
						</div>
					</div>
				</div>

				<div className='pure-g padding-top-10px'>
					<common.MiniIconButton onClick={this.update} icon="svg-check" labels="Save" className="background-color1 inline-block margin-right-10px" colorIcon="fill-white" />
					<common.MiniIconButton onClick={this.cancel} icon="svg-close" labels="Cancel" className="background-color5 inline-block" colorIcon="fill-white" />
				</div>

			</div>
		);
	}
});

let Industries = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		industry : [],
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getIndustries(x => {
 			this.setState({industries : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			let industryTab = [];
			nextProp.seeker.industries.forEach(x => {
				properties.getIndusty(x, ind => {
					industryTab.push(ind);
				});
			});
			this.setState({
				seeker : nextProp.seeker,
				industry : industryTab,
				defaultIds : _.pluck(industryTab, 'id')
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setIndustry(industries){
		this.setState({defaultIds : _.pluck(industries, 'id')});
	},
	done(){
		let seeker = this.state.seeker;
		seeker.industries = this.state.defaultIds;
		this.setState({edit : !this.state.edit});
		if(this.props.update){
			this.props.update(seeker);
		}
	},
	render() {
		let indName = '';
		if(this.state.industry){
			indName = this.state.industry.map(x => {
				return x.name;
			}).join(', ');
		}
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Industries</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-industry" />
						<span className="font-12px line-height-20px v-align-bottom">{indName}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Industries</h3>
					<common.MultiChoiceList setValue={this.setIndustry} data={this.state.industries} defaultIds={this.state.defaultIds} icon='svg-industry' iconClassProp="icon-avg fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' isFocus="focus"/>
					<common.MiniIconButton onClick={this.done} icon="svg-check" labels="Done" className="background-color1 inline-block" colorIcon="fill-white" />
				</div>
			</div>
		);
	}
});

let JobTypes = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		jobType : [],
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getJobTypes(x => {
 			this.setState({jobTypes : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			let jobTypeTab = []
			nextProp.seeker.jobTypes.forEach(x => {
				properties.getJobType(x, ind => {
					jobTypeTab.push(ind);
				});
			});
			this.setState({
				seeker : nextProp.seeker,
				jobType : jobTypeTab,
				defaultIds : _.pluck(jobTypeTab, 'id')
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setJobTypes(jobTypes){
		this.setState({defaultIds : _.pluck(jobTypes, 'id')});
	},
	done(){
		let seeker = this.state.seeker;
		seeker.jobTypes = this.state.defaultIds;
		this.setState({edit : !this.state.edit});
		if(this.props.update){
			this.props.update(seeker);
		}
	},
	render() {
		let indName = '';
		if(this.state.jobType){
			indName = this.state.jobType.map(x => {
				return x.name;
			}).join(', ');
		}
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Job types</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-industry" />
						<span className="font-12px line-height-20px v-align-bottom">{indName}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Job types</h3>
					<common.MultiChoiceList setValue={this.setJobTypes} data={this.state.jobTypes} defaultIds={this.state.defaultIds} icon='svg-industry' iconClassProp="icon-avg fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' isFocus="focus"/>
					<common.MiniIconButton onClick={this.done} icon="svg-check" labels="Done" className="background-color1 inline-block" colorIcon="fill-white" />
				</div>
			</div>
		);
	}
});

let CompanySize = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : this.props.seeker ? this.props.seeker : {},
	   		size : [],
	   		edit : false
	   	};
	},
	componentDidMount(){
		properties.getCompanySizes(x => {
 			this.setState({sizes : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.seeker){
			let sizeTab = []
			nextProp.seeker.companySizes.forEach(x => {
				properties.getCompanySize(x, ind => {
					sizeTab.push(ind);
				});
			});
			this.setState({
				seeker : nextProp.seeker,
				size : sizeTab,
				defaultIds : _.pluck(sizeTab, 'id')
			});
		}
	},
	edit(){
		this.setState({edit : !this.state.edit});
	},
	setSizes(sizes){
		this.setState({defaultIds : _.pluck(sizes, 'id')});
	},
	done(){
		let seeker = this.state.seeker;
		seeker.companySizes = this.state.defaultIds;
		this.setState({edit : !this.state.edit});
		if(this.props.update){
			this.props.update(seeker);
		}
	},
	render() {
		let indName = '';
		if(this.state.size){
			indName = this.state.size.map(x => {
				return x.name;
			}).join(', ');
		}
		let transitionClasses = this.props.canModify ? 'hover-color-white' : "",
			editButton = this.props.canModify ? '' : "hide",
			display = this.state.edit ? "hide" : "",
			modif = this.state.edit ? "" : "hide";
		return (
			<div className="padding-10px">
				<div className={"center-hover-show radius-5px transition-all-ease-3 " + transitionClasses + ' ' + display}>
					<div className='pure-g'>
						<div className="pure-u-md-1-2">
							<h3 className="margin-left-10px">Company size</h3>
						</div>
						<div className="pure-u-md-1-2">
							<div className="float-right margin-right-10px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.edit} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className="thin-text padding-10px">
						<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-size1" />
						<span className="font-12px line-height-20px v-align-bottom">{indName}</span>
					</div>
				</div>
				<div className={modif}>
					<h3 className="margin-left-10px">Company size</h3>
					<common.MultiChoiceList setValue={this.setSizes} data={this.state.sizes} defaultIds={this.state.defaultIds} icon='svg-size1' iconClassProp="icon-avg fill-color1" classProp='margin-bottom-10px v-align-top' isFocus="focus"/>
					<common.MiniIconButton onClick={this.done} icon="svg-check" labels="Done" className="background-color1 inline-block" colorIcon="fill-white" />
				</div>
			</div>
		);
	}
});

module.exports = {
	Profile : Profile
};