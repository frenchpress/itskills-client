"use strict";

let common = require('./common.js'),
	properties = require('../services/properties.js'),
	yarr = require('../../../node_modules/yarr.js/yarr.js'),
	storage = require('../services/storage.js'),
	auth = require('../services/auth');

let Login = React.createClass({
	mailSeekerUpdate(valid, mail){
		if(valid)
			this.setState({inputSeekerMail: mail});
	},
	pwdSeekerUpdate(valid, password){
		if(valid)
			this.setState({inputSeekerPwd: password});
	},
	mailEmployerUpdate(valid, mail){
		if(valid)
			this.setState({inputEmployerMail: mail});
	},
	pwdEmployerUpdate(valid, password){
		if(valid)
			this.setState({inputEmployerPwd: password});
	},
	seekerLogin(){
		auth.seeker.login(this.state.inputSeekerMail, this.state.inputSeekerPwd, ()=>{
			yarr(properties.routes.seeker.home);
		});
	},
	employerLogin(){
		auth.employer.login(this.state.inputEmployerMail, this.state.inputEmployerPwd, ()=>{
			yarr(properties.routes.employer.home);
		});
	},
	render() {
		return (
			<div id="container" className='pure-g'>
				<div className = 'pure-u-md-1-3 background-white margin-left-90px margin-top-40px thin-border'>
					<h3 className="padding-left-30px padding-top-10px padding-bottom-10px no-margin thin-border-bottom background-color1 white-color">
						<div>Seeker Login</div>
						<div className="font-11px opacity-70">Want to find a job?</div>
					</h3>
					<div className='padding-left-30px padding-right-30px padding-bottom-30px'>
						<div className='width-100pc padding-bottom-10px'>
							<common.Label labels='E-mail *'  />
							<div className='width-100pc'>
								<div className='width-10pc inline-block'>
									<common.Icon icon='svg-mail' classProp='icon-avg fill-color1'/>
								</div>
								<common.MailInput mailUpdate={this.mailSeekerUpdate} classProp='width-90pc margin-bottom-10px v-align-top'/>
							</div>
						</div> 

						<div className='thin-border-bottom padding-bottom-10px'>	
							<common.Label labels='Password *' />
							<div>
								<div className='width-10pc inline-block'>
									<common.Icon icon='svg-password' classProp='icon-avg fill-color1'/>
								</div>
								<common.PwdInput pwdUpdate={this.pwdSeekerUpdate}  classProp='width-90pc margin-bottom-10px v-align-top'/>

							</div>
						</div>
						<common.ValidateButton onClick={this.seekerLogin} submitableUpdate='true' labels='Login' classProp="float-right margin-bottom-20px"/>
					</div>
				</div>
				<div className = 'pure-u-md-1-3 background-white margin-left-90px margin-top-40px thin-border'>
					<h3 className="padding-left-30px padding-top-10px padding-bottom-10px no-margin thin-border-bottom background-color1 white-color">
						<div>Employer Login</div>
						<div className="font-11px opacity-70">Want to post a job?</div>
					</h3>
					<div className='padding-left-30px padding-right-30px padding-bottom-30px'>
						<div className='width-100pc padding-bottom-10px'>
							<common.Label labels='E-mail *'  />
							<div className='width-100pc'>
								<div className='width-10pc inline-block'>
									<common.Icon icon='svg-mail' classProp='icon-avg fill-color1'/>
								</div>
								<common.MailInput mailUpdate={this.mailEmployerUpdate} classProp='width-90pc margin-bottom-10px v-align-top'/>
							</div>
						</div>

						<div className='thin-border-bottom padding-bottom-10px'>	
							<common.Label labels='Password *' />
							<div>
								<div className='width-10pc inline-block'>
									<common.Icon icon='svg-password' classProp='icon-avg fill-color1'/>
								</div>
								<common.PwdInput pwdUpdate={this.pwdEmployerUpdate} classProp='width-90pc margin-bottom-10px v-align-top'/>

							</div>
						</div>
						<common.ValidateButton onClick={this.employerLogin} submitableUpdate='true' labels='Login' classProp="float-right margin-bottom-20px"/>
					</div>
				</div>
			</div>
		);
	}
});



module.exports = {
	Login : Login
};