"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	properties = require('../../services/properties.js'),
	requests = require('../../services/prospect-requests.js'),
	common = require('../common.js');

	let Core = React.createClass({
	render() {
		return (
			<div className="background-color1">
				<div id="outer-container">
					<Header />
					<Topbar />
					<Mechanism />
					<Partners />
					<Contact />
				</div>
			</div>
		);
	}
});

let Header = React.createClass({
	getInitialState() {
		return {validMail : false, sent : false};
	},
	changeMail(event){
		let mail = event.target.value,
        	re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    	if(mail.length > 0 && re.test(mail)){
    		this.setState({mail : mail, validMail: true});
    	} else {
    		this.setState({mail : mail, validMail: false});
    	}
	},
	sendMail(){
		if(this.state.validMail){
			requests.createSeekerProspect(this.state.mail, succ => {
				if(succ){
					this.setState({sent : true});
				}
			});
		}
	},
	goToEmployer(){
		yarr(properties.routes.emp);
	},
	render() {
		let sendIconClasses = this.state.validMail ? 'fill-color1' : 'fill-color3',
			wrapperClasses = this.state.validMail ? 'pointer' : '',
			thanksClasses = this.state.sent ? '' : 'hide',
			prospectsClasses = this.state.sent ? 'hide' : '';
		return (
			<div className="background-diver white-color">

				<div className="container pure-g thin-border-bottom-white">
					<div className="pure-u-md-1-3 pure-u-sm-1-2 pure-u-xs-1-2">
						<div className="semi-bold font-20px letter-spacing-2px line-height-50px margin-left-5px">Narwhal</div>
					</div>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div className='bloc-centered width-50px height-50px'>
							<common.Icon icon='svg-logo' classProp='icon-medium fill-white'/>
						</div>
					</div>
					<div className="pure-u-md-1-3 pure-u-sm-1-2 pure-u-xs-1-2 hide-xs">
						<div onClick={this.goToEmployer} className='float-right margin-right-5px transition-all-ease-3 pointer thin-text font-18px border-white-2px radius-5px padding-left-15px padding-right-15px padding-top-5px padding-bottom-8px margin-top-4px hover-color-white hover-color1'>
							For company
						</div>
					</div>
				</div>

				<div className="container pure-g margin-top-120px padding-bottom-100px">

					<div className={"pure-u-md-1-2 pure-u-sm-1-1 " + prospectsClasses}>
						<div className="font-26px thin-text letter-spacing-1px margin-left-5px margin-right-5px">
							The best jobs without hassle
						</div>
						<div className="font-16px thin-text letter-spacing-1px margin-top-15px margin-left-5px margin-right-5px">
							<em>"By IT guys for IT guys."</em>
						</div>

						<div className="font-20px semi-bold letter-spacing-2px margin-top-40px margin-bottom-5px margin-left-5px margin-right-5px">Get a Beta invite</div>
						<div className="margin-left-5px margin-right-5px">
							<div className="inline-block background-white v-align-top padding-left-15px padding-right-15px padding-top-10px padding-bottom-5px radius-left-5px">
								<common.Icon icon='svg-mail' classProp='icon-avg fill-color1'/>
							</div>

							<input onKeyUp={this.changeMail} type='text' className="color1 background-white width-250px inline-block v-align-top font-18px thin-text padding-top-8px padding-bottom-9px" placeholder="@Email address"/>

							<div onClick={this.sendMail} className={"inline-block background-white v-align-top padding-left-15px padding-right-15px padding-top-10px padding-bottom-5px radius-right-5px " + wrapperClasses}>
								<common.Icon icon='svg-send' classProp={'icon-avg transition-all-ease-3 ' + sendIconClasses}/>
							</div>
						</div>
					</div>

					<div className={"pure-u-md-1-2 pure-u-sm-1-1 padding-top-60px padding-bottom-60px " + thanksClasses}>
						<div className="font-26px thin-text letter-spacing-1px margin-left-5px margin-right-5px">
							Thank you for joining the pack !
							<br/>
							We will keep you posted of our moves and count you in.
						</div>
						<div className="font-16px thin-text letter-spacing-1px margin-top-15px margin-left-5px margin-right-5px">
							<em>- Cheers, the Narwhal crew</em>
						</div>
					</div>

					<div className="pure-u-md-1-2 pure-u-sm-1-1">

					</div>

				</div>
			</div>
		);
	}
});

let Topbar = React.createClass({
	goToEmployer(){
		yarr(properties.routes.emp);
	},
	mechanism(){
		window.scrollTo(0, 535);
	},
	partners(){
		window.scrollTo(0, 1030);
	},
	contact(){
		window.scrollTo(0, 1185);
	},
	render() {
		return (
			<div className="background-color3">
				<div className="container pure-g thin-border-top thin-border-bottom">
					<div onClick={this.mechanism} className="inline-block opacity-70 hover-opacity-100 pointer transition-all-ease-3 margin-right-30px line-height-60px">
						Features
					</div>
					<div onClick={this.partners} className="inline-block opacity-70 hover-opacity-100 pointer transition-all-ease-3 margin-right-30px line-height-60px margin-left-5px">
						Our companies
					</div>
					<div onClick={this.contact} className="inline-block opacity-70 hover-opacity-100 pointer transition-all-ease-3 line-height-60px">
						Contact
					</div>
					<div onClick={this.goToEmployer} className='hide-xs opacity-60 float-right transition-all-ease-3 pointer thin-text font-18px thin-border-color1 radius-5px padding-left-15px padding-right-15px padding-top-5px padding-bottom-8px margin-top-10px hover-opacity-100'>
						For company
					</div>
				</div>
			</div>
		);
	}
});

let Partners = React.createClass({
	gotoVizbot(){
		console.log('Go to Vizbot');
	},
	gotoWG(){
		console.log('Go to WG');
	},
	gotoDA(){
		console.log('Go to DA');
	},
	gotoHashBang(){
		console.log('Go to Hashbang');
	},
	render() {
		return (
			<div className="background-white">
				<div className="container pure-g padding-top-50px padding-bottom-50px">
					<div className="pure-u-md-1-4 pure-u-sm-1-4">
						<img onClick={this.gotoWG} className="pointer block bloc-centered width-200px height-55px grey-scale hover-no-grey-scale transition-all-ease-5" src="../pictures/brands/showguizmo.png" />
					</div>
					<div className="pure-u-md-1-4 pure-u-sm-1-4">
						<img onClick={this.gotoDA} className="pointer block bloc-centered width-140px height-55px grey-scale hover-no-grey-scale transition-all-ease-5" src="../pictures/brands/devacademy.png" />
					</div>
					<div className="pure-u-md-1-4 pure-u-sm-1-4">
						<img onClick={this.gotoVizbot} className="pointer block bloc-centered width-55px height-55px grey-scale hover-no-grey-scale transition-all-ease-5" src="../pictures/brands/vizbot.jpg" />
					</div>
					<div className="pure-u-md-1-4 pure-u-sm-1-4">
						<img onClick={this.gotoHashBang} className="pointer block bloc-centered width-270px height-55px grey-scale hover-no-grey-scale transition-all-ease-5" src="../pictures/brands/hashbang.png" />
					</div>
				</div>
			</div>
		);
	}
});

let Mechanism = React.createClass({
	render() {
		return (
			<div className="background-white">
				<div className="text-centered padding-top-60px font-28px thin-text letter-spacing-1px">
					What we can do for you
				</div>
				<div className="container pure-g">
					<div className="pure-u-sm-1-2 pure-u-md-1-4 padding-top-60px padding-bottom-40px">
						<div className="width-80px height-80px bloc-centered">
							<common.Icon icon='svg-algorithm' classProp='icon-large fill-color2'/>
						</div>
						<div className="padding-5px font-14px margin-top-10px semi-bold text-centered">Advanced matching algorithm</div>
						<div className="padding-5px font-14px text-centered opacity-70">
							We use every piece of given information to suggest only the best jobs for you
						</div>
					</div>
					<div className="pure-u-sm-1-2 pure-u-md-1-4 padding-top-60px padding-bottom-40px">
						<div className="width-80px height-80px bloc-centered background-color2 rounded-radius">
							<common.Icon icon='svg-hourglass' classProp='icon-large fill-white'/>
						</div>
						<div className="padding-5px font-14px margin-top-10px semi-bold text-centered">We value your time</div>
						<div className="padding-5px font-14px text-centered opacity-70">
							Don't wase your time digging into a huge pile of unrelevant job descriptions. Browse only througout the best fits
						</div>
					</div>
					<div className="pure-u-sm-1-2 pure-u-md-1-4 padding-top-60px padding-bottom-40px">
						<div className="width-80px height-80px bloc-centered background-color2 rounded-radius">
							<common.Icon icon='svg-speed' classProp='icon-more fill-white margin-top-10px margin-left-10px'/>
						</div>
						<div className="padding-5px font-14px margin-top-10px semi-bold text-centered">Quick and easy</div>
						<div className="padding-5px font-14px text-centered opacity-70">
							Quickly create your profile once and share it with potential employers easily each time you need it.
						</div>
					</div>
					<div className="pure-u-sm-1-2 pure-u-md-1-4 padding-top-60px padding-bottom-40px">
						<div className="width-80px height-80px bloc-centered background-color2 rounded-radius">
							<common.Icon icon='svg-bilateral' classProp='icon-more fill-white margin-top-10px margin-left-10px'/>
						</div>
						<div className="padding-5px font-14px margin-top-10px semi-bold text-centered">Bilateral interaction</div>
						<div className="padding-5px font-14px text-centered opacity-70">
							Proactivity is now shared. Every one is able to get in touch as soon as the interest is mutual.
						</div>
					</div>
					<hr className="margin-top-50px no-margin-bottom opacity-70"/>
				</div>
			</div>
		);
	}
});

let Contact = React.createClass({
	render() {
		return (
			<div className="background-contact white-color">
				<div className="container pure-g padding-top-40px padding-bottom-40px">
					<div className="pure-u-md-1-4 pure-u-sm-1-2">
						<div className="letter-spacing-1px margin-bottom-40px margin-left-5px">
							Contact & feedback
						</div>
						<div className="margin-left-5px">
							<a href="mailto:frank@narwhal.co.nz">
								<div className="transition-all-ease-3 pointer opacity-70 hover-opacity-100">
									<div className="inline-block v-align-top thin-text font-18px margin-right-5px">@</div>
									<div className="padding-top-3px inline-block v-align-top thin-text font-14px">Frank Bassard</div>
								</div>
							</a>
							<a href="mailto:mohamed@narwhal.co.nz">
								<div className="transition-all-ease-3 pointer opacity-70 hover-opacity-100 margin-top-10px">
									<div className="inline-block v-align-top thin-text font-18px margin-right-5px">@</div>
									<div className="padding-top-3px inline-block v-align-top thin-text font-14px">Mohamed Mokhtari</div>
								</div>
							</a>
						</div>
					</div>
					<div className="pure-u-md-1-4 pure-u-sm-1-2">
						<div className="letter-spacing-1px margin-bottom-40px">
							Social
						</div>
						<div>
							<div className="center-hover-show">
								<a href="">
									<common.Icon icon='svg-social-facebook' classProp='icon-mini fill-white'/>
									<div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Facebook</div>
								</a>
							</div>
							<div className="margin-top-10px center-hover-show">
								<a href="">
									<common.Icon icon='svg-social-twitter' classProp='icon-mini fill-white'/>
									<div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Twitter</div>
								</a>
							</div>
							<div className="margin-top-10px center-hover-show">
								<a href="">
									<common.Icon icon='svg-social-google-plus' classProp='icon-mini fill-white'/>
									<div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Google +</div>
								</a>
							</div>
						</div>
					</div>
					<div className="pure-u-md-1-4 offset-md-1-4 hide-sm">
						<div>
							<img src="../pictures/misc/new-zealand.png" className="width-180px height-200px"/>
							<common.Icon icon='svg-target' classProp='icon-mini fill-white map-target'/>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = {
	Core : Core
};