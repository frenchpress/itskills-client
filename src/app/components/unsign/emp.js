let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	properties = require('../../services/properties.js'),
	requests = require('../../services/prospect-requests.js'),
	Rating = require('../../../../node_modules/Rating/js/rating.js'),
	common = require('../common.js');

let Emp = React.createClass({
	render() {
		return (
			<div className="background-color1">
				<div id="outer-container">
					<Header />
					<Topbar />
					<Circle />
					<Howto/>
					<Contact />
				</div>
			</div>
		);
	}
});

let Header = React.createClass({
	getInitialState() {
		return {validMail : false, sent : false};
	},
	changeMail(event){
		let mail = event.target.value,
        	re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    	if(mail.length > 0 && re.test(mail)){
    		this.setState({mail : mail, validMail: true});
    	} else {
    		this.setState({mail : mail, validMail: false});
    	}
	},
	sendMail(){
		if(this.state.validMail){
			requests.createEmployerProspect(this.state.mail, succ => {
				if(succ){
					this.setState({sent : true});
				}
			});
		}
	},
	goToSeeker(){
		yarr(properties.routes.core);
	},
	render() {
		let sendIconClasses = this.state.validMail ? 'opacity-100' : 'opacity-20',
			wrapperClasses = this.state.validMail ? 'pointer' : '',
			thanksClasses = this.state.sent ? '' : 'hide',
			prospectsClasses = this.state.sent ? 'hide' : '';
		return (
			<div className="background-above white-color">

				<div className="container pure-g thin-border-bottom">
					<div className="pure-u-md-1-3 pure-u-sm-1-2 pure-u-xs-1-2">
						<div className="semi-bold font-20px letter-spacing-2px line-height-50px margin-left-5px">Narwhal</div>
					</div>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div className='bloc-centered width-50px height-50px'>
							<common.Icon icon='svg-logo' classProp='icon-medium fill-white'/>
						</div>
					</div>
					<div className="pure-u-md-1-3 pure-u-sm-1-2 pure-u-xs-1-2 hide-xs">
						<div onClick={this.goToSeeker} className='float-right margin-right-5px transition-all-ease-3 pointer thin-text font-18px border-color1-2px radius-5px padding-left-15px padding-right-15px padding-top-5px padding-bottom-8px margin-top-4px hover-color-white hover-white hover-color-color1'>
							For seeker
						</div>
					</div>
				</div>

				<div className="container pure-g padding-top-150px padding-bottom-350px">

					<div className={"pure-u-md-1-2 pure-u-sm-1-1 " + prospectsClasses}>
						<div className="font-26px thin-text letter-spacing-1px margin-left-5px margin-right-5px">
							Heaps of talented candidates sorted to match your specific needs.
						</div>

						<div className="font-20px semi-bold letter-spacing-2px margin-top-40px margin-bottom-5px margin-left-5px margin-right-5px">Get a Beta invite</div>
						<div className="margin-left-5px margin-right-5px">
							<div className="inline-block background-color3 v-align-top padding-left-15px padding-right-15px padding-top-10px padding-bottom-5px radius-left-5px">
								<common.Icon icon='svg-mail' classProp='icon-avg fill-color1'/>
							</div>

							<input onKeyUp={this.changeMail} type='text' className="background-color3 color1 width-250px inline-block v-align-top font-18px thin-text padding-top-8px padding-bottom-9px" placeholder="@Email address"/>

							<div onClick={this.sendMail} className={"inline-block background-color3 v-align-top padding-left-15px padding-right-15px padding-top-10px padding-bottom-5px radius-right-5px " + wrapperClasses}>
								<common.Icon icon='svg-send' classProp={'icon-avg transition-all-ease-3 fill-color1 ' + sendIconClasses}/>
							</div>
						</div>
					</div>

					<div className={"pure-u-md-1-2 pure-u-sm-1-1 " + thanksClasses}>
						<div className="font-26px thin-text letter-spacing-1px margin-left-5px margin-right-5px">
							Thank you for joining the pack !
							<br/>
							We will keep you posted of our moves and count you in.
						</div>
						<div className="font-16px thin-text letter-spacing-1px margin-top-15px margin-left-5px margin-right-5px">
							<em>- Cheers, the Narwhal crew</em>
						</div>
					</div>

					<div className="pure-u-md-1-2 pure-u-sm-1-1">

					</div>

				</div>
			</div>
		);
	}
});

let Topbar = React.createClass({
	goToSeeker(){
		yarr(properties.routes.core);
	},
	mechanism(){
		window.scrollTo(0, 740);
	},
	howto(){
		window.scrollTo(0, 1285);
	},
	contact(){
		window.scrollTo(0, 1785);
	},
	render() {
		return (
			<div className="background-color3">
				<div className="container pure-g thin-border-top thin-border-bottom">
					<div onClick={this.mechanism} className="inline-block opacity-70 hover-opacity-100 pointer transition-all-ease-3 margin-left-5px margin-right-30px line-height-60px">
						Targeting system
					</div>
					<div onClick={this.howto} className="inline-block opacity-70 hover-opacity-100 pointer transition-all-ease-3 line-height-60px margin-right-30px">
						How to
					</div>
					<div onClick={this.contact} className="inline-block opacity-70 hover-opacity-100 pointer transition-all-ease-3 line-height-60px">
						Contact
					</div>
					<div onClick={this.goToSeeker} className='hide-xs opacity-60 float-right transition-all-ease-3 pointer thin-text font-18px thin-border-color1 radius-5px padding-left-15px padding-right-15px padding-top-5px padding-bottom-8px margin-top-10px hover-opacity-100'>
						For seeker
					</div>
				</div>
			</div>
		);
	}
});

let Circle = React.createClass({
	componentDidMount(){
		new Rating(properties.ratingProps2("rating", 80));	
	},
	render() {
		return (
			<div className="background-color2 white-color">
				<div className="container pure-g padding-top-100px padding-bottom-100px">
					<div className="pure-u-md-1-3 pure-u-sm-1-2">

						<div className="max-width-250px bloc-centered text-centered margin-top-20px">
							<div className="padding-5px font-14px margin-top-10px semi-bold">
								<common.Icon icon='svg-target2' classProp='icon-mini fill-white'/>
								<span className="margin-left-5px v-align-top">Precise targeting</span>
							</div>
							<div className="padding-5px font-14px opacity-70">
								We target the best profiles to match your job offers
							</div>
						</div>

						<div className="max-width-250px bloc-centered text-centered margin-top-50px">
							<div className="padding-5px font-14px margin-top-10px semi-bold">
								<common.Icon icon='svg-hourglass' classProp='icon-mini fill-white'/>
								<span className="margin-left-5px v-align-top">Time sparing</span>
							</div>
							<div className="padding-5px font-14px opacity-70">
								Don't waste time analysing unrelevant profiles, focus on the best ones
							</div>
						</div>

					</div>
					<div className="pure-u-md-1-3 hide-sm">
						<div className="font-26px thin-text letter-spacing-1px text-centered margin-bottom-40px">
							Accurate targeting
						</div>
						<div id="rating" className="bloc-centered width-200px"></div>
					</div>
					<div className="pure-u-md-1-3 pure-u-sm-1-2">

						<div className="max-width-250px bloc-centered text-centered margin-top-20px">
							<div className="padding-5px font-14px margin-top-10px semi-bold">
								<common.Icon icon='svg-bilateral' classProp='icon-mini fill-white'/>
								<span className="margin-left-5px v-align-top">Bilateral interaction</span>
							</div>
							<div className="padding-5px font-14px opacity-70">
								Take the initiative and initiate contact with the best candidates
							</div>
						</div>

						<div className="max-width-250px bloc-centered text-centered margin-top-50px">
							<div className="padding-5px font-14px margin-top-10px semi-bold">
								<common.Icon icon='svg-dollar' classProp='icon-mini fill-white'/>
								<span className="margin-left-5px v-align-top">No success fees</span>
							</div>
							<div className="padding-5px font-14px opacity-70">
								As soon as you have a successful relationaship with a candidate we leap out of the process
							</div>
						</div>

					</div>
				</div>
			</div>
		);
	}
});

let Howto = React.createClass({

	render() {
		return (
			<div className="background-white color1">
				<div className="container pure-g padding-top-80px padding-bottom-80px">
					
					<div className="pure-u-md-1-2">
						<div className="inline-block width-10pc v-align-top">
							<div className="bloc-centered width-30px height-30px text-centered font-22px thin-text">1</div>
						</div>
						<div className="inline-block width-90pc v-align-top">
							<div className="padding-top-5px padding-bottom-5px">Describe your company</div>
							<div className="opacity-80 font-14px">Create your account and shape the public facing picture of your company</div>
						</div>
					</div>
					<div className="pure-u-md-1-2 margin-top-90px">
						<div className="inline-block width-90pc v-align-top">
							<div className="padding-top-5px padding-bottom-5px text-right">Expose your needs</div>
							<div className="opacity-80 font-14px text-right">Use our template made for IT jobs to quickly create your job offers</div>
						</div>
						<div className="inline-block width-10pc">
							<div className="bloc-centered width-30px height-30px text-centered font-22px thin-text">2</div>
						</div>
					</div>
					<div className="pure-u-md-1-2 margin-top-40px">
						<div className="inline-block width-10pc">
							<div className="bloc-centered width-30px height-30px text-centered font-22px thin-text">3</div>
						</div>
						<div className="inline-block width-90pc v-align-top">
							<div className="padding-top-5px padding-bottom-5px">Find and be found</div>
							<div className="opacity-80 font-14px">Go through the most fitting profiles for each position. Declare your interest for the best prospects or wait to get their attention</div>
						</div>
					</div>
					<div className="pure-u-md-1-2 margin-top-130px">
						<div className="inline-block width-90pc v-align-top">
							<div className="padding-top-5px padding-bottom-5px text-right">Contact and be contacted</div>
							<div className="opacity-80 font-14px text-right">If interest is mutual, each side is able to initiate contact. Once you have reach this step, relationaship is yours to handle kiwi style</div>
						</div>
						<div className="inline-block width-10pc">
							<div className="bloc-centered width-30px height-30px text-centered font-22px thin-text">4</div>
						</div>
					</div>

				</div>
			</div>
		);
	}
});

let Contact = React.createClass({
	render() {
		return (
			<div className="background-contact white-color">
				<div className="container pure-g padding-top-40px padding-bottom-40px">
					<div className="pure-u-md-1-4 pure-u-sm-1-2">
						<div className="letter-spacing-1px margin-bottom-40px margin-left-5px">
							Contact & feedback
						</div>
						<div className="margin-left-5px">
							<a href="mailto:frank@narwhal.co.nz">
								<div className="transition-all-ease-3 pointer opacity-70 hover-opacity-100">
									<div className="inline-block v-align-top thin-text font-18px margin-right-5px">@</div>
									<div className="padding-top-3px inline-block v-align-top thin-text font-14px">Frank Bassard</div>
								</div>
							</a>
							<a href="mailto:mohamed@narwhal.co.nz">
								<div className="transition-all-ease-3 pointer opacity-70 hover-opacity-100 margin-top-10px">
									<div className="inline-block v-align-top thin-text font-18px margin-right-5px">@</div>
									<div className="padding-top-3px inline-block v-align-top thin-text font-14px">Mohamed Mokhtari</div>
								</div>
							</a>
						</div>
					</div>
					<div className="pure-u-md-1-4 pure-u-sm-1-2">
						<div className="letter-spacing-1px margin-bottom-40px">
							Social
						</div>
						<div>
							<div className="center-hover-show">
								<a href="">
									<common.Icon icon='svg-social-facebook' classProp='icon-mini fill-white'/>
									<div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Facebook</div>
								</a>
							</div>
							<div className="margin-top-10px center-hover-show">
								<a href="">
									<common.Icon icon='svg-social-twitter' classProp='icon-mini fill-white'/>
									<div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Twitter</div>
								</a>
							</div>
							<div className="margin-top-10px center-hover-show">
								<a href="">
									<common.Icon icon='svg-social-google-plus' classProp='icon-mini fill-white'/>
									<div className="inline-block v-align-top margin-left-5px thin-text font-14px transition-all-ease-3 opacity-0 hover-opacity-100">Google +</div>
								</a>
							</div>
						</div>
					</div>
					<div className="pure-u-md-1-4 offset-md-1-4 hide-sm">
						<div>
							<img src="../pictures/misc/new-zealand.png" className="width-180px height-200px"/>
							<common.Icon icon='svg-target' classProp='icon-mini fill-white map-target'/>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = {
	Emp : Emp
};