	"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	seekerCommon = require('./common.js'),
	requests = require('../../services/seeker-requests.js'),
	tagsRequests = require('../../services/tag-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	Stepper = require('../../../../node_modules/SimpleSteps/js/stepper.js'),
	auth = require('../../services/auth.js'),
	steps = ['Basic info', 'Skills', 'Media', 'Completed'];

let Reg1Form = React.createClass({
	getInitialState() {
   	 return {
   	 		stateMail : false,
   	 		statePwd : false,
   	 		stateName: false,
   	 		submitable : false,
   	 		mailErrVisible : false,
   	 		pwdErrVisibility : false,
   	 		nameErrVisibility : false,
   	 		formErrVisibility : false,
   	 		progressBarVisibility : {
   	 			visibility : 'hidden'
   	 		},
   	 		strength : 0,
   	 		color : '',
   	 		mailErrorLabel : "Invalid email address",
		};
 	},
	mailUpdate(valid, mail){
		if(valid){
			requests.isMailAvailable(mail, (ans) => {
				if(ans){
					this.setState({stateMail : true, submitable : this.state.statePwd && this.state.stateName, mailErrVisible: false, inputMail: mail}); 
				} else {
					this.setState({stateMail : false, submitable : false, mailErrVisible: true, mailErrorLabel : "Email already in use"});
				}
			});
		} else{
			this.setState({stateMail : false, submitable : false, mailErrVisible: true, mailErrorLabel : "Invalid email address"});
		}
	},
	pwdUpdate(valid, password){
		if(valid){
			this.setState({statePwd : true, submitable : this.state.stateMail && this.state.stateName, pwdErrVisibility: false, progressBarVisibility : {visibility : 'hidden'}, inputPwd: password});
		} else{
			this.setState({statePwd : false, submitable : false, pwdErrVisibility: true, progressBarVisibility : {visibility : 'hidden'}});
		}
	},
	nameUpdate(exist, name){
		if(exist){
			this.setState({stateName : true, submitable : this.state.stateMail && this.state.statePwd, nameErrVisibility: false, inputName: name});
		} else {
			this.setState({stateName : false, submitable : false, nameErrVisibility: true});
		}
	},
	pwdStrength(strength){
		let colors = ['#FF530D', '#293540', '#79BDE0'], use = 0;
		if(strength > 66){
			use = 2;
		} else if(strength > 33){
			use = 1;
		}
		this.setState({progressBarVisibility : {visibility : 'visible'}, strength : strength, color : colors[use]});
	},
	testEnter(event){
		if(event.which === 13){
			this.register();
		}
	},
	register(){
		if(this.state.submitable){
			requests.create(this.state.inputMail, this.state.inputPwd, this.state.inputName, (ans, error) => {
				if(ans){
					storage.set('seeker', {email : this.state.inputMail, password : this.state.inputPwd, name : this.state.inputName});
					yarr(properties.routes.seeker.register2);
				} else {
					this.setState({formError: error, formErrVisibility : true});
				}
			});
		}
	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className = 'pure-u-md-1-2 offset-md-1-4 pure-u-sm-3-4 margin-top-40px'>
						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">Sign up</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">Take 5 minutes to create you account.</div>
						</div>

						<form className='padding-left-30px padding-right-30px padding-bottom-30px' onKeyUp={this.testEnter}>
							
							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Name *'}  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-username' classProp='icon-mini fill-color1'/>
									</div>
									<common.GenericInput compulsory='true' exists={this.nameUpdate} classProp='width-80pc margin-bottom-10px v-align-top' isFocus='focus' placeholder="your name"/>
									<common.ErrorBox labels='Name is required' visible={this.state.nameErrVisibility}/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'E-mail *'}  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-mail' classProp='icon-mini fill-color1'/>
									</div>
									<common.MailInput mailUpdate={this.mailUpdate} classProp='width-80pc margin-bottom-10px v-align-top' placeholder="e.g. your@mail.com"/>
									<common.ErrorBox labels={this.state.mailErrorLabel} visible={this.state.mailErrVisible}/>
								</div>
							</div>

							<div className='padding-bottom-10px'>	
								<common.Label labels={'Password *'} />
								<div>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-password' classProp='icon-mini fill-color1'/>
									</div>
									<common.PwdInput pwdUpdate={this.pwdUpdate} pwdStrength={this.pwdStrength} enterPressed={this.register} classProp='width-80pc margin-bottom-10px v-align-top' placeholder="password : 6 characters min"/>
									<common.ErrorBox labels='Your password must have at between 6 and 30 characters' visible={this.state.pwdErrVisibility}/>
									<div className = 'pure-g' style={this.state.progressBarVisibility}>
										<div className = 'pure-u-md-1-6 text-right padding-right-10px font-12px'>
											{this.state.strength}%
										</div>
										<div className = 'pure-u-md-2-3'>
											<common.ProgressBar color={this.state.color} percent={this.state.strength}/>
										</div>
									</div>

								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate={this.state.submitable} labels='Next' classProp="margin-bottom-20px"/>
								<common.ErrorBox labels={this.state.formError} visible={this.state.formErrVisibility}/>
							</div>
						</form>
					</div>
				</div>
				<common.Bottombar />
			</div>
			
		);
	}
});

let Reg2Form = React.createClass({
	getInitialState() {
		return {
			seeker : storage.get('seeker'),
			errorJobTitleVisibility : false,
			submitable : false,
			defaultJobTypes : [1],
			defaultId : 2,
			statusDefaultId : 1
		};
 	},
 	componentWillMount(){
 		properties.getStatuses(x => {
 			this.setState({statuses : x});
 		});
 		properties.getLocations(x => {
 			this.setState({locations : x});
 		});
 		properties.getXps(x => {
 			this.setState({xps : x});
 		});
 		properties.getJobTypes(x => {
 			this.setState({jobTypes : x});
 		});
 	},
 	componentDidMount(){
 		let stepper = new Stepper(properties.stepperProps("stepper", steps, 0));
 		stepper.changeStep(1);
 	},
 	jobTitleUpdate(bool, value, cb){
 		let submitPreReqs = this.state.location && this.state.jobType.length;
 		this.setState({errorJobTitleVisibility : !bool, jobTitle : value, submitable : submitPreReqs && bool}, cb);
 	},
 	webEnter(value){
 		this.websiteUpdate(!!value.length, value, function(){
 			this.register();
 		});
 	},
 	testEnter(event){
		if(event.which === 13){
			this.register();
		}
	},
	setStatus(status){
		this.setState({status : status, statusDefaultId : status.id});
	},
	setXP(xp){
		this.setState({xp : xp, defaultId : xp.id});
	},
	setJobType(jobType){
		let submitPreReqs = this.state.jobTitle && this.state.location;
		this.setState({
			jobType : jobType, 
			submitable : submitPreReqs && jobType.length,
			defaultJobTypes : _.pluck(jobType, 'id')
		});

	},
	locationUpdate(block){
		let submitPreReqs = this.state.jobTitle && this.state.jobType.length;
 		this.setState({location : block, submitable : submitPreReqs});
	},
 	register(){
 		if(this.state.submitable){
 			requests.update(this.state.seeker.email, {
				jobTitle : this.state.jobTitle,
				location : this.state.location,
				status : this.state.status.id, 
				xp : this.state.xp.id,
				jobTypes : _.pluck(this.state.jobType, 'id')
	 		}, (ans, error) => {
 				let seeker = storage.get('seeker');
 				storage.set('seeker', seeker);
 				yarr(properties.routes.seeker.register3);
 			});
 		}
 	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div id="stepper" className='margin-top-100px inner-svg-centered'></div>
					</div>
					<div className = 'pure-u-md-1-2 pure-u-sm-3-4 margin-top-40px'>

						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">Welcome {this.state.seeker.name}</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">Thank you for joining us</div>
						</div>

						<div className='padding-left-30px padding-right-30px padding-bottom-30px' onKeyUp={this.testEnter}>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Job title *'}  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block'>
										<common.Icon icon='svg-username' classProp='icon-mini fill-color1'/>
									</div>
									<common.GenericInput compulsory='true' exists={this.jobTitleUpdate} classProp='width-80pc margin-bottom-10px v-align-top' isFocus='focus'/>
									<common.ErrorBox labels='Job title is required' visible={this.state.errorJobTitleVisibility}/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Status *'}  />
								<div className='margin-left-5pc width-90pc'>
									<common.TinyList defaultId={this.state.statusDefaultId} setValue={this.setStatus} data={this.state.statuses} icon='svg-status' iconClassProp="icon-mini fill-color1" classProp='width-90pc margin-bottom-10px v-align-top'/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Location *'}  />
								<div className='margin-left-5pc width-90pc'>
									<common.CustomSelect data={this.state.locations} setValue={this.locationUpdate} icon='svg-web' iconClassProp='icon-mini fill-color1'/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Experience *'}  />
								<div className='margin-left-5pc width-90pc'>
									<common.TinyList defaultId={this.state.defaultId} setValue={this.setXP} data={this.state.xps} icon='svg-degree' iconClassProp="icon-mini fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' />
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Job Type *'}  />
								<div className='margin-left-5pc width-90pc'>
									<common.MultiChoiceList defaultIds={this.state.defaultJobTypes} setValue={this.setJobType} data={this.state.jobTypes} icon='svg-calendar' iconClassProp="icon-mini fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' />
								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate={this.state.submitable} labels='Next' classProp="margin-bottom-20px"/>
							</div>
						</div>
					</div>
				</div>
				<common.Bottombar />
			</div>
			
		);
	}
});

let Reg3Form = React.createClass({
	getInitialState() {
   	 return {
   	 	seeker : storage.get('seeker'),
   	 	defaultId : 1
   	 };
 	},
 	componentWillMount(){
 		properties.getDegrees(x => {
 			this.setState({degrees : x});
 		});
 	},
	componentDidMount() {
		let stepper = new Stepper(properties.stepperProps("stepper", steps, 1));
 		stepper.changeStep(2);
		tagsRequests.getTags(this.state.seeker.email, tags => {
			this.setState({tags : tags});
		});
	},
	updateTags(tags){
		this.setState({selectedTags : tags});
	},
	register(){
		if(this.state.selectedTags && this.state.selectedTags.length || this.state.education){
			requests.update(this.state.seeker.email, {
				skills : _.pluck(this.state.selectedTags, 'label'),
				education : this.state.education.id
			}, (ans, msg) => {
				yarr(properties.routes.seeker.register4);
			});
		}else{
			yarr(properties.routes.seeker.register4);
		}
	},
	setEducation(edu){
		this.setState({education : edu, defaultId : edu.id});
	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div id="stepper" className='margin-top-100px inner-svg-centered'></div>
					</div>
					<div className = 'pure-u-md-1-2 pure-u-sm-3-4 margin-top-40px'>
						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">Skills</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">JS guru ? ROR expert ? Let the world know what your are the best at.</div>
						</div>

						<div className='padding-left-30px padding-right-30px padding-bottom-30px'>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels='Education'  />
								<div className='margin-left-5pc width-90pc'>
									<common.TinyList defaultId={this.state.defaultId} setValue={this.setEducation} data={this.state.degrees} icon='svg-degree' iconClassProp="icon-mini fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' isFocus="focus"/>
								</div>
							</div>
							
							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels='Skills'  />
								<div className='width-100pc'>
									<div className='margin-left-5pc width-10pc inline-block v-align-top'>
										<common.Icon icon='svg-laptop' classProp='icon-mini fill-color1'/>
									</div>
									<div className='width-80pc inline-block'>
										<common.TagsBox allTags={this.state.tags} limit="8" oolMessage="You can only pick 8 skills, choose skills you really want to work with !" updateTags={this.updateTags}/>
									</div>
									
								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate="true" labels='Next' classProp="margin-bottom-20px"/>
							</div>
						</div>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Reg4Form = React.createClass({
	getInitialState() {
		return {
			seeker : storage.get('seeker')
		};
 	},
 	componentWillMount(){
 		properties.getSocials(x => {
 			this.setState({social : x});
 		});
 	},
 	componentDidMount(){
 		let stepper = new Stepper(properties.stepperProps("stepper", steps, 2));
 		stepper.changeStep(3);
 	},
 	handleUpload(files){
 		if(files[0]){
 			requests.addCv(this.state.seeker.email, files[0], (x, y) => {
 				if(!x){
 					this.setState({fileErrorMsg : y});
 				} else if(this.state.fileErrorMsg){
 					this.setState({fileErrorMsg : undefined});
 				}
 			});
 		}
 	},
 	updateSocial(social){
 		this.setState({social : social});
 	},
 	testEnter(event){
		if(event.which === 13){
			this.register();
		}
	},
 	register(){
 		let seeker = storage.get('seeker');
 		if(this.state.social){
 			requests.update(this.state.seeker.email, { 
	 			social : this.state.social
	 		}, (ans, error) => {
	 			if(ans){
	 				auth.seeker.login(seeker.email, seeker.password, () => {
	 					yarr(properties.routes.seeker.home);	
	 				});
	 			}
			});
 		} else {
 			auth.seeker.login(seeker.email, seeker.password, () => {
				yarr(properties.routes.seeker.home);
			});
 		}
 	},
	render() {
		return (
			<div className="main-container">
				<Topbar />
				<div id="container" className = 'pure-g'>
					<div className="pure-u-md-1-3 hide-sm hide-xs">
						<div id="stepper" className='margin-top-100px inner-svg-centered'></div>
					</div>
					<div className = 'pure-u-md-1-2 pure-u-sm-3-4 margin-top-40px'>
						<div className="padding-left-5px thin-border-bottom">
							<h3 className="padding-top-10px padding-bottom-10px no-margin">One last step</h3>
							<div className="font-13px opacity-70 margin-bottom-5px padding-left-5px">Show how passionated and involved you are ?</div>
						</div>

						<div className='padding-left-30px padding-right-30px padding-bottom-30px' onKeyUp={this.testEnter}>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels='Cv' />
								<div className='margin-left-5pc width-90pc'>
									<common.DragAndDrop errorMsg={this.state.fileErrorMsg} classProp="file-picker" defaultIcon="svg-degree" handleFiles={this.handleUpload} image={this.state.image}/>
								</div>
							</div>

							<div className='width-100pc padding-bottom-10px'>
								<common.Label labels={'Social medias'} />
								<div className='margin-left-5pc width-90pc'>
									<common.SocialMedias social={this.state.social} updateSocial={this.updateSocial} enterPressed={this.register}/>		
								</div>
							</div>
							<div>
								<common.ValidateButton onClick={this.register} submitableUpdate="true" labels='Finish' classProp="margin-bottom-20px"/>
							</div>
						</div>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Topbar = React.createClass({
 	goToProspect(){
 		yarr(properties.routes.employer.home);
 	},
 	goToSeeker(){
 		yarr(properties.routes.core);
 	},
	render() {
		return (
			<div className = 'pure-g background-white height-60px'>
				<div id="container" className = 'tobar'>
					<div className='float-left margin-top-5px margin-right-5px margin-left-5px'>
						<common.Icon icon='svg-logo' classProp='icon-medium fill-color1'/>
					</div>
					<div className="letter-spacing-1px pointer float-left color1 padding-left-15px padding-right-15px navUnSelectable" onClick={this.goToSeeker}>Narwhal</div>
					<div className="letter-spacing-1px pointer thin-text float-left color1 padding-left-15px padding-right-15px navSelected">Register (Seeker)</div>
					
				</div>
			</div>
		);
	}
});

module.exports = {
	Reg1Form : Reg1Form,
	Reg2Form : Reg2Form,
	Reg3Form : Reg3Form,
	Reg4Form : Reg4Form
};