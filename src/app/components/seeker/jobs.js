let jobs = require('../jobs/jobs.js'),
	common = require('../common.js'),
	seekCommon = require('./common.js');


let Jobs = React.createClass({
	getInitialState() {
	   	return {
	   		employer : {}
	   	};
	},
	componentDidMount(){
		
	},
	render() {
		return (
			<div className="main-container">
				<seekCommon.Topbar forceRoute="job"/>
				<jobs.Jobs jobId={this.props.jobId} edit="false"/>
				<common.Bottombar />
			</div>
		);
	}
});

module.exports = {
	Jobs : Jobs
}