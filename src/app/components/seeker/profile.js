"use strict";

let skprofile = require('../profiles/seeker.js'),
	requests = require('../../services/seeker-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	seekerCommon = require('./common.js');

let Seekprofile = React.createClass({
	getInitialState() {
	   	return {
	   		seeker : storage.get('seeker')
	   	};
	},
	componentDidMount(){
		let seeker = storage.get('seeker');
		requests.login(seeker.email, seeker.password, seeker => {
			seeker.social.forEach(soc => {
				properties.getSocial(soc.id, bundle => {
					soc.name = bundle.name;
				});
			});
			this.setState({seeker : seeker});
		});
	},
	render() {
		return (
			<div>
				<seekerCommon.Topbar />
				<skprofile.Profile seeker={this.state.seeker} edit="true"/>
			</div>
		);
	}
});

module.exports = {
	Seekprofile : Seekprofile
};