"use strict";

let empprofile = require('../profiles/employer.js'),
	requests = require('../../services/seeker-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	seekerCommon = require('./common.js');

let Empprofile = React.createClass({
	getInitialState() {
	   	return {
	   		employer : {}
	   	};
	},
	componentDidMount(){
		let emp = storage.get('employer');
		requests.getEmployerProfile(this.props.empId, emp => {
			emp.social.forEach(soc => {
				properties.getSocial(soc.id, bundle => {
					soc.name = bundle.name;
				});
			});
			this.setState({employer : emp});
		});
	},
	render() {
		return (
			<div>
				<seekerCommon.Topbar forceRoute="seeker/profile"/>
				<empprofile.Profile employer={this.state.employer} edit="false"/>
			</div>
		);
	}
});

module.exports = {
	Empprofile : Empprofile
};