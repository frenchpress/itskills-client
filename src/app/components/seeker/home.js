"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	seekerCommon = require('./common.js'),
	requests = require('../../services/seeker-requests.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js'),
	Rating = require('../../../../node_modules/Rating/js/rating.js');

let HomePage = React.createClass({
	getInitialState() {
		return {
			seeker : storage.get('seeker'),
			jobs : []
		};
 	},
 	componentWillMount(){
 		requests.getJobs(this.state.seeker.email, jobs => {
 			jobs.forEach(job => {
 				properties.getLocation(job.location, x => {
 					job.location = x;
 				});
 				job.types.forEach((type, index) => {
 					properties.getJobType(type, x => {
	 					job.types[index] = x;
	 				});
 				});
 				job.xp.forEach((xp, index) => {
 					properties.getXp(xp, x => {
	 					job.xp[index] = x;
	 				});
 				});
 				properties.getIndusty(job.employer.industry, x => {
 					job.employer.industry = x;
 				});
 				properties.getCompanySize(job.employer.size, x => {
 					job.employer.size = x;
 				});
 				job.isFavoring = job.favored.indexOf(this.state.seeker._id) !== -1;
 				job.match = !!(job.isFavoring && job.isFavored);

 			});
 			this.toggleJobs(jobs);
 		});
 	},
 	toggleJobs(jobs){
 		if(jobs.length){
 			jobs.forEach(job => {
 				job.show = false;
	 			if(this.state.matches){
	 				if(this.state.matches.indexOf(1) !== -1){
	 					if(job.match){
	 						job.show = true;
	 					}
	 				}
	 				if(this.state.matches.indexOf(2) !== -1){
	 					if(job.isFavored){
	 						job.show = true;
	 					}
	 				}
	 				if(this.state.matches.indexOf(3) !== -1){
	 					if(job.isFavoring){
	 						job.show = true;
	 					}
	 				}
	 				if(this.state.matches.indexOf(4) !== -1){
	 					if(!job.isFavoring && !job.isFavored && !job.match){
	 						job.show = true;
	 					}
	 				}

	 			}
	 			if(this.state.salary){
	 				if(job.salaryMax <= this.state.salary.id) job.show = false;
	 			}
 			});
 			this.setState({jobs : jobs});
 		}
 	},
 	favor(job){
 		requests.favor(job._id, x => {
 			let jobs = this.state.jobs;
 			jobs.forEach(y => {
 				if(job._id === y._id){
 					y.isFavored = true;
 					if(y.isFavoring){
 						y.match = true;
 					}
 				}
 			});
 			this.setState({jobs : jobs});
 		});
 	},
 	discard(job){
		requests.discard(job._id, x => {
 			let jobs = this.state.jobs,
 				index = -1;
 			jobs.forEach((y, ind) => {
 				if(job._id === y._id){
 					index = ind;
 				}
 			});
 			if(index !== -1){
 				jobs.splice(index, 1);
 			}
 			this.setState({jobs : jobs});
 		});
 	},
 	unfavor(job){
		requests.unfavor(job._id, x => {
 			let jobs = this.state.jobs;
 			jobs.forEach(y => {
 				if(job._id === y._id){
 					y.isFavored = false;
 					y.match = false;
 				}
 			});
 			this.setState({jobs : jobs});
 		});
 	},
 	salarySort(salary){
 		this.setState({salary : salary}, () => {
 			this.toggleJobs(this.state.jobs);
 		});
 	},
 	matchesSort(matches){
 		this.setState({matches : matches}, () => {
 			this.toggleJobs(this.state.jobs);
 		});
 	},
	render() {
		return (
			<div className="main-container">
				<seekerCommon.Topbar forceRoute="seeker/home"/>
				<div id="container" className='pure-g padding-top-10px'>
					<div className="pure-u-md-1-3">
						<Filters salarySort={this.salarySort} matchesSort={this.matchesSort}/>
					</div>
					<div className="pure-u-md-2-3">
						<JobList jobs={this.state.jobs} favor={this.favor} discard={this.discard} unfavor={this.unfavor}/>
					</div>
				</div>
				<common.Bottombar />
			</div>
		);
	}
});

let Filters = React.createClass({
	getInitialState() {
	   	return {
	   		selection : [
	   			{id: 1, name: 'Matches'},
	   			{id: 2, name: 'Jobs of interest'},
	   			{id: 3, name: 'Companies interested'},
	   			{id: 4, name: 'Without contact history'},
	   		],
	   		defaultSelection : [1, 2, 3, 4],
	   		defaultId : 3
	   	};
	},
	componentWillMount(){
		properties.getSalaries(x => {
			this.setState({salaries : x});
		});
	},
	setSalary(salary){
		this.setState({defaultId: salary.id});
		if(salary.id && this.props.salarySort){
			this.state.salaries.forEach(x => {
				if(x.id === salary.id){
					this.props.salarySort(x);
				}
			});
		}
	},
	setMatches(matches){
		if(this.props.matchesSort && matches){
			let array = _.pluck(matches, 'id');
			this.setState({defaultSelection: array});
			this.props.matchesSort(array);
		}
	},
	render() {
		return (
			<div>
				<h3 className="no-margin-bottom no-margin-left no-margin-right padding-10px thin-border-bottom">Job filters</h3>
				<h4 className="thin-text font-16px padding-left-10px margin-bottom-15px">Salary <span className="opacity-50">NZD</span></h4>
				<p className="font-11px opacity-80 padding-left-10px margin-bottom-15px">Select the lower bound of your salary expectation. It is not always shonw but every job has a salaray range.</p>
				<common.TinyList data={this.state.salaries} defaultId={this.state.defaultId} setValue={this.setSalary} icon="svg-dollar" iconClassProp='icon-mini fill-color1 opacity-80 margin-left-5px'/>
				<h4 className="thin-text font-16px padding-left-10px margin-bottom-15px">Matches</h4>
				<common.MultiChoiceList data={this.state.selection} defaultIds={this.state.defaultSelection} setValue={this.setMatches} icon="svg-handshake" iconClassProp='icon-mini fill-color1 opacity-80 margin-left-5px'/>
			</div>
		);
	}
});

let JobList = React.createClass({
	getInitialState() {
	   	return {
			jobs : this.props.jobs ? this.props.jobs : [],
			page : 1,
			pages : this.props.jobs ? Math.ceil(this.props.jobs.length/4) : 1
	   	};
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.jobs){
			let count = 0;
			nextProp.jobs.forEach(x => {
				if(x.show){
					count++;
				}
			});
			this.setState({jobs : nextProp.jobs, pages : Math.ceil(count/4)});
		}
	},
	componentDidUpdate(){
 		this.state.jobs.map((job, index) => {
 			let ref = React.findDOMNode(this.refs["rating-" + index]);
 			while (ref.firstChild) {
				ref.removeChild(ref.firstChild);
			}
 			new Rating(properties.ratingProps("rating-" + index, Math.round(job.rating)));
 		});
 	},
 	favor(job){
 		if(this.props.favor){
			this.props.favor(job);
 		}
 	},
 	discard(job){
		if(this.props.discard){
			this.props.discard(job);
 		}
 	},
 	unfavor(job){
		if(this.props.unfavor){
			this.props.unfavor(job);
 		}
 	},
 	jobDetails(job){
 		yarr(properties.routes.seeker.jobs.replace(':id', job._id));
 	},
 	employerDetails(emp){
 		yarr(properties.routes.seeker.empprofile.replace(':id', emp._id));
 	},
 	pageBefore(){
 		let page = this.state.page;
 		this.setState({page : page - 1 > 0 ? page - 1 : 1});
 	},
 	pageAfter(){
 		let page = this.state.page;
 		this.setState({page : page + 1 <= this.state.pages ? page + 1 : this.state.pages});
 	},
 	pageGoTo(page){
 		this.setState({page : page});
 	},
	render() {
		let count = 0;
		let jobs = this.state.jobs.map((job, index) => {
			let types = _.pluck(job.types, 'name').join(', '),
				xps = _.pluck(job.xp, 'name').join(', '),
				favorStyle = {display : job.isFavored ? 'none' : 'inline-block'},
				unFavorStyle = {display : job.isFavored ? 'inline-block' : 'none'},
				innFavor = this.favor.bind(this, job),
				innDiscard = this.discard.bind(this, job),
				innUnfavor = this.unfavor.bind(this, job),
				innDetails = this.jobDetails.bind(this, job),
				innDetailsEmp = this.employerDetails.bind(this, job.employer),
				hide = job.show ? '' : 'hide',
				showMatchingDetails = job.match ? '' : 'hide',
				onPage = count < 4*this.state.page && count >= (this.state.page-1)*4 ? '' : 'hide';

				if(job.show){
					count++;
				}
			return (
				<div key={index} className={"thin-border-bottom padding-10px " + hide + " " + onPage}>
					<div className="pure-g">
						<div className="pure-u-md-2-3">
							<div className="font-18px margin-bottom-5px letter-spacing-1px">{job.title}</div>
							<div className="font-13px margin-bottom-5px letter-spacing-1px opacity-50 pointer" onClick={innDetailsEmp}>{job.employer.companyName}</div>

							<div className={"margin-top-10px " + showMatchingDetails}>
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-mail" />
								<span className="font-12px line-height-20px v-align-bottom">{job.employer.email}</span>
							</div>
							<div className={"margin-top-10px " + showMatchingDetails}>
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-number" />
								<span className="font-12px line-height-20px v-align-bottom">{job.employer.phone}</span>
							</div>

							<common.TagsBoxShow tags={job.tags} />

							<div className="margin-top-10px">
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-address" />
								<span className="font-12px line-height-20px v-align-bottom">{job.location.name}</span>
							</div>
							<div className="margin-top-10px">
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-calendar" />
								<span className="font-12px line-height-20px v-align-bottom">{types}</span>
							</div>
							<div className="margin-top-10px">
								<common.Icon classProp="icon-mini v-align-top fill-color1 margin-right-5px" icon="svg-degree" />
								<span className="font-12px line-height-20px v-align-bottom">{xps}</span>
							</div>
						</div>
						<div className="pure-u-md-1-3">
							<div id={"rating-" + index} ref={"rating-" + index} className="float-right margin-top-10px margin-right-10px"></div>
							<MatchBlock job={job}/>
						</div>
					</div>
					<div>
						<div className="height-35px margin-top-10px">
							<common.MiniIconButton icon="svg-details" labels="Details" className="background-color1 inline-block" colorIcon="fill-white" onClick={innDetails} />
							<common.MiniIconButton icon="svg-close" labels="Discard" className="background-color1 inline-block float-right margin-right-10px" colorIcon="fill-white" onClick={innDiscard}/>
							<div style={favorStyle} className="float-right">
								<common.MiniIconButton icon="svg-check" labels="Interested" className="background-color1 inline-block margin-right-5px" colorIcon="fill-white" onClick={innFavor}/>
							</div>
							<div style={unFavorStyle} className="float-right">
								<common.MiniIconButton icon="svg-close" labels="Uninterested" className="background-color5 inline-block margin-right-5px" colorIcon="fill-white" onClick={innUnfavor}/>
							</div>
						</div>
					</div>
				</div>
			)
		});
		let jobBUClass = '';
		this.state.jobs.forEach(job => {
			if(job.show){
				jobBUClass = 'hide';
			}
		});
		return (
			<div>
				<h3 className="no-margin-bottom no-margin-left no-margin-right padding-10px thin-border-bottom">Matching jobs</h3>
				{jobs}
				<div className="margin-top-10px">
					<common.Paginator page={this.state.page} pages={this.state.pages} before={this.pageBefore} after={this.pageAfter} goTo={this.pageGoTo}/>
				</div>
				<div className={"text-centered margin-top-20px font-14px " + jobBUClass}>Sorry, we didn't find any job fitting these criteria</div>
			</div>
		);
	}
});

let MatchBlock = React.createClass({
	render() {
		let showMatches = {display : this.props.job && this.props.job.match ? 'block' : 'none'},
			showLikes = {display : this.props.job && this.props.job.isFavored && !(this.props.job && this.props.job.match) ? 'block' : 'none'},
			showLikedBy = {display : this.props.job && this.props.job.isFavoring && !(this.props.job && this.props.job.match) ? 'block' : 'none'};
		return (
			<div className={this.props.className + ' thin-text'}>
				<div style={showMatches}>
					<common.Icon icon='svg-handshake' classProp="icon-mini fill-color1 margin-right-5px"/>
					<span className="v-align-top">This is a match !</span>
				</div>
				<div style={showLikes}>
					<common.Icon icon='svg-arrow-r-3' classProp="icon-mini fill-color1 margin-right-5px padding-top-1px"/>
					<span className="v-align-top">You're interested</span>
				</div>
				<div style={showLikedBy}>
					<common.Icon icon='svg-arrow-r-3' classProp="icon-mini fill-color1 margin-right-5px padding-top-1px rotate-180"/>
					<span className="v-align-top">Interested !</span>
				</div>
			</div>
		);
	}
});

module.exports = {
	HomePage : HomePage
};