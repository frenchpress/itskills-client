"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	properties = require('../../services/properties.js'),
	storage = require('../../services/storage.js');

let Topbar = React.createClass({
	getInitialState() {
		let seeker = storage.get('seeker'); 
		return {
			seeker : seeker ? seeker : {}
		};
 	},
 	settings(){
 		console.log('Settings');
 	},
 	handshake(){
 		console.log('Handshake');
 	},
 	goToJobs(){
 		yarr(properties.routes.seeker.home);
 	},
 	isWithinCurrentRoute(label){
 		if(this.props.forceRoute){
 			return this.props.forceRoute === label;
 		} else {
 			return window.location ? window.location.pathname.indexOf(label) !== -1 : false;
 		}
 	},
 	goProfil(){
 		yarr(properties.routes.seeker.profile);
 	},
	render() {

		let jobSel = this.isWithinCurrentRoute('seeker/home') ? 'navSelected' : 'navUnSelected',
			profileSel = this.isWithinCurrentRoute('seeker/profile') ? 'iconSelected' : '',
			profileSelNav = this.isWithinCurrentRoute('seeker/profile') ? 'navSelected' : 'navUnSelected';
		
		return (
			<div className = 'pure-g background-white height-60px'>
				<div id="container" className = 'tobar'>
					<div className='pointer float-left margin-top-5px margin-right-5px margin-left-5px'>
						<common.Icon icon='svg-logo' classProp='icon-medium fill-color1' click={this.goToJobs} />
					</div>
					
					<div className={"letter-spacing-1px pointer thin-text float-left color1 padding-left-10px padding-right-10px " + jobSel} onClick={this.goToJobs}>Jobs</div>

					<div className={"float-right letter-spacing-1px pointer color1 padding-left-10px padding-right-10px " + profileSelNav} onClick={this.goProfil}>{this.state.seeker.name}</div>
					
					<div onClick={this.goProfil} className={profileSel + ' pointer float-right margin-top-20px margin-right-5px padding-top-3px padding-left-5px padding-right-5px radius-5px background-greycolor hover-color-color1 transition-3'}>
						<common.Icon icon='svg-anonymous' classProp='icon-tiny fill-white'/>
					</div>
					
					<div onClick={this.settings} className='pointer float-right margin-top-20px margin-right-5px padding-top-3px padding-left-5px padding-right-5px radius-5px background-greycolor hover-color-color1 transition-3'>
						<common.Icon icon='svg-settings1' classProp='icon-tiny fill-white'/>
					</div>

					<div onClick={this.handshake} className='pointer float-right margin-top-20px margin-right-5px padding-top-3px padding-left-5px padding-right-5px radius-5px background-greycolor hover-color-color1 transition-3'>
						<common.Icon icon='svg-handshake' classProp='icon-tiny fill-white'/>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = {
	Topbar : Topbar
};