"use strict";

let register = require('./register.js'),
	home = require('./home.js'),
	profile = require('./profile.js'),
	empprofile = require('./empprofile.js'),
	jobs = require('./jobs.js');

let Register1 = React.createClass({
	render() {
		return (
			<register.Reg1Form />
		);
	}
});

let Register2 = React.createClass({
	render() {
		return (
			<register.Reg2Form />
		);
	}
});

let Register3 = React.createClass({
	render() {
		return (
			<register.Reg3Form />
		);
	}
});

let Register4 = React.createClass({
	render() {
		return (
			<register.Reg4Form />
		);
	}
});

let Home = React.createClass({
	render() {
		return (
			<home.HomePage />
		);
	}
});

let Empprofile = React.createClass({
	render() {
		return (
			<empprofile.Empprofile empId={this.props.empId}/>
		);
	}
});

let Profile = React.createClass({
	render() {
		return (
			<profile.Seekprofile/>
		);
	}
});

let Jobs = React.createClass({
	render() {
		return (
			<jobs.Jobs jobId={this.props.jobId}/>
		);
	}
});

module.exports = {
	Register1 : Register1,
	Register2 : Register2,
	Register3 : Register3,
	Register4 : Register4,
	Home : Home,
	Profile : Profile,
	Empprofile : Empprofile,
	Jobs : Jobs
};