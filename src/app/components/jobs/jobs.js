"use strict";

let yarr = require('../../../../node_modules/yarr.js/yarr.js'),
	common = require('../common.js'),
	requests = require('../../services/jobs-requests.js'),
	tagsRequests = require('../../services/tag-requests.js'),
	storage = require('../../services/storage.js'),
	properties = require('../../services/properties.js');

let Jobs = React.createClass({
	getInitialState() {
		return {
			job : {},
			canModify : this.props.edit === "true",
			companyOpen : false,
		};
 	},
 	componentWillMount() {
 		requests.getJobDetail(this.props.jobId, (ans) => {
 			if(ans){
 				this.setState({job : ans})
 			}
 		});
 	},
 	openCompany(){
 		this.setState({companyOpen : !this.state.companyOpen});
 	},
	render() {
		let companyName = this.state.job.emp ? this.state.job.emp.companyName : '',
			jobName = this.state.job ? this.state.job.title : '',
			showCompany = this.state.canModify ? 'hide' : '',
			companyOpenClass = this.state.companyOpen ? '' : 'hide',
			flatArrowClass = this.state.companyOpen ? 'hide' : '';
		return (
			<div>
				<div id="container">
					<div className = {'pure-g ' + showCompany}>
						<div className="padding-20px">
							<div>
								<h3 className="padding-left-10px padding-top-10px padding-bottom-10px no-margin thin-border-bottom pointer background-color3" onClick={this.openCompany}>
									<common.Icon icon='svg-arrow-r-2' classProp={'icon-tiny fill-color1 v-align-top margin-top-5px margin-right-5px ' + flatArrowClass}/>
									<common.Icon icon='svg-arrow-d' classProp={'icon-tiny fill-color1 v-align-top margin-top-5px margin-right-5px ' + companyOpenClass}/>
									{companyName}
								</h3>
								<div className={companyOpenClass}>
									<Company data={this.state.job.emp} />
								</div>
							</div>
						</div>
					</div>
					<div className = 'pure-g'>
						<div className="padding-20px">
							<div>
								<h3 className="padding-left-30px padding-top-10px padding-bottom-10px no-margin thin-border-bottom background-color3">
									Job details
								</h3>
								<Job id={this.props.jobId} job={this.state.job} canModify={this.state.canModify} employer={this.props.employer}/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

let Company = React.createClass({
	getInitialState() {
		return {
			employer : {}
		};
 	},
 	componentWillReceiveProps(nextProp){
 		if(nextProp.data){
 			this.setState({employer : nextProp.data});	
 		}
 	},
	render() {
		return (
			<div className='pure-g padding-left-40px padding-right-40px'>
				<div>
					<div className="inline-block v-align-top width-100px padding-top-10px">
						<Logo logo={this.state.employer.avatar} />
					</div>
					<div className="inline-block v-align-top padding-left-10px">
						<DescriptionCompany employer={this.state.employer} />
					</div>
				</div>
				<Sociaux social={this.state.employer.social} website={this.state.employer.website} /> 
				<hr className="opacity-30"/>
				<h3>Job description</h3> 
				<DetailsCompany description={this.state.employer.description} /> 
			</div>
		);
	}
});

let Logo = React.createClass({
	getInitialState() {
		return {};	
 	},
 	componentWillReceiveProps(nextProp){
		this.setState({logo : nextProp.logo});
 	},
	render() {
		if(this.state.logo){
			let res = this.state.logo.split("/"),
				url = res[res.length -1];
			return (
				<div>
					<img src={properties.baseUrl + 'employers-avatars/' + url} alt="Company logo" className='width-100px height-100px'></img>
				</div>
			);
		} else {
			return (
				<div></div>
			);
		}
	}
});

let DescriptionCompany = React.createClass({
	getInitialState() {
		return {
			employer : {},
			size : {},
			industry : {}
		};
 	},
 	componentWillReceiveProps(nextProp){
 		if(nextProp.employer){
 			this.setState({employer : nextProp.employer});	
			properties.getCompanySize(nextProp.employer.size, x => {
	 			this.setState({size : x});
	 		});
	 		properties.getIndusty(nextProp.employer.industry, x => {
	 			this.setState({industry : x});
	 		});	
 		}
 	},
	render() {
		return (
			<div className="margin-top-20px">
				<div className="margin-top-5px">
					<common.Icon icon='svg-crowd' classProp='icon-mini fill-color1 v-align-top'/>
	      			<div className='margin-left-10px inline-block font-12px v-align-top padding-top-2px'>{this.state.size.name}</div>
				</div>
				<div className="margin-top-5px">
					<common.Icon icon='svg-industry' classProp='icon-mini fill-color1 v-align-top'/>
	      			<div className='margin-left-10px inline-block font-12px v-align-top padding-top-2px'>{this.state.industry.name}</div>
				</div>
				<div className="margin-top-5px">
					<common.Icon icon='svg-web' classProp='icon-mini fill-color1 v-align-top'/>
					<a href={this.state.employer.website} target="_blank">
						<div className='margin-left-10px inline-block font-12px v-align-top padding-top-2px'>{this.state.employer.website}</div>
	      			</a>
				</div>
			</div>
		);
	}
});

let Sociaux = React.createClass({
	getInitialState() {
		return {};
 	},
 	componentWillMount(){
 		properties.getSocials(x => {
 			if(this.state.social){
 				this.state.social.forEach(soc => {
 					if(x.id === soc.id){
 						x.url = soc.url;
 						x.selected = soc.selected;
 					}
 				});
 			}
 			this.setState({socials : x});
 		});
 	},
	componentWillReceiveProps(nextProp){
		let socArr = [];
		if(nextProp.social){
			if(this.state.socials){
				this.state.socials.forEach(x => {
					let found = false;
					nextProp.social.forEach(soc => {
						if(x.id === soc.id){
	 						x.url = soc.url;
	 						x.selected = soc.selected;
							socArr.push(x);
							found = true;
	 					}
					});
					if(!found){
						socArr.push(x);
					}
				});
			}
			this.setState({socials : socArr});	
		}
	},
	render() {
		let showStyle = {
			display : this.state.socials ? 'block' : 'inline'
		};
		return (
			<div style={showStyle}>
				<common.Label labels='Social medias' />
				<div>
					<common.SocialMediasDisplay social={this.state.socials}/>
				</div>
			</div>
		);
	}
});

let DetailsCompany = React.createClass({
	getInitialState() {
		return {

		};
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({description : nextProp.description})
 	},
	render() {
		return (
 			<p className='font-12px opacity-70'>{this.state.description}</p>
		);
	}
});

let Job = React.createClass({
	getInitialState() {
		return {
			job : {},
			editView : false,
			confirmPanel : false
		};
 	},
 	componentWillReceiveProps(nextProp){
 		if(nextProp.job){
 			this.setState({
 				job : nextProp.job,
 				choices : [{
					icon : 'svg-visible',
					label : 'Visible',
					selected : nextProp.job.visible
				}, {
					icon : 'svg-invisible',
					label : 'Invisible',
					selected : !nextProp.job.visible
				}]
 			});
 		}
 	},
 	changeVisibility() {
 		this.setState({editView : true});
 	},
 	updateWorkType(types){
		let job = this.state.job;
		job.types =  _.pluck(types, 'id');
		this.setState({job : job} , () => {
			this.save();
		});
 	},
 	updateSalaryMin(min){
 		if(min){
 			let job = this.state.job;
 			job.salaryMin = min;
 			this.setState({job : job}, () => {
 				this.save();
 			});
 		}
 	},
 	updateSalaryMax(max){
 		if(max){
 			let job = this.state.job;
 			job.salaryMax = max;
 			this.setState({job : job}, () => {
 				this.save();
 			});
 		}
 	},
 	updateProfile(profile) {
 		let job = this.state.job;
 		job.xp = _.pluck(profile, 'id');
 		this.setState({job : job}, () => {
			this.save();
		});
 	},
 	updateLocation(location) {
 		let job = this.state.job;
 		job.location = location;
 		this.setState({job : job}, () => {
			this.save();
		});
 	},
 	save() {
 		requests.update(this.props.id, this.state.job, (ans, res) => {
 			if(ans){
 				this.setJob(res);
 			}
 		});
 	},
 	setJob(job){
 		this.setState({
 			job : job,
 			choices : [{
				icon : 'svg-visible',
				label : 'Visible',
				selected : job.visible
			}, {
				icon : 'svg-invisible',
				label : 'Invisible',
				selected : !job.visible
			}]
 		});
 	},
 	closeEdition(){
 		this.setState({editView: false});
 		this.save();
 	},
 	toggleActive(){
 		let job = this.state.job;
 		job.active = !job.active;
 		this.setState({confirmPanel : false, job : job}, () => {
 			this.save();
 		});
 	},
 	toggleVisibility(){
 		let job = this.state.job;
 		job.visible = !job.visible;
 		this.setState({job : job}, () => {
 			this.save();
 		});
 	},
 	toggleConfirmationPanel(){
 		this.setState({confirmPanel : !this.state.confirmPanel});
 	},
 	goToSettings(){
 		yarr(properties.routes.employer.settings);
 	},
	render() {
		let editRightStyle = {
			display : this.props.canModify && !this.state.editView? 'block' : 'none'
		}
		let editStyle = {
			display : this.state.editView ? 'block' : 'none' 
		};
		let transitionClasses = this.props.canModify && !this.state.editView ? 'hover-color-white' : "",
			modifyClass = this.props.canModify && this.state.editView ? 'background-white' : "",
			iconColor = this.state.job.visible ? 'fill-color1' : 'fill-color5',
			overIconColor = this.state.job.visible ? 'fill-color5' : 'fill-color1',
			labelColor = this.state.job.visible ? 'color1' : 'color5',
			overLabelColor = this.state.job.visible? 'color5' : 'color1',

			activationLeft = this.props.employer ? this.props.employer.monthlyLeft : 0,
			resetDate = this.props.employer ? new Date(this.props.employer.nextReset) : 0,
			displayableResetDate = moment(resetDate).format('MMMM Do YYYY'),
			activationButtonClass = activationLeft > 0 ? '' : 'hide',
			activationFallbackClass = activationLeft > 0 ? 'hide' : '',

			displayConfPanel = this.state.confirmPanel ? '' : 'hide',
			displayToggle = !this.state.confirmPanel && this.state.job.complete && this.props.canModify && this.state.job.active ? '' : 'hide',
			displayIncomplete = this.state.job.complete || this.state.confirmPanel ? 'hide' : '',
			displayInactive = !this.state.confirmPanel && this.state.job.complete && !this.state.job.active && this.props.canModify ? '' : 'hide';

		return (
			<div className = 'padding-20px'>
				<Title id={this.props.id} save={this.save} title={this.state.job.title} editRight={this.props.canModify} setJob={this.setJob}/> 

				<div className={displayConfPanel + " white-color background-color1 radius-5px padding-20px"}>

					<div className="font-18px margin-bottom-5px">
						Job activation
					</div>
					<div className="font-13px opacity-80 margin-bottom-5px">
						<div className="margin-top-10px">Setting this job to active will start his lifecycle and will remain active for 30 days. Remember that a job can be active and unvisible, toggle visibility on if you wish your job to be seen.</div>
						<div className="margin-top-10px">Your currently have {activationLeft} job activations left this month</div>
						<div className="margin-top-10px">The activation allowance will be reset on {displayableResetDate}</div>
					</div>
					<div className={activationButtonClass}>
						<common.MiniIconButton icon="svg-check" labels="Activate" textColorlass="color1" className="background-white inline-block margin-right-5px" colorIcon="fill-color1" onClick={this.toggleActive}/>
					</div>
					<div className={activationFallbackClass}>
						<common.MiniIconButton icon="svg-arrow-u" labels="More activations" textColorlass="color1" className="background-white inline-block margin-right-5px" colorIcon="fill-color1" onClick={this.goToSettings}/>
					</div>
				</div>

				<div className={displayToggle}>
					<div className="width-80px margin-left-10px margin-bottom-10px">
						<common.Toggle choices={this.state.choices} toggle={this.toggleVisibility} iconColor={iconColor} overIconColor={overIconColor} iconClasses='icon-mini ' labelColor={labelColor} overLabelColor={overLabelColor} labelClasses='font-12px margin-top-1px margin-right-5px'/>
					</div>
				</div>

				<div className={"margin-left-12px margin-top-5px margin-bottom-5px pointer " + displayInactive} onClick={this.toggleConfirmationPanel}>
					<common.Icon icon="svg-close" classProp="icon-mini fill-color5 hover-fill-color5"/>
					<div className="inline-block color5 v-align-top font-14px margin-left-5px">Inactive job</div>
				</div>

				<div className={"margin-left-12px margin-top-5px margin-bottom-5px " + displayIncomplete}>
					<common.Icon icon="svg-warning" classProp="icon-mini fill-color5 hover-fill-color5"/>
					<div className="inline-block color5 v-align-top font-14px margin-left-5px">Incomplete job</div>
				</div>

				<hr className="margin-left-10px margin-right-10px opacity-30"/>

				<SkillsTags id={this.props.id} save={this.save} mail={this.state.job.mail} tags={this.state.job.tags} editRight={this.props.canModify} setJob={this.setJob}/>
				
				<div className = {"center-hover-show padding-10px radius-5px transition-all-ease-3 " + transitionClasses + ' ' + modifyClass}>
					<div className = 'pure-g'>
						<div className="pure-u-md-1-2">
							<WorkType visible={this.state.editView} types={this.state.job.types} updateWorkType={this.updateWorkType} setJob={this.setJob}/>
						</div>
						<div className="pure-u-md-3-8">
							<Salary visible={this.state.editView} salaryMin={this.state.job.salaryMin} salaryMax={this.state.job.salaryMax} updateSalaryMin={this.updateSalaryMin} updateSalaryMax={this.updateSalaryMax} setJob={this.setJob}/>
						</div>
						<div className="pure-u-md-1-8">
							<div style={editRightStyle} className="float-right margin-right-5px transition-all-ease-3 opacity-0 hover-opacity-100">
								<common.MiniIconButton onClick={this.changeVisibility} icon="svg-edit" labels="Edit" className="background-color1 inline-block" colorIcon="fill-white" />
							</div>
						</div>
					</div>
					<div className = 'pure-g margin-top-10px'>
						<div className="pure-u-md-1-2">
							<Profile visible={this.state.editView} xp={this.state.job.xp} updateProfile={this.updateProfile}/> 
						</div>
						<div className="pure-u-md-1-2">
							<Address visible={this.state.editView} location={this.state.job.location} updateLocation={this.updateLocation}/>
						</div>
					</div>
				</div>
				<div style={editStyle} className={'pure-g padding-10px ' + modifyClass}>
					<div className="pure-u-md-1-2">
						<common.MiniIconButton onClick={this.closeEdition} icon="svg-check" labels="Save" className="background-color1 inline-block margin-right-10px" colorIcon="fill-white" />
					</div>
				</div>

				<hr className="margin-left-10px margin-right-10px opacity-30"/>

				<DescriptionJob id={this.props.id} description={this.state.job.description} save={this.save} editRight={this.props.canModify} setJob={this.setJob}/> 

				<hr className="margin-left-10px margin-right-10px opacity-30"/>

				<RoleJob id={this.props.id}  roles={this.state.job.roles} save={this.save} editRight={this.props.canModify} setJob={this.setJob}/> 

				<hr className="margin-left-10px margin-right-10px opacity-30"/>
				
				<WhyUs id={this.props.id} whyUs={this.state.job.whyUs} save={this.save} editRight={this.props.canModify} setJob={this.setJob}/>
			</div>
		);
	}
});

let Title = React.createClass({
	getInitialState() {
		return {
			editView : false,
			titleValue : this.props.title ? this.props.title : ''
		};
 	},
 	changeVisibility() {
 		this.setState({editView : !this.state.editView});
 	},
 	titleUpdate(exists, value){
 		if(exists)
 			this.setState({titleValue : value});
 	},
 	saveTitle: function(){
 		requests.update(this.props.id, {title : this.state.titleValue}, (ans, res) => {
 			if(ans){
 				this.setState({editView : !this.state.editView});
 				if(this.props.setJob){
 					this.props.setJob(res);
 				}
 			}
 		});
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({titleValue : nextProp.title, editRight : nextProp.editRight});
 	},
	render() {
		let editButton = this.props.editRight ? '' : "hide",
			transitionClasses = this.props.editRight ? 'hover-color-white' : "",
			showDisplay = this.state.editView ? 'hide' : '',
			showEdit = this.state.editView ? '' : 'hide';
		return (
			<div className={"pure-g center-hover-show margin-bottom-5px radius-5px transition-all-ease-3 " + transitionClasses}>
				<div className={"pure-u-md-1-2 " + showDisplay}>
					<TitleShow titleValue={this.state.titleValue}/> 
				</div>
				<div className={"pure-u-md-1-2 " + showDisplay}>
					<div className="float-right margin-right-5px padding-5px transition-all-ease-3 opacity-0 hover-opacity-100">
						<common.MiniIconButton onClick={this.changeVisibility} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
					</div>
				</div>
				<div className={showEdit}>
					<TitleEdit titleUpdate={this.titleUpdate} onClick={this.saveTitle} titleValue={this.state.titleValue}/>
				</div>
			</div>
		);
	}
});

let TitleShow = React.createClass({
	render() {
		return (
			<div className="padding-10px">
				<h2 className="margin-top-10px margin-bottom-10px">{this.props.titleValue}</h2>
			</div>		
		);
	}
});

let TitleEdit = React.createClass({
	render() {
		return (
			<div className="padding-10px background-white">
				<div>
					<common.GenericInput compulsory='false' exists={this.props.titleUpdate} defaultValue={this.props.titleValue} placeholder='Job Title' classProp='width-100pc margin-bottom-10px v-align-top'/>
				</div>
				<common.MiniIconButton onClick={this.props.onClick} icon="svg-check" labels="Save" className="background-color1 margin-right-10px" colorIcon="fill-white" />
			</div>
		);
	}
});

let SkillsTags = React.createClass({
	getInitialState() {
		return {
			editView : false
		};
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({tags : nextProp.tags});
 	},
 	setVisibility(){
 		this.setState({editView : !this.state.editView});
 	},
	updateTags(tags){
		let newSelected = [];
 		tags.forEach((x) => {
			newSelected.push(x.label);
		});
		this.setState({tags : newSelected});
	},
 	saveTags(){
 		requests.update(this.props.id, {tags : this.state.tags}, (ans, res) => {
 			if(ans){	
 				this.setState({editView : !this.state.editView});
 				if(this.props.setJob){
 					this.props.setJob(res);
 				}
 			}
 		});
 	},
	render() {
		return (
			<div>
				<SkillsTagsDisplay visible={this.state.editView} tags={this.state.tags} onClick={this.setVisibility} editRight={this.props.editRight}/>
				<SkillsTagsEdit visible={this.state.editView} selected={this.state.tags} updateTags={this.updateTags}  onClick={this.saveTags} />
			</div>
		);
	}
});

let SkillsTagsDisplay = React.createClass({
	getInitialState() {
		return {
			tags : []
		};
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({tags : nextProp.tags});
 	},
	render() {
		let givenStyle = {
      		display : this.props.visible ? 'none' : 'block'
    	};
		let editButton = this.props.editRight ? '' : "hide",
			transitionClasses = this.props.editRight ? 'hover-color-white' : "",
			showTags = this.state.tags && this.state.tags.length ? '' : "hide",
			showBackup = this.state.tags && this.state.tags.length ? 'hide' : "";

		return (
			<div style={givenStyle} className={"center-hover-show padding-10px radius-5px transition-all-ease-3 " + transitionClasses}>
				<div className="pure-g">
					<div className="pure-u-md-1-2">
						<div className={showTags}>
							<common.TagsBoxShow tags={this.state.tags} />
						</div>
						<div className={'margin-top-5px width-80pc ' + showBackup}>
							<common.Icon icon='svg-tag' classProp='icon-mini fill-color1 v-align-top'/>
         					<div className='margin-left-20px inline-block opacity-70 font-12px'> No tags selected </div>
						</div>
					</div>
					<div className="pure-u-md-1-2">
						<div className="float-right margin-right-5px transition-all-ease-3 opacity-0 hover-opacity-100">
							<common.MiniIconButton onClick={this.props.onClick} icon="svg-edit" labels="Edit" className={"background-color1 inline-block " + editButton} colorIcon="fill-white" />
						</div>
					</div>
				</div>
			</div>
		);
	}
});

let SkillsTagsEdit = React.createClass({
	getInitialState() {
		return {};
	},
	componentWillReceiveProps(nextProp){
 		this.setState({selected : nextProp.selected});
 	},
	componentDidMount: function() {
		tagsRequests.getTags(this.state.email, tags => {
			this.setState({tags : tags});
		});
	},
	render() {
		let givenStyle = {
      		display : this.props.visible ? 'block' : 'none'
    	};
		return (
			<div style={givenStyle} className="padding-10px background-white">
				<common.TagsBox allTags={this.state.tags} preSelected={this.props.selected}  limit="8" oolMessage="You can only pick 8 skills, choose skills you really want to work with !" updateTags={this.props.updateTags}/>
				<common.MiniIconButton onClick={this.props.onClick} icon="svg-check" labels="Save" className="background-color1 inline-block margin-right-10px" colorIcon="fill-white" />
			</div>
		);
	}
});

let WorkType = React.createClass({
	getInitialState() {
		return {};
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({types : nextProp.types});
 	},
	render() {
		return (
			<div> 
				<WorkTypeDisplay visible={this.props.visible} types={this.state.types}  />
				<WorkTypeEdit visible={this.props.visible} types={this.state.types} updateWorkType={this.props.updateWorkType} />
			</div>
		);
	}
});

let WorkTypeDisplay = React.createClass({
  getInitialState() {
  	return {
  		types : []
  	};
  },
  componentWillReceiveProps(nextProp){
 		properties.getJobTypesSelected(nextProp.types, x =>{
 			this.setState({types : x });
 		});
 	},
  render() {
    let typesDisplay = this.state.types.map((type, index) => {
      return <div key={index}> {type.name} </div>
    });
    let givenStyle = {
  		display : this.props.visible ? 'none' : 'inline'
	};
	let showTypes = this.state.types && this.state.types.length ? '' : 'hide',
		showBackUp = this.state.types && this.state.types.length ? 'hide' : '';
    return (
      <div style={givenStyle}>
	      <common.Icon icon='svg-contrat' classProp='icon-mini fill-color1 v-align-top'/>
	      <div className={'font-12px margin-left-20px inline-block ' + showTypes}>{typesDisplay}</div>
	      <div className={'font-12px margin-left-20px inline-block opacity-70 ' + showBackUp}>No type selected</div>
      </div>
    );
  }
});

let WorkTypeEdit = React.createClass({
  getInitialState() {
  	return {
  		types : []
  	};
  },
  componentWillReceiveProps(nextProp){
 		this.setState({types : nextProp.types});
  },
  componentWillMount(){
 		properties.getJobTypes(x => {
 			this.setState({jobTypes : x});
 		});
  },
  render() {
  	let givenStyle = {
      		display : this.props.visible ? 'inline' : 'none'
    	};
    return (
      <div style={givenStyle}>
	      <common.MultiChoiceList defaultIds={this.state.types} setValue={this.props.updateWorkType} data={this.state.jobTypes} icon='svg-contrat' iconClassProp="icon-mini fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' />
      </div>
    );
  }
});

let Salary = React.createClass({
	getInitialState() {
		return {};
 	},
 	componentWillReceiveProps(nextProp){
 		if(nextProp.salaryMax && nextProp.salaryMin){
			properties.getSalary(nextProp.salaryMin, ind => {
				this.setState({
					min : ind
				});	
			});
			properties.getSalary(nextProp.salaryMax, ind => {
				this.setState({
					max : ind
				});	
			});
		}
 	},
	render() {
		return (
			<div>
				<SalaryDisplay visible={this.props.visible} min={this.state.min} max={this.state.max} />
				<SalaryEdit visible={this.props.visible} min={this.state.min} max={this.state.max} updateSalaryMin={this.props.updateSalaryMin} updateSalaryMax={this.props.updateSalaryMax}/>
			</div>
		);
	}
});

let SalaryDisplay = React.createClass({
 	render() {
 		let givenStyle = {
  			display : this.props.visible ? 'none' : 'block'
		};
		let min = this.props.min ? this.props.min.name : '',
			max = this.props.max ? this.props.max.name : '';
		let showSpan = this.props.max && this.props.min ? '' : 'hide',
			showBackup = this.props.max && this.props.min ? 'hide' : '';
 		return (
 			<div style={givenStyle} >  
 				<common.Icon icon='svg-dollar' classProp='icon-mini fill-color1 v-align-top'/>
         		<div className={'font-12px margin-left-20px inline-block ' + showSpan}>{min} to {max}</div>
         		<div className={'font-12px margin-left-20px inline-block opacity-70 ' + showBackup}>No salary range selected</div>
 			</div>
 		);
 	}
});

let SalaryEdit = React.createClass({
	getInitialState() {
		return {
			min : {},
			max : {}
		};
	},
	componentDidMount(){
		properties.getSalaries(x => {
 			this.setState({salaries : x});
 		});
	},
	componentWillReceiveProps(nextProp){
		if(nextProp.min && nextProp.max){
			this.setState({
				min : nextProp.min, 
				max : nextProp.max
			});
		}
 	},
 	render() {
 		let givenStyle = {
  			display : this.props.visible ? 'block' : 'none'
		};
 		return (
 			<div style={givenStyle}>  
	 			<div className="pure-g">
	 				<div className="pure-u-md-1-2">
	 					<h3 className="margin-left-10px">Salary min</h3>
	 					<common.CustomSelect data={this.state.salaries} setValue={this.props.updateSalaryMin} icon='svg-dollar' iconClassProp='icon-mini fill-color1'/>
	 				</div>
	 				<div className="pure-u-md-1-2">
	 					<h3 className="margin-left-10px">Salary max</h3>
	 					<common.CustomSelect data={this.state.salaries} setValue={this.props.updateSalaryMax} icon='svg-dollar' iconClassProp='icon-mini fill-color1'/>
	 				</div>
	 			</div>
 				
 			</div>
 		);
 	}
});

let Profile = React.createClass({
	getInitialState() {
		return {

		};
 	},
 	componentWillReceiveProps(nextProp){
 		properties.getXpsSelected(nextProp.xp, x =>{
 			this.setState({xp : x});
 		});
 		
 	},
	render() {
		return (
			<div>
				<ProfileDisplay visible={this.props.visible} xp={this.state.xp} />
				<ProfileEdit visible={this.props.visible} xp={this.state.xp} updateProfile={this.props.updateProfile} />
			</div>
		);
	}
});

let ProfileDisplay = React.createClass({
	getInitialState() {
		return {
			xp : []
		};
	},
	componentWillReceiveProps(nextProp){
		this.setState({xp : nextProp.xp});
	},
	render() {
		let xpSelected = this.state.xp.map((exp, index) => {
      		return <div key={index}>{exp.name}</div>
      	});
      	let givenStyle = {
  			display : this.props.visible ? 'none' : 'block'
		},
			showProfile = xpSelected && xpSelected.length ? '' : 'hide',
    		showBackup = xpSelected && xpSelected.length ? 'hide' : '';
		return (
			<div style={givenStyle}> 
				<common.Icon icon='svg-suitcase' classProp='icon-mini fill-color1 v-align-top'/>
				<div className={'font-12px margin-left-20px inline-block ' + showProfile}>{xpSelected} </div>
				<div className={'font-12px margin-left-20px inline-block opacity-70 ' + showBackup}>No requirement selected</div>
			</div>
		);
	}
});

let ProfileEdit = React.createClass({
	getInitialState() {
		return {
			xp : []
		};
	},
	componentWillReceiveProps(nextProp){
		this.setState({xp : nextProp.xp, defaultIds : _.pluck(nextProp.xp, 'id')});
	},
	componentWillMount(){
 		properties.getXps(x => {
 			this.setState({xps : x});
 		});
 	},
	render() {
      	let givenStyle = {
      			display : this.props.visible ? 'block' : 'none'
    		};
		return (
			<div style={givenStyle}> 
				<common.MultiChoiceList defaultIds={this.state.defaultIds} setValue={this.props.updateProfile} data={this.state.xps} icon='svg-suitcase' iconClassProp="icon-mini fill-color1" classProp='width-90pc margin-bottom-10px v-align-top' />
			</div>
		);
	}
});

let Address = React.createClass({
	getInitialState() {
		return {

		};
 	},
 	componentWillReceiveProps(nextProp){
 		properties.getLocation(nextProp.location, x =>{
 			this.setState({location : x});
 		});
 	},
	render() {
		return (
			<div className='inline-block'> 
				<AddressDisplay visible={this.props.visible} location={this.state.location} />
				<AddressEdit visible={this.props.visible} location={this.state.location} updateLocation={this.props.updateLocation} />
			</div>
		);
	}
});

let AddressDisplay = React.createClass({
	getInitialState() {
		return {
			location : {}
		};
	},
	componentWillReceiveProps(nextProp){
 		this.setState({location : nextProp.location});
 	},
 	render() {
 		let givenStyle = {
      			display : this.props.visible ? 'none' : 'block'
    		},
    		locationName = this.state.location ? this.state.location.name : '',
    		showLocation = this.state.location ? '' : 'hide',
    		showBackup = this.state.location ? 'hide' : '';

 		return (
 			<div style={givenStyle}>
 				<common.Icon icon='svg-pin' classProp='icon-mini fill-color1 v-align-top'/>
         		<div className={'font-12px margin-left-20px inline-block ' + showLocation}>{locationName}</div>
         		<div className={'font-12px margin-left-20px inline-block opacity-70 ' + showBackup}>No address selected</div>
         	</div>
 			);
 	}
});

let AddressEdit = React.createClass({
	getInitialState() {
		return {
			location : {}
		};
	},
	componentWillReceiveProps(nextProp){
 		this.setState({location : nextProp.location});
 	},
 	componentWillMount(){
 		properties.getLocations(x => {
 			this.setState({locations : x});
 		});
 	},
 	render() {
 		let givenStyle = {
      			display : this.props.visible ? 'block' : 'none'
    		};
 		return (
 			<div style={givenStyle}>
         		<common.CustomSelect data={this.state.locations} setValue={this.props.updateLocation} icon='svg-pin' iconClassProp='icon-mini fill-color1'/>
         	</div>
 			);
 	}
});

let DescriptionJob = React.createClass({
	getInitialState() {
		return {
			editView : false
		};
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({description : nextProp.description});
 	},
 	changeVisibility(){
 		this.setState({editView : !this.state.editView});
 	},
 	setDescription(description){
 		this.setState({description : description});
 	},
 	save: function(){
 		requests.update(this.props.id, {description : this.state.description}, (ans, res) => {
 			if(ans){
 				this.setState({editView : !this.state.editView}, () => {
 					if(this.props.setJob){
 						this.props.setJob(res);
 					}
 				});
 			}
 		});
 	},
	render() {
		let editButton = this.props.editRight ? '' : 'hide', 
			showDescription = this.state.editView ? 'hide' : '' ,
			showEdit = this.state.editView ? '' : 'hide',
			transitionClasses = this.props.editRight ? 'hover-color-white' : "";
		return (

			<div>
				<h3>Job description</h3> 
 				<div className={"center-hover-show padding-10px radius-5px transition-all-ease-3 " + transitionClasses + " " + showDescription}>
 					<div className="float-right margin-right-5px transition-all-ease-3 opacity-0 hover-opacity-100">
						<common.MiniIconButton onClick={this.changeVisibility} icon="svg-edit" labels="Edit" className={"margin-top-5px background-color1 inline-block " + editButton} colorIcon="fill-white" />
					</div>
					<DescriptionDisplay description={this.state.description} onClick={this.changeVisibility} />
				</div>
				<div className={showEdit}>
					<DescriptionEdit description={this.state.description} setDescription={this.setDescription} onClick={this.save} />
				</div>
			</div>
		);
	}
});

let DescriptionDisplay = React.createClass({
 	render() {
 		let displayRoles = this.props.description ? '': 'hide',
 			displayBackup = this.props.description ? 'hide': '';
 		return (
 			<div> 
 				<p className={'font-12px ' + displayRoles}> {this.props.description} </p>
 				<p className={'font-12px opacity-70 width-80pc ' + displayBackup}>No job description</p>
 			</div>
 		);
 	}
});

let DescriptionEdit = React.createClass({
	render() {
		return(
			<div className="padding-10px background-white">
				<div>
					<common.Textarea setValue={this.props.setDescription} defaultValue={this.props.description} compulsory='true' classProp='width-90pc margin-bottom-10px v-align-top' rows='5' placeholder="Generic job description"/>
				</div>
				<div>
					<common.MiniIconButton onClick={this.props.onClick} icon="svg-check" labels="Save" className="background-color1 inline-block margin-right-10px" colorIcon="fill-white" />
				</div>
			</div>
			);
	}
});

let RoleJob = React.createClass({
	getInitialState() {
		return {
			editView : false
		};
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({roles : nextProp.roles});
 	},
 	changeVisibility(){
 		this.setState({editView : !this.state.editView});
 	},
 	setRoles(roles){
 		this.setState({roles : roles});
 	},
 	save: function(){
 		requests.update(this.props.id, {roles : this.state.roles}, (ans, res) => {
 			if(ans){
 				this.setState({editView : !this.state.editView});
 				if(this.props.setJob){
					this.props.setJob(res);
				}
 			}
 		});
 	},
	render() {
		let editButton = this.props.editRight ? '' : 'hide', 
			showDescription = this.state.editView ? 'hide' : '' ,
			showEdit = this.state.editView ? '' : 'hide',
			transitionClasses = this.props.editRight ? 'hover-color-white' : "";
		return (
			<div>
				<h3>Responsabilities</h3> 
 				<div className={"center-hover-show padding-10px radius-5px transition-all-ease-3 " + transitionClasses + " " + showDescription}>
 					<div className="float-right margin-right-5px transition-all-ease-3 opacity-0 hover-opacity-100">
						<common.MiniIconButton onClick={this.changeVisibility} icon="svg-edit" labels="Edit" className={"margin-top-5px background-color1 inline-block " + editButton} colorIcon="fill-white" />
					</div>
					<RoleDisplay roles={this.state.roles} onClick={this.changeVisibility} />
				</div>
				<div className={showEdit}>
					<RoleEdit setRoles={this.setRoles} roles={this.state.roles} onClick={this.save} />
				</div>
			</div>
		);
	}
});

let RoleDisplay = React.createClass({
	getInitialState() {
		return {}; 
	},
	componentWillReceiveProps(nextProp){
 		this.setState({roles : nextProp.roles});
 	},
 	render() {
 		let displayRoles = this.state.roles ? '': 'hide',
 			displayBackup = this.state.roles ? 'hide': '';
 		return(
 			<div> 
 				<p className={'font-12px ' + displayRoles}> {this.state.roles} </p>
 				<p className={'font-12px opacity-70 width-80pc ' + displayBackup}>No responsability description</p>
 			</div>
 			);
 	}
});

let RoleEdit = React.createClass({
	getInitialState(){
		return{};
	},
	render() {
		return(
			<div className="padding-10px background-white">
				<div>
					<common.Textarea setValue={this.props.setRoles} defaultValue={this.props.roles} compulsory='true' classProp='width-90pc margin-bottom-10px v-align-top' rows='5' placeholder="Role description"/>
				</div>
				<div>
					<common.MiniIconButton onClick={this.props.onClick} icon="svg-check" labels="Save" className="background-color1 inline-block margin-right-10px" colorIcon="fill-white" />
				</div>
			</div>
			);
	}
});

let WhyUs = React.createClass({
	getInitialState() {
		return {
			editView : false
		};
 	},
 	componentWillReceiveProps(nextProp){
 		this.setState({whyUs : nextProp.whyUs});
 	},
 	changeVisibility(){
 		this.setState({editView : !this.state.editView});
 	},
 	setDescription(desc){
 		this.setState({whyUs : desc});
 	},
 	save: function(){
 		requests.update(this.props.id, {whyUs : this.state.whyUs}, (ans, res) => {
 			if(ans){
 				this.setState({editView : !this.state.editView});
 				if(this.props.setJob){
					this.props.setJob(res);
				}
 			}
 		});
 	},
	render() {
		let editButton = this.props.editRight ? '' : 'hide', 
			showDescription = this.state.editView ? 'hide' : '' ,
			showEdit = this.state.editView ? '' : 'hide',
			transitionClasses = this.props.editRight ? 'hover-color-white' : "";
		return (
			<div>
				<h3>Why us</h3> 
 				<div className={"center-hover-show padding-10px radius-5px transition-all-ease-3 " + transitionClasses + " " + showDescription}>
 					<div className="float-right margin-right-5px transition-all-ease-3 opacity-0 hover-opacity-100">
						<common.MiniIconButton onClick={this.changeVisibility} icon="svg-edit" labels="Edit" className={"margin-top-5px background-color1 inline-block " + editButton} colorIcon="fill-white" />
					</div>
					<WhyUsDisplay whyUs={this.state.whyUs} onClick={this.changeVisibility} />
				</div>
				<div className={showEdit}>
					<WhyUsEdit setDescription={this.setDescription} whyUs={this.state.whyUs} onClick={this.save} />
				</div>
			</div>
		);
	}
});

let WhyUsDisplay = React.createClass({
	getInitialState() {
		return {}; 
	},
	componentWillReceiveProps(nextProp){
 		this.setState({whyUs : nextProp.whyUs});
 	},
 	render() {
 		let givenStyle = {
			display : this.props.visible ? 'none' : 'inline' 
		};
		let displayRoles = this.state.whyUs? '': 'hide',
 			displayBackup = this.state.whyUs ? 'hide': '';
 		return(
 			<div style={givenStyle} > 
 				<p className={'font-12px ' + displayRoles}> {this.state.whyUs} </p>
 				<p className={'font-12px opacity-70 width-80pc ' + displayBackup}>No "Why us" description</p>
 			</div>
 			);
 	}
});

let WhyUsEdit = React.createClass({
	render() {
		return(
			<div className="padding-10px background-white">
				<div>
					<common.Textarea setValue={this.props.setDescription} defaultValue={this.props.whyUs} compulsory='true' classProp='width-90pc margin-bottom-10px v-align-top' rows='5' placeholder="'Why us' description"/>
				</div>
				<div>
					<common.MiniIconButton onClick={this.props.onClick} icon="svg-check" labels="Save" className="background-color1 inline-block margin-right-10px" colorIcon="fill-white" />
				</div>
			</div>
			);
	}
});

module.exports = {
	Jobs : Jobs
}